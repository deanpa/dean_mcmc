#!/bin/bash

# Navigate to the parent directory containing the iter_* subdirectories
cd ScenRakiura9k_Mod1_zip

# Loop through each subdirectory named iter_001 to iter_100
for i in $(seq -w 1 100); do

    dir=$(printf "iter_%03d" $((10#$i))) # Ensure the number is treated as decimal

#    dir=$(printf "iter_%03d" $i)  # Format directory name with leading zeros
    if [ -d "$dir" ]; then  # Check if directory exists
        echo "Deleting directory: $dir"
        rm -rf "$dir"
        echo "Deleted $dir"
    else
        echo "Directory $dir does not exist, skipping..."
    fi
    
    # Sleep for 1 second between each deletion
    sleep 1
done

echo "All directories deleted."
