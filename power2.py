#!/usr/bin/env python

import numpy as np
from numba import jit
import pylab as P


class PowerData(object):
    def __init__(self):
        """
        """
        # priors
        self.pmean = 0.99
        self.pAdd = - 0.44
        print('pCam and pDog', self.pmean, self.pmean + self.pAdd)

#        self.priorab = self.get_beta()
        self.priorab = np.array([1,1])
#        print('pmean', self.pmean)
#        print('priorab', self.priorab)

        ####################################
        ###################### gen data
        self.meanC = self.pmean
        self.meanD = self.pmean + self.pAdd
        self.nPosteriors = 5000     # 10000
        self.nSignif = 2000         #2000
        self.ntrial = np.arange(8, 14, 1)
        self.ntot = len(self.ntrial)
        self.camPostMat = np.zeros((self.nSignif, 2))
        self.dogPostMat = np.zeros((self.nSignif, 2))
        self.power = np.zeros(self.ntot)
        self.power2 = np.zeros(self.ntot)

        ##################
        ######  Run Functions

        self.loopNTrials()
        print('ntrial', self.ntrial)
        print('power', self.power)
#        self.plotFX()

    ###################### Functions

    def get_beta(self):
        """
        get alpha and beta values frm mean and sd
        """
        a = self.pmean * ( ((self.pmean * (1.0 - self.pmean)) / (self.psd**2.0)) - 1.0)
        b = (1.0-self.pmean)*((self.pmean*(1.0-self.pmean))/self.psd**2.0 - 1.0)
        p = np.array([a,b])
        return p

    def loopNTrials(self):
        """
        loop through each sample size to get power
        """
        for i in range(self.ntot):
            self.signifTot = 0
            self.genDataVariates(i)
            # loop through each data variate and get posterior beta parameters
            for j in range(self.nPosteriors):
                # get posterior beta parameters using generated data for camera and dogs.
                self.getPost(i,j)
                # use posterior to gen random variates for camera and dogs
                camBetaVariates = np.random.beta(self.camPostPara[0], self.camPostPara[1], self.nSignif) 
                dogBetaVariates = np.random.beta(self.dogPostPara[0], self.dogPostPara[1], self.nSignif) 
#                camDogDiff = dogBetaVariates - camBetaVariates
                nGreater = np.sum(dogBetaVariates < camBetaVariates)
                pGreater = nGreater/self.nSignif
                if pGreater >= 0.975:
                    self.signifTot += 1
                #### second signif test: mean > 97.5% CI
                
            self.power[i] = self.signifTot / self.nPosteriors


    def genDataVariates(self, i): 
        """
        generate data varariates for the number of posteriors
        """
        # gen obs camera and dog detection data
        self.camDat = np.random.binomial(self.ntrial[i], self.meanC, self.nPosteriors)
        self.dogDat = np.random.binomial(self.ntrial[i], self.meanD, self.nPosteriors)

    def getPost(self, i, j):     
        """
        get posterior beta a and b parameters for observed variates for cameras and dogs
        """
        self.camPostPara = self.betaPost(self.ntrial[i], self.camDat[j]) 
        self.dogPostPara = self.betaPost(self.ntrial[i], self.dogDat[j]) 

    def betaPost(self, n, y):    
        """
        fx to get a and b parameters
        """        
        alpha = y + self.priorab[0]
        beta = n - y + self.priorab[1]
        betapost = np.array([alpha, beta]) 
        return betapost
               
    def plotFX(self):
        """
        plot
        """
        P.figure()
        P.subplot(1,2,1)

        rprior = np.random.beta(self.priorab[0], self.priorab[1], 10000)
        P.hist(rprior)
        P.subplot(1,2,2)
        P.plot(self.ntrial, self.power)
        P.show()



########            Main function
#######
def main():

    powerobj = PowerData()


if __name__ == '__main__':
    main()

