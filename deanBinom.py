#!/usr/bin/env python

from scipy import stats
from scipy.special import gamma
import numpy as np
import pylab as P
import prettytable


##### global functions
##
def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))
##### end global functions



#########################################
#
#   GENERATE DATA
#
#########################################
class BernData(object):
    def __init__(self):
        """
        Object to generate Bernouilli data
        """
        self.ndat = 200
        self.nTraps = 200

        # betas used to simulate data
        self.B = np.array([-.75, 0.5, -1.0])
        self.Sig = 0.5
        self.npara = len(self.B)

        # make x data
        self.x = np.empty((self.ndat, self.npara))
        self.x[:,0] = 1.0
        xTmp = np.random.uniform(-10, 10, size = self.ndat)
        xTmp = (xTmp - np.mean(xTmp)) / np.std(xTmp)
        self.x[:,1] = xTmp

        xTmp = np.random.uniform(5,75, size = self.ndat)
        xTmp = (xTmp - np.mean(xTmp)) / np.std(xTmp)
        self.x[:, 2] = xTmp

        # simulate y data
        Ltheta = np.dot(self.x, self.B)
#        Ltheta = np.random.normal(mu, self.Sig)
        theta = inv_logit(Ltheta)
        self.y = np.random.binomial(self.nTraps, theta)
#        print('sum y', np.sum(self.y), theta, 'y', self.y)

############################################
#
#   END GENERATING DATA
#
############################################



############################################
#
#   ANALYSE DATA
#
############################################


class Params(object):
    def __init__(self):
        """
        #define priors and intial parameter
        """
        self.b = np.array([-.05, -0.05, -0.01]) #initial b estimate
        #defining priors
        self.meanB = 0.0
        self.varB = np.sqrt(1000.0)
        self.npara = len(self.b)
        #defining mcmc run parameters
        self.betaJump = .01
        self.ngibbs = 2500  #number of runs
        self.thin = 1       # thin rate
        self.burnin = 0  # burn in number of iterations
        self.totalIterations = (self.ngibbs * self.thin) + self.burnin
        self.keep = np.arange( start = self.burnin, stop = self.totalIterations, step = self.thin)
        

#defining MCMC sampler
class MCMC(object):
    """
    Defines MCMC updating functions and arrays for storing predicted coefficients
    """
    def __init__(self, berndata, params): 

        #### Data from generated data
        self.x = berndata.x    # covariate data
        self.y = berndata.y    # Bernouilli response variable
        # parameters to be updated in mcmc
        self.b = params.b.copy()
        self.nTraps = berndata.nTraps

        ### initial theta values
        self.ltheta = np.dot( self.x, self.b ) #initial log theta
        self.theta = inv_logit( self.ltheta ) #intital theta 
        # storage array for parameter estimates
        self.bgibbs = np.empty((len(params.keep), params.npara))
        print('betas from simulation', berndata.B)
        #######################################
        #
        #   Run updater

        self.gibbs(params)
        
        #
        #######################################

    
    def bupdate(self, params):
        """
        update beta parameters one at a time
        """
        for i in range(params.npara):
            bprop = self.b.copy()
            bprop[i] = np.random.normal(self.b[i], params.betaJump) #proposed values
            ltheta_s = np.dot(self.x, bprop)
            theta_s = inv_logit(ltheta_s)
            #log probabilities for current (pnow) & proposed (pnew) values
            lbinom = stats.binom.logpmf(self.y, self.nTraps , self.theta)
            lprior = stats.norm.logpdf(self.b[i] , params.meanB, params.varB)
            pnow = np.sum(lbinom) + lprior
            lbinom_s = stats.binom.logpmf(self.y, self.nTraps, theta_s)
            lprior_s = stats.norm.logpdf(bprop[i] , params.meanB, params.varB)
            pnew = np.sum(lbinom_s) + lprior_s
#            print('i', i, 'pnew', pnew, 'pnow', pnow)
            r = np.exp( pnew -  pnow ) #acceptance criterion
            #print(r)
            z = np.random.uniform( 1, 0, size = None)
            if z < r: #if accept with probability r update b & theta
                self.b[i] = bprop[i]
                self.ltheta = ltheta_s.copy()
                self.theta = theta_s.copy()
        

        #                        
    def gibbs(self, params):
        """
        run metropolis updater"
        """
        g = 0
        for gg in range(params.totalIterations):
            self.bupdate(params)
            #
            if gg in params.keep:
                self.bgibbs[g] = self.b
                g += 1
#        print('bgibbs', self.bgibbs)            #


class Results(object):
    """
    Process results
    """
    def __init__(self, berndata, params, mcmc): 
    ################
    ################ Functions to make table

        self.makeTableFX(params, mcmc)
        self.plotFX(params, mcmc)

    def quantileFX(self, a):
        """
        function to calculate quantiles
        """
        return stats.mstats.mquantiles(a, prob=[0.025, 0.5, 0.975])

    def makeTableFX(self, params, mcmc):
        """
        Function to print table of results
        """
        resultTable = np.zeros(shape = (4, (params.npara)))
        quants =  np.apply_along_axis(self.quantileFX, 0, mcmc.bgibbs)
        print('quants', quants)
        resultTable[0:3, :] = quants 
        resultTable[3, :] = np.apply_along_axis(np.mean, 0, mcmc.bgibbs)
        resultTable = np.round(resultTable.transpose(), 3)
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])
        names = ['Intercept', 'Beta1', 'Beta2']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)


    def plotFX(self, params, mcmc):
        """
        make diagnostic trace plots
        """
        meanBeta = np.apply_along_axis(np.mean, 0, mcmc.bgibbs)
        Ypredicted = np.dot(params.x, meanBeta[0:3])
        nplots = params.npara  # + 1
        P.figure(0)
        for i in range(nplots):
            P.subplot(2, 2, i+1)
            P.plot(mcmc.bgibbs[:, i])
        P.subplot(2,2, 4)
        P.plot(mcmc.bgibbs[:,1], mcmc.bgibbs[:,2])
#        P.scatter(self.params.y, Ypredicted)
        P.show()



########            Main function
#######
def main():

    # run berndata to generate data
    berndata = BernData()
    params = Params()
    mcmc = MCMC(berndata, params)
    results = Results(params, berndata, mcmc)

if __name__ == '__main__':
    main()
