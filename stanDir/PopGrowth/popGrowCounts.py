#!/usr/bin/env python

import numpy as np
import pandas as pd
import json
from cmdstanpy import CmdStanModel
import matplotlib.pyplot as plt

# Class to generate data
class DataGenerator(object):
    def __init__(self):
        self.T = 50
#        self.B = np.array([4.0, 0.3])
        self.beta0 = 4.0
        self.beta1 = 0.3
        self.rb = 0.75
        self.sigma = 0.2
        self.seed = 42
        self.C0 = 23
        self.X = None
        self.C = None
        self.data = None

    def generate_data(self):

        np.random.seed(self.seed)
        xTmp = np.random.uniform(-10, 10, size = self.T)
        self.X = (xTmp - np.mean(xTmp)) / np.std(xTmp)

        print('self.X', self.X)

        K = np.exp(self.beta0 + self.beta1 * self.X)

        print('K', K)

        self.C = np.zeros(self.T - 1)
        current_C = self.C0     #np.random.uniform(5, 65)  # Initial C0

        for t in range(1, self.T):
            mu_t = current_C * np.exp(self.rb * (1 - current_C / K[t-1]))
            current_C = np.exp(np.random.normal(np.log(mu_t), self.sigma))
            self.C[t-1] = current_C

        self.data = {'T': self.T,
                    'X': self.X.tolist(),
                    'C': self.C.tolist()}

        print('c data', self.data['C'])

    def save_data(self, file_name='data.json'):
        if self.data is None:
            raise ValueError("Data has not been generated yet.")
        with open(file_name, 'w') as f:
            json.dump(self.data, f)

# Class to set parameters for analysis
class AnalysisParameters(object):
    def __init__(self, chains=4, iter_sampling=1000, iter_warmup=500):
        self.chains = chains
        self.iter_sampling = iter_sampling
        self.iter_warmup = iter_warmup

# Class to perform analysis
class PopulationGrowthAnalysis(object):
    def __init__(self, stan_file="population_growth.stan"):
        self.model = CmdStanModel(stan_file=stan_file)
        self.fit = None

    def fit_model(self, data_file, params: AnalysisParameters, adapt_delta=0.8):
        self.fit = self.model.sample(data=data_file,
                                    chains=params.chains,
                                    iter_sampling=params.iter_sampling,
                                    iter_warmup=params.iter_warmup,
                                    adapt_delta=adapt_delta)

    def summary(self):
        if self.fit is not None:
            return self.fit.summary()
        else:
            raise ValueError("Model has not been fitted yet.")

# Class for diagnostics and visualization
class DiagnosticsAndVisualization(object):
    def __init__(self, fit):
        self.fit = fit

    def trace_plots(self):
        self.fit.diagnose()

    def plot_posterior(self, param_name):
        samples = self.fit.stan_variable(param_name)
        plt.hist(samples, bins=30, density=True)
        plt.title(f'Posterior distribution of {param_name}')
        plt.show()

    def save_results(self, summary_file='summary.csv'):
        summary = self.fit.summary()
        df = pd.DataFrame(summary)
        df.to_csv(summary_file, index=False)


## MAIN FUNCTION
def main():
    # Generate data
    generator = DataGenerator()
    generator.generate_data()
    generator.save_data('PopGrowData.json')

    # Set analysis parameters
    params = AnalysisParameters(chains=4, iter_sampling=1000, iter_warmup=500)

    # Perform analysis
    analysis = PopulationGrowthAnalysis(stan_file="popGrowCounts.stan")
    analysis.fit_model(data_file="PopGrowData.json", params=params, adapt_delta=0.95)
    print('Analysis summary', analysis.summary())

    # Diagnostics and visualization
    diagnostics = DiagnosticsAndVisualization(fit=analysis.fit)
    diagnostics.trace_plots()
    diagnostics.plot_posterior(param_name='rb')
    diagnostics.save_results('summary.csv')

if __name__ == "__main__":
    main()

