data {
    int<lower=1> T;            // Number of time points
    vector[T] X;               // Covariate values
    vector[T-1] C;             // Observed counts, excluding the initial unobserved C0
}

parameters {
    real<lower=0> sigma;       // Standard deviation of the log-normal distribution
    real<lower=0> rb; // Growth rate, constrained between 0 and 2 for example
    real<lower=0> C0; // Unobserved initial count, C0
    vector[2] beta;            // Parameters for calculating carrying capacity, K
}

model {
    // Priors
    sigma ~ lognormal(log(.5), .5);   // Inverse gamma prior for sigma
    beta ~ normal(0, 5);      // Normal prior for beta parameters
    rb ~ lognormal(log(0.75), 0.1);    // Uniform prior for growth rate
    C0 ~ lognormal(log(25), 0.5);      // Uniform prior for the initial unobserved count C0

    // Pre-computed carrying capacities for all time points based on covariates
    vector[T] K = exp(beta[1] + beta[2] * X); // Exponential to ensure K is positive
    print("K: ", K); // Print the entire vector K after calculation

    // Initialize the first mu value
    vector[T] mu;
    mu[1] = C0;                 // Start the series with C0

    // Time series model
    for (t in 2:T) {
        mu[t] = mu[t-1] * exp(rb * (1 - mu[t-1] / K[t-1])); // Recursive population growth model
        if (is_inf(mu[t]) || is_nan(mu[t])) {
            mu[t] = fmin(mu[t-1], 1e8); // Add a cap to prevent overflow
        }
    }

    // Model for observed counts
    for (t in 1:T-1) {
        if (!is_inf(log(mu[t+1])) && !is_nan(log(mu[t+1]))) {
            C[t] ~ lognormal(log(mu[t+1]), sigma); // C[t] influenced by mu[t+1]
        }
        print("mu[", t, "]: ", mu[t]); // Print mu at each time step
    }
}

generated quantities {
    vector[T-1] C_pred;        // Posterior predictions of counts
    vector[T] K = exp(beta[1] + beta[2] * X); // Re-calculate K
    vector[T] mu;                              // Re-calculate mu
    mu[1] = C0;

    for (t in 2:T) {
        mu[t] = mu[t-1] * exp(rb * (1 - mu[t-1] / K[t-1])); // Recalculate mu for generated quantities
    }

    for (t in 1:T-1) {
        C_pred[t] = exp(normal_rng(log(mu[t+1]), sigma)); // Generate predicted counts
    }
}


