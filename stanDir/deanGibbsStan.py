#!/usr/bin/env python

from scipy import stats
#from scipy.special import gamma
import numpy as np
import pylab as P
import prettytable
import os
from cmdstanpy import CmdStanModel
#import json
import pandas as pd


class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        self.ndat = 50
 
        # betas used to simulate data
        self.b = np.array([2.5, 1.0])
        self.a = -2.0
        
        # make x data
        self.x = np.empty((self.ndat,2))
        #self.x[:,0] = 1.0
        self.x[:,0] = np.random.uniform(-5, 5, size= self.ndat)
        xTmp = np.random.uniform(-10, 10, size= self.ndat) 
        self.x[:,-1] = xTmp
        
        # variance used to simulate data
        self.sg = 15.0

        # simulate y data
        yTmp = self.a + np.dot(self.x, self.b)
        self.y = np.random.normal(yTmp, np.sqrt(self.sg), size=self.ndat)

        self.npara = len(self.b)


        ## DATA DICTIONARY
        self.dataDict = {'N' : self.ndat, 'y' : self.y.tolist(), 'x' : self.x.tolist(), 
            'K' : self.npara}
        print('dataDict', self.dataDict)
        
        # specify Stan program file
        self.stan_file = os.path.join('.', 'multiGibbs.stan')
        # Priors
        self.diag = np.diagflat(np.tile(1000., self.npara))
        self.vinvert = np.linalg.inv(self.diag)
        self.s1 = .1
        self.s2 = .1
        self.ppart = np.dot(self.vinvert, np.zeros(self.npara))
        # Initial values
##        self.b = np.array([-9.5, 9.5])
##        self.sg = 4.0

class MCMC(object):
    def __init__(self, params):

        self.params = params

########            Main mcmc function
########
    def mcmcFX(self):
        """
        Run Stan
        """
        # instantiate the model object
        self.model = CmdStanModel(stan_file=self.params.stan_file)

        # inspect model object
        print('model', self.model)

        # inspect compiled model
        print(' ######################################    model.exe_info', self.model.exe_info())

        # fit the model
        self.fit = self.model.sample(data=self.params.dataDict)

        #summary
        print(self.fit.summary())

        # access model variable by name
        print('variable', self.fit.stan_variable('beta'))
        print('posteriors', self.fit.draws_pd('beta')[:13])
        print('alpha posteriors', self.fit.draws_pd('alpha')[:13])
        print('sigma posteriors', self.fit.draws_pd('sigma')[:13])
#        print(self.fit.draws_xr('beta'))
        for k, v in self.fit.stan_variables().items():
            print('fit variables', f'{k}\t{v.shape}')
        for k, v in self.fit.method_variables().items():
            print('fit method variables', f'{k}\t{v.shape}')
        print(f'numpy.ndarray of draws: {self.fit.draws().shape}')
        self.fit.draws_pd()
        
        (self.iters, self.chains, xxxx) = self.fit.draws().shape

        # the per-chain HMC tuning parameters from the NUTS-HMC adaptive sampler
        print(self.fit.metric_type)
        print(self.fit.metric)
        print(self.fit.step_size)

        #The CmdStanMCMC object also provides access to metadata about model and sampler run.
        print('metadata', self.fit.metadata.cmdstan_config['model'])

        self.betaPosteriors = self.fit.draws_pd('beta')
        # shpBeta = self.betaPosteriors.shape
        print('type alpha', type(self.fit.draws_pd('alpha')))
        print('type beta', type(self.fit.draws_pd('beta')))

        self.posteriors = pd.concat((self.fit.draws_pd('alpha'), self.betaPosteriors,
            self.fit.draws_pd('sigma')), axis = 1)


    ################
    ################ Functions to make table
    def quantileFX(self, a):
        """
        function to calculate quantiles
        """
        return stats.mstats.mquantiles(a, prob=[0.025, 0.5, 0.975])

    def makeTableFX(self):
        """
        Function to print table of results
        """

        resultTable = np.zeros(shape = (4, (self.params.npara+2)))

        resultTable[0:3] = np.apply_along_axis(self.quantileFX, 0, self.posteriors)

        print('resultTable', resultTable)

        resultTable[3] = np.apply_along_axis(np.mean, 0, self.posteriors)
        resultTable = np.round(resultTable.transpose(), 3)
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])

        names = ['Intercept', 'Beta1', 'Beta2', 'Standard dev']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)


    def plotFX(self, params):
        """
        make diagnostic trace plots
        """
        meanBeta = np.apply_along_axis(np.mean, 0, self.posteriors)
#        Ypredicted = np.dot(self.params.x, meanBeta[0:3])
        nplots = self.params.npara + 2  # + 1
        chainIndx = np.repeat(np.arange(self.chains), self.iters)
        P.figure(0)
        for i in range(nplots):
            
            P.subplot(2, 2, i+1)
            for j in range(self.chains):
                mask_j = chainIndx == j
                P.plot(np.arange(self.iters), self.posteriors.iloc[mask_j, i])
#        P.subplot(2,2, 4)
#        P.plot(self.posteriors.iloc[:,0], self.posteriors.iloc[:,1])
#        P.scatter(self.params.y, Ypredicted)
        P.show()





########            Main function
#######
def main():


    params = Params()

    mcmcobj = MCMC(params)
    mcmcobj.mcmcFX()
    mcmcobj.makeTableFX()
    mcmcobj.plotFX(params)

if __name__ == '__main__':
    main()

