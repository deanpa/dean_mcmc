#!/usr/bin/env python

from scipy import stats
#from scipy.special import gamma
import numpy as np
import pylab as P
import prettytable
import os
from cmdstanpy import CmdStanModel
#import json
#import pandas as pd

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        self.nPlots = 20
        self.subPlots = 5
        self.N = 5
        # betas used to simulate data
        self.d = np.array([0.5, 1.0])
        # make x data
        self.x = np.ones((self.nPlots, 2))
        self.x[:,1] = np.random.uniform(-1,1, size= self.nPlots)
        # simulate y data
        self.mu = inv_logit(np.dot(self.x, self.d))
        self.npara = len(self.d)
        ## SIMULATE PRECISION
        self.tau = 4.0
        self.epsil = 1.0
        self.phi = np.exp(np.random.normal(self.tau, self.epsil, self.nPlots))
        self.alpha = self.mu * self.phi
        self.beta = self.phi * (1 - self.mu)
        self.theta = np.zeros((self.nPlots, self.subPlots))
#        self.theta = np.random.beta(self.alpha, self.beta)

        self.y = np.zeros((self.nPlots, self.N), dtype=np.int32)
        for i in range(self.nPlots):
            self.theta[i] = np.random.beta(self.alpha[i], self.beta[i], self.subPlots)
            self.y[i] = np.random.binomial(self.N, self.theta[i])

            print('i', i, 'y', self.y[i], 'th', np.round(self.theta[i],2), 
                'alpha', np.round(self.alpha[i], 2), 'beta', np.round(self.beta[i], 2),
                'phi', np.round(self.phi[i],2))
            
        self.dataDict = {'nPlots' : self.nPlots, 'subPlots' : self.subPlots, 'N' : self.N,
                 'y' : self.y.tolist(), 'x' : self.x.tolist()}
#        self.dataDict = {'nPlots' : self.nPlots, 'subPlots' : self.subPlots, 'N' : self.N,
#            'y' : self.y, 'x' : self.x}
        
        # specify Stan program file
        self.stan_file = os.path.join('.', 'binomHierarchical.stan')



class HMC(object):
    def __init__(self, params):

        self.params = params

########            Main mcmc function
########
    def hmcFX(self):
        """
        Run Stan
        """
        # instantiate the model object
        self.model = CmdStanModel(stan_file=self.params.stan_file)
        # inspect model object
        print('model', self.model)

        # inspect compiled model
        print(' ######################################    model.exe_info', self.model.exe_info())

        # fit the model
        self.fit = self.model.sample(data=self.params.dataDict, show_console=True)

        #summary
        print(self.fit.summary())






########            Main function
#######
def main():


    params = Params()

    hmcobj = HMC(params)
    hmcobj.hmcFX()
#    mcmcobj.makeTableFX()
#    mcmcobj.plotFX(params)

if __name__ == '__main__':
    main()

