data {
  int<lower=0> N;   // sample size
  int<lower=0> K;   // number of covariates
  matrix[N, K] x;      // covariate
  vector[N] y;      // observed data
}
parameters {
  real alpha; // Intercept
  vector[K] beta; // Slope (regression coefficients)
  real < lower = 0 > sigma; // Error SD
}
model {
  y ~ normal(alpha + x * beta , sigma);
}

generated quantities {
}                   // The posterior predictive distribution
