#!/usr/bin/env python
import os
import sys
import optparse
import csv
import json



class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--csvFilePath", dest="csvFilePath", help="Set csv path")
        p.add_option("--jsonFilePath", dest="jsonFilePath", help="Set json path")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)

cmdargs = CmdArgs()

print('cmdargs', cmdargs.csvFilePath, cmdargs.jsonFilePath)

# Function to convert a CSV to JSON
# Takes the file paths as arguments
def make_json(csvFilePath, jsonFilePath):
    
    # create a dictionary
    data = {}
    
    # Open a csv reader called DictReader
    with open(csvFilePath, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
        
        # Convert each row into a dictionary
        # and add it to data
        for rows in csvReader:
            for columnName in rows:
                if columnName not in data:
                    data[columnName] = []
                data[columnName].append(rows[columnName])
            
    # Open a json writer, and use the json.dumps()
    # function to dump data
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
        jsonf.write(json.dumps(data, indent=4))

# Driver Code

# Decide the two file paths according to your
# computer system

#csvFilePath = os.path.join('/home/dean/workfolder/projects/dean_mcmc/stanDir/bernDat0.csv')
#csvFilePath = r'bernDat0.csv'
#jsonFilePath = r'Names.json'
#jsonFilePath = os.path.join('/home/dean/workfolder/projects/dean_mcmc/stanDir/bernDat0.json')

# Call the make_json function
make_json(cmdargs.csvFilePath, cmdargs.jsonFilePath)




## ON COMMAND LINE
### ./convertCSV_to_JSON.py --csvFilePath '/home/dean/workfolder/projects/dean_mcmc/stanDir/bernDat0.csv' --jsonFilePath '/home/dean/workfolder/projects/dean_mcmc/stanDir/bernDat0.json' --primeKey 'id'