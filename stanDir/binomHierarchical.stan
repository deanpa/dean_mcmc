data {
  int<lower=1> nPlots;
  int<lower=1> subPlots;
  array[nPlots, subPlots] int<lower=0> y;
  int<lower=0> N;
  matrix[nPlots, 2] x;
}

parameters {
  vector[2] delta;
  real<lower=0> tau;
  real<lower=0> log_epsilon;
  vector<lower=0>[nPlots] log_phi;
  matrix<lower=0.0001, upper=1>[nPlots, subPlots] theta;
}

transformed parameters {
  vector[nPlots] mu;
  vector<lower=0.0001>[nPlots] phi;
  real<lower=0.0001> epsilon;
  vector<lower=0.00001>[nPlots] alpha;
  vector<lower=0.0001>[nPlots] beta;
  mu = inv_logit(x * delta);
  phi = exp(log_phi);
  epsilon = exp(log_epsilon);
  alpha = mu .* phi;
  beta = (1 - mu) .* phi;
}

model {
  // Priors
  delta ~ normal(0, 1);
  tau ~ normal(4, 1);
  log(epsilon) ~ normal(log(1), 1);
  log_phi ~ normal(tau, epsilon);
  
  for (i in 1:nPlots) {
    for (j in 1:subPlots) {
      theta[i, j] ~ beta(alpha[i], beta[i]);
      y[i, j] ~ binomial(N, theta[i, j]);
#      theta[i, j] ~ beta(y[i, j] + alpha[i], (N - y[i, j]) + beta[i]);
    }
  }
}
