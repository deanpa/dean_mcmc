#!/usr/bin/env python

import os
from cmdstanpy import CmdStanModel
import json

f = open('bernDat0.json')
datafile = json.load(f)
datafile['N'] = len(datafile['y'])
datafile['y'] = [int(iii) for iii in datafile['y']]
del datafile['id']

print('datafile', datafile)

# specify Stan program file
stan_file = os.path.join('.', 'bernouilli.stan')

# instantiate the model object
model = CmdStanModel(stan_file=stan_file)

# inspect model object
print('model', model)

# inspect compiled model
print('model.exe_info', model.exe_info())

#datafile = {"N" : 10, "y" : [0,1,0,0,0,0,0,0,0,1]}

# fit the model
fit = model.sample(data=datafile)

#summary
print('fit.summary', fit.summary())

# access model variable by name
print('stan variable theta', fit.stan_variable('theta'))
print('draws', fit.draws_pd('theta')[:3])
#print(fit.draws_xr('theta'))
for k, v in fit.stan_variables().items():
    print(f'{k}\t{v.shape}')
for k, v in fit.method_variables().items():
    print(f'{k}\t{v.shape}')
print(f'numpy.ndarray of draws: {fit.draws().shape}')
fit.draws_pd()

# the per-chain HMC tuning parameters from the NUTS-HMC adaptive sampler
print('metric type', fit.metric_type)
print('metric', fit.metric)
print('stepsize', fit.step_size)

#The CmdStanMCMC object also provides access to metadata about the model and the sampler run.
print('metadata', fit.metadata.cmdstan_config['model'])
print('seed', fit.metadata.cmdstan_config['seed'])
print('stan_vars keys', fit.metadata.stan_vars.keys())
#print(fit.metadata.stan_vars_cols.keys())

#print(fit.metadata.method_vars.keys())


#analyzes the per-draw sampler parameters across all chains looking for potential problems
print(fit.diagnose())