#!/usr/bin/env python

import numpy as np
from scipy import stats
import pylab as P
#from scipy.special import gamma
#from scipy.special import gammaln
#from scipy.stats.mstats import mquantiles

def getBetaFX(mg0, g0Sd):
    a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    return (a, b)

        
class ThreeEs(object):
    def __init__(self):

        self.gg = np.linspace(.01,.99, num = 500)
        ## Beta means and SD
        self.betaMean = np.array([[.90, .6, .5],
                        [0.20, 0.85, 0.90],
                        [0.95, 0.85, 0.80]])
        self.betaSD = np.array([[0.07, 0.1, 0.1],
                        [0.05, 0.05, 0.07],
                        [0.05, 0.05, 0.1]])
        self.betaAB = np.zeros((3,2))
        self.priorA = np.array([10.0, 20.5, 12.0])
        self.priorB = np.array([23.0, 11.0, 45.0])
               
        
    def plotPSuccess(self):
        """
        ## make plot and write to directory
        """
        self.plotIndx = [1, 4, 2, 5, 3, 6]
        lineCol = ['k', 'r', 'b']
        labName = ['Ecology', 'Economics', 'Ethics']
        titNames = [' A.', ' B.', ' C.']
        P.figure(figsize=(14, 8))
        cc = 0
        for i in range(3):
            print('iiiiiiiiiiiiiiiiii', i)
            for j in range(2):
                ax1 = P.subplot(2, 3, self.plotIndx[cc])
                if j == 0:
                    # ecol line
                    self.betaAB[0] = getBetaFX(self.betaMean[i, 0], self.betaSD[i, 0]) 
                    pdf_ik = stats.beta.pdf(self.gg, self.betaAB[0, 0], self.betaAB[0, 1])
                    maxY = np.max(pdf_ik)
                    lns1 = P.plot(self.gg, pdf_ik,  label=labName[0], color=lineCol[0], 
                        linewidth=3)
                    ## econ line
                    self.betaAB[1] = getBetaFX(self.betaMean[i, 1], self.betaSD[i, 1]) 
                    pdf_ik = stats.beta.pdf(self.gg, self.betaAB[1, 0], self.betaAB[1, 1])
                    maxY = np.max([maxY, np.max(pdf_ik)])
#                    lns2 = P.plot(self.gg, pdf_ik,  label=labName[1], color=lineCol[1], 
#                        linewidth=4)
                    lns2 = P.plot(self.gg, pdf_ik, 'k--', label=labName[1], linewidth=3)
                    ## ethics line
                    self.betaAB[2] = getBetaFX(self.betaMean[i, 2], self.betaSD[i, 2]) 
                    pdf_ik = stats.beta.pdf(self.gg, self.betaAB[2, 0], self.betaAB[2, 1])
                    maxY = np.max([maxY, np.max(pdf_ik)])
#                    lns3 = P.plot(self.gg, pdf_ik,  label=labName[2], color=lineCol[2], 
#                        linewidth=4)
                    lns3 = P.plot(self.gg, pdf_ik, ls = '-.', color = 'k', 
                        label=labName[2], lw = 2)
                    if cc == 0:
                        lns = lns1 + lns2 + lns3  
                        labs = [l.get_label() for l in lns]
                        ax1.legend(lns, labs, loc = 'upper left')
                        ax1.set_ylabel('Density', fontsize = 14)
                    P.title(titNames[i], loc = 'left', fontsize = 14)
#                        ax1.set_ylim([0.0, maxY])
                else:
                    self.getOverallProb(i)
                    lns4 = P.plot(self.gg, self.fitpdf, label = 'Combined', color='k', 
                        linewidth=5)
                    if i == 0:
                        ax1.set_ylabel('Density', fontsize = 14)
                        P.legend(loc = 'upper right')
                    ax1.set_xlabel('Probability of success', fontsize = 14)
                cc += 1
                print('betaAB', self.betaAB)
                print('new a and b', np.sum(self.betaAB, axis = 0) - 2.0)
        P.savefig('threeEsProb.png', format='png', dpi = 1000)
        P.show()            
                    
                    
    
    def getOverallProb(self, i):
        nVariates = 30000
        prob0 = np.random.beta(self.betaAB[0,0], self.betaAB[0,1], size = nVariates)
        prob1 = np.random.beta(self.betaAB[1,0], self.betaAB[1,1], size = nVariates)
        prob2 = np.random.beta(self.betaAB[2,0], self.betaAB[2,1], size = nVariates)
        totalProb = np.log(prob0) + np.log(prob1) + np.log(prob2)
        totalProb = np.exp(totalProb)
        print('min max', np.min(totalProb), np.max(totalProb))
        fitBeta = stats.beta.fit(totalProb, self.priorA[i], self.priorB[i], 
                loc = 0.0, scale = 1.0)
        print('fit a', fitBeta[0], 'fit b', fitBeta[1])
        self.fitpdf = stats.beta.pdf(self.gg, fitBeta[0], fitBeta[1])
        maxPDFMask = self.fitpdf == np.max(self.fitpdf)
        print('i', i, 'Max Lik Prob', self.gg[maxPDFMask])

########            Main function
#######
def main():

    threeEs = ThreeEs()
    threeEs.plotPSuccess()
    
if __name__ == '__main__':
    main()
