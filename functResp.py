#!/usr/bin/env python

import numpy as np
from scipy import stats
import pylab as P

class FR_Test(object):
    def __init__(self):


#        a = 2.0
#        den = np.arange(21, step = 0.25)
#        h = 0.05
#        f = (a*den) / (1.0 + (a*h*den))
#        print('f', f)


        #### run functions
        self.readData()
        self.calcFunctRespDetectable()
        self.calcFR_TotalDensity()
        self.getDensityThresh()
        self.plotFR_detectable()
        self.plotFR_TotDensity()
        #####################

    def readData(self):
        """
        read in csv file
        """
        huntFname = 'DeerHarvest.csv'  
        self.huntDat = np.genfromtxt(huntFname,  delimiter=',', names=True,
            dtype=['i8', 'S10', 'S10', 'f8', 'f8', 'f8', 'i8', 'f8','f8','f8','f8',
                    'f8','f8','f8','f8'])
        # attack rate (m2 / hour): area searched per hour
        self.a = self.huntDat['AreaHrKm2']
        # Rate of removal: deer per hour
        self.F = self.huntDat['DeerHr']
#        self.F = self.huntDat['DeerMoveHr']

        # handling time deer / hours
        self.h = self.huntDat['HandlingHr']
        # density from data
        self.density = 1.0 / self.a / ((1.0 / self.F) - self.h)
        print('Density', self.density)

#        a = 2.0
#        den = np.arange(21, step = 0.25)
#        h = 0.05
#        f = (a*den) / (1.0 + (a*h*den))
#        print('f', f)

    def calcFunctRespDetectable(self):
        """
        calc FR using detectable densities
        """
        self.AR = np.mean(self.a)
        self.HR = np.mean(self.h)
        self.popDen = np.arange(10., step = 0.0001)

        self.predictF = (self.AR * self.popDen) / (1.0 + (self.AR * self.HR * self.popDen))

    def calcFR_TotalDensity(self):
        """
        calc FR using total density - obtained by estimating probability of detection
        """
        FR_diff = np.abs(2.0 - self.predictF)
        maskID = FR_diff == np.min(FR_diff)
        print('FRdiff 2/hr', FR_diff[maskID])
        detectDensity = self.popDen[maskID]
        print('detectDensity', detectDensity)
        # p(detect) using 1.2 total density as time with would get 2 deer / hour - ask Bruce
        self.probDetection = detectDensity / 1.2
        print('per deer probability of detection =', self.probDetection)
        self.totalDensityLine = self.popDen / self.probDetection
        self.estTotDensityPts = self.density / self.probDetection
        print('Estimated total density =', self.estTotDensityPts)

    def getDensityThresh(self):
        """
        get required pop density to make profit
        """
        self.ThreshHarvest = 5.5
        harvDiff = np.abs(self.predictF - self.ThreshHarvest)
        maskID = harvDiff == np.min(harvDiff)
        self.threshDensity = self.totalDensityLine[maskID]
        print('Threshold pop. density =', self.threshDensity)

    def plotFR_detectable(self):
        """
        plot FR using detectable densities
        """
        P.figure(figsize = (11,9))
        ax1 = P.gca()
        lns1 = (ax1.plot(self.density, self.F, 'ko', ms = 8.0, 
            label = 'Observed harvest rate'))
        lns2 = (ax1.plot(self.popDen, self.predictF, color = 'k', 
            label = 'Predicted hourly harvest rate', linewidth = 4))
        lns = lns1 + lns2
        labs = [l.get_label() for l in lns]
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Detectable density (deer / $\mathregular{km^2}$)', fontsize = 17)
        ax1.set_ylabel('Hourly harvest rate', fontsize = 17)
        ax1.set_ylim([0.0, 25.0])
        ax1.set_xlim([0.0, np.max(self.popDen + 1.)])
        ax1.legend(lns, labs, loc = 'upper left')
        figFname = 'deerFunctResp_detectDensity.png'
        P.savefig(figFname, format='png')
        P.show()

    def plotFR_TotDensity(self):
        """
        plot FR using TOTAL densities
        """
        P.figure(figsize = (11,9))
        ax1 = P.gca()
        lns1 = (ax1.plot(self.estTotDensityPts, self.F, 'ko', ms = 8.0, 
            label = 'Observed harvest rate'))
        lns2 = (ax1.plot(self.totalDensityLine, self.predictF, color = 'k', 
            label = 'Predicted hourly harvest rate', linewidth = 4))

        horzLineX = self.totalDensityLine[self.totalDensityLine <= self.threshDensity]
        horzLineY = np.repeat(self.ThreshHarvest, len(horzLineX))
        vertLineY = self.predictF[self.predictF <= self.ThreshHarvest]
        vertLineX = np.repeat(self.threshDensity, len(vertLineY))
        
        lns3 = ax1.plot(horzLineX, horzLineY, color = 'b',ls='dashed',
            label = 'Profit threshold', linewidth = 3)
        lns4 = ax1.plot(vertLineX, vertLineY, color = 'b',ls='dashed',
            linewidth = 3)


#        lns = lns1 + lns2
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Total density (deer / $\mathregular{km^2}$)', fontsize = 17)
        ax1.set_ylabel('Hourly harvest rate', fontsize = 17)
        ax1.set_ylim([0.0, 25.0])
        ax1.set_xlim([0.0, np.max(self.popDen + 1.)])
        ax1.legend(lns, labs, loc = 'upper left')
#        P.xticks(mon, monNames)
        figFname = 'deerFunctResp_totDen_blue.png'
        P.savefig(figFname, format='png')
        P.show()



########            Main function
#######
def main():

    FR_Test()

if __name__ == '__main__':
    main()
