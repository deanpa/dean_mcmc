#!/usr/bin/env python

from scipy import stats
import numpy as np
import pylab as P
import prettytable
from numba import njit
from scipy.special import gammaln
import time

##### global functions

import ctypes

gsl = ctypes.CDLL('libgsl.so')

gsl_ran_binomial_pdf = gsl.gsl_ran_binomial_pdf
gsl_ran_binomial_pdf.argtypes = [ctypes.c_uint, ctypes.c_double, ctypes.c_uint]
gsl_ran_binomial_pdf.restype = ctypes.c_double

# numba calls the GSL function
@njit
def binomial_pdf_numba(k, p, n):
    val = gsl_ran_binomial_pdf(k, p, n)
    return val

def getBetaParas(mg0, g0Sd):
    a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    return (a, b)

#@njit
#def logBinomPMF(success, trial, prob):
#    combBin = gammaln(trial + 1) - gammaln(success + 1) - gammaln(trial - success + 1)
#    lpmf = combBin + (success * np.log(prob)) + ((trial - success) * np.log(1.0 - prob))
#    return(lpmf)

@njit
def gibbsNumba(totalIterations, keep, nStore, N, SSE, indx, nJump, prior, nDetected):
    """
    run metropolis updater"
    """
    g = 0
    for gg in range(totalIterations):
        i = indx[gg]
        sse = SSE[i]
        N = n_updateNumba(sse, N, nJump, prior, nDetected)
        if gg in keep:
            nStore[g] = N
            g += 1


@njit
def n_updateNumba(sse, N, nJump, prior, nDetected):
    """
    ## METROPOLIS STEP TO UPDATE N
    """
    # Propose a new n from a simple symmetric proposal distribution (e.g., random walk)
    (n_star, mhCorrect) = propose_n(N, nJump, prior)
    #log probabilities for current (pnow) & proposed (pnew) values
    llik = np.log(binomial_pdf_numba(nDetected, sse, N))
    llik_s = np.log(binomial_pdf_numba(nDetected, sse, n_star))   
#    llik = logBinomPMF(nDetected, N , sse)
#    llik_s = logBinomPMF(nDetected, n_star, sse)
    r = np.exp(llik_s - llik + np.log(mhCorrect)) #acceptance criterion
    z = np.random.uniform(1, 0, size = None)
    if z < r: #if accept with probability r update n
        N = n_star
    return(N)

@njit
def propose_n(current_n, nJump, prior):
    """
    Propose a new n using an adjusted jump array based on the proximity to boundaries,
    ensuring all proposals are within the valid range, 
    and calculate Metropolis-Hastings correction.
    """
    # Adjust the jump array to only include jumps that keep the proposal within bounds
    adjusted_nJump = nJump[(current_n + nJump >= prior[0]) & (current_n + nJump <= prior[1])]
    # Choose a jump randomly from the adjusted array
    jump = np.random.choice(adjusted_nJump)
    proposed_n = current_n + jump
    # Calculate Metropolis-Hastings correction factor
    possible_jumps_from_current = len(adjusted_nJump)
    adjusted_nJump_proposed = nJump[(proposed_n + nJump >= prior[0]) & 
        (proposed_n + nJump <= prior[1])]
    possible_jumps_from_proposed = len(adjusted_nJump_proposed)
    # Correct MH correction to reflect the reverse jump over the forward jump probabilities
    mh_correction = possible_jumps_from_current / possible_jumps_from_proposed
    return(proposed_n, mh_correction)

##### end global functions


class Params(object):
    def __init__(self):
        """
        #define priors and parameters and data
        """
        self.meanSSe = 0.6
        self.sdSSe = 0.04
        self.a, self.b = getBetaParas(self.meanSSe, self.sdSSe)
        self.nSSe = 200
        self.SSe = np.random.beta(self.a, self.b, self.nSSe)
        self.n = 7
        self.nDetected = np.random.binomial(self.n, self.meanSSe)
        self.prior = np.array([self.nDetected, 100])
        print('Real values: mean SSe = ', self.meanSSe, '; sd SSe = ', self.sdSSe, 
            '; True pop. = ', self.n, '; n Detected = ', self.nDetected)

        #defining mcmc run parameters
        self.nJump = np.array([-2,-1, 1, 2])
        self.ngibbs = 2000  #number of runs
        self.thin = 20       # thin rate
        self.burnin = 100  # burn in number of iterations
        self.totalIterations = (self.ngibbs * self.thin) + self.burnin
        self.keep = np.arange( start = self.burnin, stop = self.totalIterations, step = self.thin)


class MCMC(object):
    """
    Defines MCMC updating functions and arrays for storing predicted coefficients
    """
    def __init__(self, params): 

        self.params = params
        ### initial N values
        self.N = 10 #n
        # storage array for parameter estimates
        self.nStore = np.empty(len(self.params.keep))
        self.indx = np.random.randint(0, self.params.nSSe, self.params.totalIterations)
        #######################################
        #   Run updater
        start_time = time.time()  # start timing

        gibbsNumba(self.params.totalIterations, self.params.keep, self.nStore, self.N,
        	self.params.SSe, self.indx, self.params.nJump, self.params.prior, self.params.nDetected)

        end_time = time.time()  # end timing

        print(f"Function execution time: {end_time - start_time} seconds")

        #######################################


class Results(object):
    """
    Process results
    """
    def __init__(self, params, mcmc): 
    ################
    ################ Functions to make table
        self.params = params
        self.mcmc = mcmc

        self.makeTableFX()
        self.plotTrace()

    def quantileFX(self, a):
        """
        function to calculate quantiles
        """
        return stats.mstats.mquantiles(a, prob=[0.05, 0.5, 0.95])

    def makeTableFX(self):
        """
        Function to print table of results
        """
        resultTable = np.zeros(shape = (6, 1))
        quants =  self.quantileFX(self.mcmc.nStore)
        print('quants', quants)
        resultTable[0:3,0] = quants 
        resultTable[3, 0] = np.mean(self.mcmc.nStore)
        resultTable[4, 0] = self.params.n 
        resultTable[5, 0] = np.sum(self.mcmc.nStore == self.params.nDetected)/self.params.ngibbs
        
        resultTable = np.round(resultTable.transpose(), 3)
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean', 'Real N', 'PoA prior'])
        names = ['N']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)


    def plotTrace(self):
        """
        make diagnostic trace plots
        """
        P.figure(figsize=(12,5))
        P.subplot(1,2,1)
        P.plot(self.mcmc.nStore)
        P.title('Estimated N')
        P.subplot(1,2,2)
        P.hist(self.params.SSe)
        P.title('SSe')
        P.savefig('traceHist.png', format='png')
        P.show()

########            Main function
#######
def main():
    params = Params()
    mcmc = MCMC(params)
    results = Results(params, mcmc)

if __name__ == '__main__':
    main()
