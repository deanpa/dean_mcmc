#!/usr/bin/env python

import os
from pathlib import Path
import getpass
import numpy as np
from scipy import stats
import pylab as P
from numba import njit
from scipy.stats.mstats import mquantiles
import json


@njit
def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX_sq = (x1 - x2)**2
    deltaY_sq = (y1 - y2)**2
    dist = np.sqrt(deltaX_sq + deltaY_sq)
    return dist

@njit
def foo2(l,h,n):
    ll = list(np.random.uniform(l, h, n))
    for i in range(3):
        newList = list(np.random.uniform(l, h, n))
        ll.extend(newList)
        m = np.min(np.array([6, (3+ll[-1])]))
        print('m', m)
    print('list', ll)
    del(ll[3])
    return(ll)
#    return(newList)

@njit
def loopIterations(iter, prop, nProperties, startDensity, areaHa, nStorage, extentSide,
        sigma, g0, nYears, trapX, trapY, trapNights, pTrapFail, pNeoPhobic,
        adultSurv, adultSurvDecay, perCapRecruit, recruitDecay, kExtent, dispersalSD, 
        kDistanceDD, centre, DDRadius, DDArea, eradEventSum, centralDensity):
    for i in range(iter):
#        N = 500
        N = np.random.poisson(startDensity * areaHa)
        X = list(np.random.uniform(0, extentSide, N))
        Y = list(np.random.uniform(0, extentSide, N))
        for yr in range(nYears):
            recruitX = []
            recruitY = []
            nRemoved = 0
            for ind in range(N):
                indx = ind - nRemoved

#                print('ind', ind, 'nRem', nRemoved, 'INDX', indx, 'N', len(X), 'X', X[indx])

                dist = distFX(X[indx], Y[indx], trapX, trapY)
                pCapt = g0 * np.exp(-(dist**2 / 2.0 / sigma**2))
                pNoTrap = 1 - (pCapt * (1.0 - pTrapFail))
                pNoTrap = pNoTrap**trapNights
                pCaptAll = 1.0 - (np.prod(pNoTrap) * (1.0 - pNeoPhobic))
                capt_ind = np.random.binomial(1, pCaptAll)
                if capt_ind == 1:
                    del(X[indx])
                    del(Y[indx])
                    nRemoved += 1
                    continue
            N = len(X)
            ## LOOP INDIVIDUALS AGAIN FOR SURVIVAL, RECRUITMENT AND DISPERSAL                    
            nRemoved = 0
            print('prop', prop, 'iter', i, 'yr', yr , 'N FOLLOWING TRAPPING', N)
            for ind in range(N):
                indx = ind - nRemoved

#                print('ind', ind, 'nRem', nRemoved, 'INDX', indx, 'N', len(X), 'X', X[indx])

                ## GET LOCAL DD
                distDD = distFX(X[indx], Y[indx], np.array(X), np.array(Y))
                nDD = np.sum(distDD < DDRadius)
                pSurv = adultSurv * (np.exp(-nDD**2 / kDistanceDD**adultSurvDecay))
                pMaxRec = np.exp(-nDD**2 / kDistanceDD**recruitDecay)
                recRate = perCapRecruit * pMaxRec

                surv_ind = np.random.binomial(1, pSurv)
                if surv_ind == 0:
                    del(X[indx])
                    del(Y[indx])
                    nRemoved += 1
                else:
                    nRecruit = np.random.poisson(recRate)
#                    print('ind survived', ind, 'Indx', indx, 'nRecruit', nRecruit, 'recRate', recRate,
#                        'X', X[indx])
                    for rec in range(nRecruit):
                        dx = np.random.normal(0, dispersalSD)
                        dy = np.random.normal(0, dispersalSD)
                        recX = X[indx] + dx
                        recY = Y[indx] + dy
                        xOut = recX < 0 or recX > extentSide
                        yOut = recY < 0 or recY > extentSide
#                        print('rec', rec, 'recX', recX)
                        if xOut or yOut:
#                            print('OUTSIDE', 'recX', recX, 'recY', recY, 'extentSide', extentSide)
                            upY = np.min(np.array([Y[indx] + 100, extentSide]))
                            loY = np.max(np.array([Y[indx] - 100, 0]))
                            lx = np.max(np.array([X[indx] - 100, 0]))
                            rx = np.min(np.array([X[indx] + 100, extentSide]))
                            x = np.random.uniform(rx, lx)
                            y = np.random.uniform(loY, upY)
                            recX = x
                            recY = y
#                            print('rec', rec, 'FIXED LOC', 'recX', recX, 'recY', recY, 
#                                'X', X[indx], 'Y', Y[indx])

                        recruitX.append(recX)
                        recruitY.append(recY)
            X.extend(recruitX)
            Y.extend(recruitY)    
            N = len(X)
#            print('yr', yr, 'N', N, 'n Rec', len(recruitX))    

#            if prop == 8:
                ## PLOT CENTRAL DENSITY FOR LAST PROPERTY
            distCentre = distFX(centre[0], centre[1], np.array(X), np.array(Y))
            nDD = np.sum(distCentre < DDRadius)
            densityHA = nDD / DDArea
            centralDensity[prop, i, yr] = densityHA


        nStorage[i, prop] = N
        if N == 0:
            eradEventSum[prop] += 1

#        print('prop', prop, 'iter', i, centralDensity[prop, i])
#            print('prop central density', centralDensity[iter])
#    print('End centralDensity', centralDensity[0])
    return(X,Y)



class Params(object):
    def __init__(self):
        self.species = 'Rats'
        self.r = {'Rats' : np.log(3), 'Possums' : np.log(.75), 'Stoats' : np.log(3.0)}
        self.k = {'Rats' : 5.0, 'Possums' : 8.0, 'Stoats' : 3.0}
        self.sigma = {'Rats' : 40, 'Possums' : 90, 'Stoats' : 240}
        self.g0 = {'Rats' : .05, 'Possums' : 0.1, 'Stoats' : 0.02}

        self.startDensity = {'Rats' : 3, 'Possums' : 7, 'Stoats' : 2}
        self.propHrMultiplier = [.5, 4]
        self.extentHRMultiplier = 25
#        self.disperseParas = {'Rats' : {'Mean' : 4, 'Var' : 0.7}, 
#                            'Possums' : {'Mean' : 4, 'Var' : 0.7},
#                            'Stoats' : {'Mean' : 10, 'Var' : 1.0}}
        self.dispersalSD = {'Rats' : 80, 'Possums' : 150, 'Stoats' : 400}
        self.trapLayout = {'Rats' : {'transectDist' : 100, 'trapDist' : 50}, 
                            'Possums' : {'transectDist' : 100, 'trapDist' : 50},
                            'Stoats' : {'transectDist' : 1000, 'trapDist' : 200}}
        self.bufferLayout = {'Rats' : {'transectDist' : 75, 'trapDist' : 25}, 
                            'Possums' : {'transectDist' : 75, 'trapDist' : 25},
                            'Stoats' : {'transectDist' : 750, 'trapDist' : 100}}
        self.bufferHRProp = 1.0
        self.adultSurv = {'Rats' : np.exp(-0.5), 'Possums' :  np.exp(-0.3), 
            'Stoats' : np.exp(-.2)}
        self.adultSurvDecay = {'Rats' : 2.0, 'Possums' : 2.0, 'Stoats' : 2.0}
        self.perCapRecruit = {'Rats' : 3.0, 'Possums' : 0.75, 'Stoats' : 3.5}
        self.recruitDecay = {'Rats' : 1.5, 'Possums' : 1.5, 'Stoats' : 1.5}
        self.distanceDD = {'Rats' : 2, 'Possums' : 2, 'Stoats' : 2}

        self.iter = 3
        self.nYears = 3
        self.trapNights = 45
        self.pTrapFail = 0.02
        self.pNeoPhobic = 0.01

        self.closedSystem = True

        baseDir = os.getenv('KIWIPROJDIR', default='.')
        if baseDir == '.':
            baseDir = '/home/dean/workfolder/projects/dean_predatorfree/DataResults/Results/'
#            baseDir = Path.cwd()
        else:
            ## IF ON NESI, ADD THE PredatorFree BASE DIRECTORY
            baseDir = os.path.join(baseDir, 'PredatorFree', 'DataResults', 'Results') 
        ## GET USER
        userName = getpass.getuser()
        resultsPath = os.path.join(userName, 'OwnerParticipation')
        ## PUT TOGETHER THE BASE DIRECTORY AND PATH TO RESULTS DIRECTORY 
        self.outputDataPath = os.path.join(baseDir, resultsPath)

        print('Results directory:', self.outputDataPath)
        print('############################')
        ## MAKE NEW RESULTS DIRECTORY IF DOESN'T EXIST
        if not os.path.isdir(self.outputDataPath):
            os.makedirs(self.outputDataPath)

        self.simResultsPath = os.path.join(self.outputDataPath, 'ownerResults.json')

class Simulation(object):
    def __init__(self, params):
        self.params = params
        ####################
        ## RUN FUNCTIONS
        self.getElements()
        self.makeTrapNetworks()
        self.loopProperties()
#        self.plotCentralDensity()
        self.prepareWriteJSON()
        ####################

    def getElements(self):
        self.hrRadius = int(self.params.sigma[self.params.species] * 3)
        self.hrDiameter = self.hrRadius * 2
        print('hrDiameter', self.hrDiameter, 'hr area (ha)', np.pi * self.hrRadius**2 / 10000)
        self.extentSide = self.hrDiameter * self.params.extentHRMultiplier
        self.centre = np.array([self.extentSide / 2, self.extentSide / 2])
        self.propRadius = np.arange(self.hrRadius * self.params.propHrMultiplier[0],
            self.hrRadius * self.params.propHrMultiplier[1], 
            self.params.bufferLayout[self.params.species]['trapDist'] / 0.75)
        self.areaHa = self.extentSide**2 / 10000
        self.propAreaHA = np.pi * self.propRadius**2 / 10000
        self.nProperties = len(self.propAreaHA)
        print('n props', self.nProperties, 'area Ha', self.areaHa)
        self.bufferRadius = ((self.hrRadius * self.params.bufferHRProp) + self.propRadius)
        print('N bufferRadius', len(self.bufferRadius))
        ## CARRYING CAPACITY
        self.kExtent = self.params.k[self.params.species] * self.areaHa
        self.DDRadius = self.hrRadius * self.params.distanceDD[self.params.species]
        self.DDArea = np.pi * (self.DDRadius**2) / 10000
        self.kDistanceDD = self.params.k[self.params.species] * self.DDArea


    def makeTrapNetworks(self):
        xMin = self.params.trapLayout[self.params.species]['transectDist'] / 2
        yMin = self.params.trapLayout[self.params.species]['trapDist'] / 2
        x0 = np.arange(xMin, self.extentSide, 
            self.params.trapLayout[self.params.species]['transectDist'])  
        nX = len(x0)
        y0 = np.arange(yMin, self.extentSide,
            self.params.trapLayout[self.params.species]['trapDist'])
        nY = len(y0)
        self.trapX = np.repeat(x0, nY)
        self.trapY = np.tile(y0, nX)
        xMin = self.params.bufferLayout[self.params.species]['transectDist'] / 2
        yMin = self.params.bufferLayout[self.params.species]['trapDist'] / 2
        x0 = np.arange(xMin, self.extentSide, 
            self.params.bufferLayout[self.params.species]['transectDist'])  
        nX = len(x0)
        y0 = np.arange(yMin, self.extentSide,
            self.params.bufferLayout[self.params.species]['trapDist'])
        nY = len(y0)
        self.bufferX = np.repeat(x0, nY)
        self.bufferY = np.tile(y0, nX)
        self.distCentreTrap = distFX(self.centre[0], self.centre[1], 
            self.trapX, self.trapY)
        self.distbuffTrapCentre = distFX(self.centre[0], self.centre[1], 
            self.bufferX, self.bufferY)
#        self.costStorage = np.zeros((self.params.iter, self.nProperties))
        self.nStorage = np.zeros((self.params.iter, self.nProperties))
        self.eradEventSum = np.zeros(self.nProperties)
        self.centralDensity = np.zeros((self.nProperties, self.params.iter, self.params.nYears))
        self.nTrapsStorage = []     #np.zeros(self.nProperties)

    def plotCentralDensity(self):
        P.figure(figsize=(9,11))
        meanDen = np.mean(self.centralDensity[-1], axis = 0)
        years = np.arange(self.params.nYears)
        P.plot(years, meanDen, 'k-o')
        P.show()


    def loopProperties(self):
#        for prop in range(7,9):
        for prop in range(self.nProperties):
            ## GET TRAP DATA FOR THIS PROPERTY SIZE
#            print('prop', prop, 'prop radius', self.propRadius[prop], 
#                'buf rad', self.bufferRadius[prop])
            minBuffMask = self.distbuffTrapCentre >= self.propRadius[prop]
            maxBuffMask = self.distbuffTrapCentre <= self.bufferRadius[prop]
            buffMask = minBuffMask & maxBuffMask
            xTrap_prop = self.bufferX[buffMask]
            yTrap_prop = self.bufferY[buffMask]
            mask_trap = self.distCentreTrap > self.bufferRadius[prop]
            xTrap_prop = np.append(xTrap_prop, self.trapX[mask_trap])
            yTrap_prop = np.append(yTrap_prop, self.trapY[mask_trap])
            self.nTrapsStorage.append(len(xTrap_prop)) 
            (X,Y) = loopIterations(self.params.iter, prop, self.nProperties, 
                self.params.startDensity[self.params.species], self.areaHa, 
                self.nStorage, self.extentSide, self.params.sigma[self.params.species], 
                self.params.g0[self.params.species], self.params.nYears,
                xTrap_prop, yTrap_prop, self.params.trapNights, self.params.pTrapFail,
                self.params.pNeoPhobic, self.params.adultSurv[self.params.species],
                self.params.adultSurvDecay[self.params.species], 
                self.params.perCapRecruit[self.params.species], 
                self.params.recruitDecay[self.params.species], self.kExtent, 
                self.params.dispersalSD[self.params.species],
                self.kDistanceDD, self.centre, self.DDRadius, self.DDArea,
                self.eradEventSum, self.centralDensity)

#            print('XXX Central Density', centralDensity)

        P.figure()
        P.scatter(X, Y)
        P.xlim(0, self.extentSide)
        P.ylim(0, self.extentSide)
        P.show()    

#        print('central density', centralDensity)

    def prepareWriteJSON(self):
        simResults = {}
        simResults['CentralDensity'] = self.centralDensity.tolist()


#        print('hr', type(self.hrRadius), 'proprad', type(self.propRadius),
#            'proparea', type(list(self.propAreaHA)), 'ntrap', type(self.nTrapsStorage),
#            'nstore', type(list(self.nStorage)), 'event', type(list(self.eradEventSum)))

        simResults['hrRadius'] = self.hrRadius
        simResults['propRadius'] = self.propRadius.tolist()
        simResults['propArea'] = self.propAreaHA.tolist()

        simResults['nTraps'] = self.nTrapsStorage
        simResults['nStorage'] = self.nStorage.tolist()
        simResults['eradEventSum'] = self.eradEventSum.tolist()



        with open(self.params.simResultsPath, 'w') as f:
            json.dump(simResults, f)


class ProcessResults(object):
    def __init__(self, params):
        self.params = params
        ####################
        ## RUN FUNCTIONS
        self.readJSON()


    def readJSON(self):
        # Load the JSON file back into a list
        with open(self.params.simResultsPath, 'r') as json_file:
            self.simResultsLists = json.load(json_file)
        # Convert the list back to a NumPy array
        self.centralDensity = np.array(self.simResultsLists['CentralDensity'])
        print('cent density', self.centralDensity, self.centralDensity.shape)



########            Main function
#######
def main():
    #foo2(2,12,10)

    params = Params()
    simulation = Simulation(params)
    processresults = ProcessResults(params)

if __name__ == '__main__':
    main()
