#!/usr/bin/env python

import numpy as np

def viterbi(states, init, trans, emit, obs):
    """
    Viterbi Algorithm to find the most probable path for a sequence of observations.

    Parameters:
    states (list): List of hidden states.
    init (np.array): Initial probabilities of each state.
    trans (np.array): Transition matrix (S x S).
    emit (np.array): Emission matrix (S x O).
    obs (list): Sequence of observations.

    Returns:
    list: Most likely sequence of states.
    """
    T = len(obs)
    S = len(states)
    prob = np.zeros((T, S))
    prev = np.empty((T, S), dtype=int)
    print('prev', prev)

    # Initialize the probabilities for the first observation
    for s in range(S):
        prob[0, s] = init[s] * emit[s, obs[0]]
        print('INTIAL prob', 's column', s, 'prob', prob)

    # Fill the probability and previous state matrices
    for t in range(1, T):
        for s in range(S):
            for r in range(S):
                new_prob = prob[t-1, r] * trans[r, s] * emit[s, obs[t]]
                if new_prob > prob[t, s]:
                    prob[t, s] = new_prob
                    prev[t, s] = r
                print('t', t, 's', s, 'r', r, 'prob_t', prob[t], 'prev_t', prev[t])

    # Backtrack to find the optimal path
    path = np.zeros(T, dtype=int)
    path[T-1] = np.argmax(prob[T-1, :])
    for t in range(T-2, -1, -1):
        path[t] = prev[t+1, path[t+1]]

    return [states[p] for p in path]

# Example usage
states = ['Rainy', 'Sunny']
init = np.array([0.6, 0.4])
trans = np.array([[0.7, 0.3],
                  [0.4, 0.6]])
emit = np.array([[0.7, 0.2, 0.1],  # Rainy: Observations are Umbrella, Partial Umbrella, no umbrella
                 [0.1, 0.3, 0.6]])  # Sunny: Observations are Umbrella, partial Umbrella, No umbrella
obs = [1, 0, 2]  # Umbrella, Partial mbrella, No Umbrella

# Find the most likely state sequence
most_likely_states = viterbi(states, init, trans, emit, obs)
print("Most likely states:", most_likely_states)
