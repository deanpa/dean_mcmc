#!/usr/bin/env python


import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os


class ResultsProcessing(object):
    def __init__(self, gibbsobj, processeddata):


        self.gibbsobj = gibbsobj
        self.results = self.gibbsobj.bgibbs.copy()
        self.processeddata = processeddata

        self.ndat = np.shape(self.results)[0]
        self.npara = self.results.shape[1]

    @staticmethod
    def quantileFX(a):
        return mquantiles(a, prob=[0.025, 0.5, 0.975], axis = 0)


    def makeTableFX(self):
        """
        Function to print table of results
        """
        resultTable = np.zeros(shape = (4, (self.npara)))

        resultTable[0:3, :] = np.apply_along_axis(self.quantileFX, 0, self.results)

        resultTable[3, :] = np.apply_along_axis(np.mean, 0, self.results)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])

        names = ['Intercept', 'Beta1', 'Beta2']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)


    def plotFX(self):
        """
        make diagnostic trace plots
        """
        meanBeta = np.apply_along_axis(np.mean, 0, self.results)
        Ypredicted = np.dot(self.processeddata.x, meanBeta)
        nplots = self.npara
        P.figure(0)
        for i in range(nplots):
            P.subplot(2, 2, i+1)
            P.plot(self.results[:, i])
        P.subplot(2,2, 4)
        P.plot(self.results[:,0], self.results[:,1])
#        P.scatter(self.params.y, Ypredicted)
        P.show()




########            Write data to file
########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'U12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float),
                    ('Number Removed', np.float), ('Month', np.float), ('Year', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]

        structured['Number Removed'] = self.summaryTable[:, 4] 
        structured['Month'] = self.summaryTable[:, 5]
        structured['Year'] = self.summaryTable[:, 6]
        structured['Names'] = self.names
        np.savetxt('summaryTable_cats.txt', structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f',
                    '%.4f', '%.4f', '%.4f'],
                    header='Names Low_CI Median High_CI Mean Number_removed Month Year')


def main():

    # paths and data to read in
    bernpath = os.getenv('NESIPROJDIR', default='.')

    inputGibbs = os.path.join(bernpath, 'out_gibbs.pkl')
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    BernDataFile = os.path.join(bernpath,'out_berndata.pkl')
    fileobj = open(BernDataFile, 'rb')
    processeddata = pickle.load(fileobj)
    fileobj.close()

    resultsobj = ResultsProcessing(gibbsobj, processeddata)

    resultsobj.makeTableFX()
    
#    print(resultsobj.makeTableFX())

    resultsobj.plotFX()
    
#    resultsobj.writeToFileFX()


if __name__ == '__main__':
    main()


