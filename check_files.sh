#!/bin/bash

# Navigate to the parent directory containing the iter_* subdirectories
cd ScenRakiura9k_Mod1_zip

# Find and list all directories that match the pattern 'iter_???'
echo "Checking directories to be deleted..."
find . -name 'iter_???' -type d -print | sort

echo "Check complete. Directories listed above will be deleted with the run script."
