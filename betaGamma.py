#!/usr/bin/env python

import numpy as np
from scipy import stats
import pylab as P
from scipy.special import gamma
from scipy.special import gammaln
from numba import njit, prange
import time
from scipy.stats.mstats import mquantiles

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf

def wrapped_cauchy_pdf(th, mu, rho):
    """
    Wrapped Cauchy PDF based on the Wikipedia equation.
    """
    numerator = 1 - rho**2
    denominator = 1 + rho**2 - 2*rho*np.cos(th - mu)
    pdf = numerator / (2 * np.pi * denominator)
    return pdf

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    Wikipedia pdf equation
    """
    sinh_rho = np.sinh(rho)
    cosh_rho = np.cosh(rho)
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc




def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D

def binomCombine(trial, success):
    combBin = gammaln(trial + 1) - gammaln(success + 1) - gammaln(trial - success + 1)
    return(combBin)

def getBetaParas(mg0, g0Sd):
    a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    return (a, b)


class G0Test(object):
    def __init__(self):

        self.gg = np.arange(.001,.5, .001)
#        self.dpdf = stats.beta.pdf(self.gg, self.params.g0_alpha, self.params.g0_beta)
#        P.plot(self.gg, self.dpdf)
#        P.show()
#        P.cla()
        mode = .08
        a = 3.0 #1.3
#        b = 4
        b = ((a-1.0)/mode) - a + 2.0
        print("a and b", a, b)
        print('mean beta', (a/(a+b)), 'sd =', np.sqrt(a*b / (a+b)**2 / (a+b+1)))
        print('mode', mode) 
        modeAB = (a - 1) /(a + b - 2)
        print('modeAB', modeAB)
#        randbeta = np.random.beta(a,b,100000)
#        print('nabove', len(randbeta[randbeta>.04]), 'nbelow', len(randbeta[randbeta < .04]))
        self.dpdf = stats.beta.pdf(self.gg, a, b)   #3.9, 191.)  # 1.0, 15.667)         # .095, 37.05)
        P.plot(self.gg, self.dpdf)
#        P.ylim(0., 15.0)
        P.xlim(0.001, np.max(self.gg))        
#        P.hist(randbeta)
        P.show()
        P.cla()



    @staticmethod
    def getBetaFX(mg0, g0Sd):
        a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
        b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
        return (a, b)




class eradTest(object):
    def __init__(self):
        P.figure(figsize=(6,6))
        
        self.g0 = 0.96
        self.sig = 8.0
        self.dist = np.arange(20)
        self.pRem = self.g0 * np.exp(-self.dist**2 / 2.0 / self.sig**2)
        P.plot(self.dist, self.pRem, linewidth= 3.0, color='k')

        P.xlabel('Property size', fontsize = 16)
        P.ylabel('Probability of eradication', fontsize = 16)
        P.show()
        P.cla()




class gammaTest(object):
    def __init__(self):

#        self.params = params
#        print(self.params.g0_alpha)
#        self.rr = np.arange(.005, 1.5, .001)
        self.rr = np.arange(.001, 2, .001)
#        mode = 1.0
        sh = .001
        sc = 1000  #0.1   #0.15   #1000.    #.1    # 1.0/ 5.4  #2.25   #1.6667  #3.    
#        sh = (mode / sc) + 1.0
        print('sh =', sh, 'sc =', sc)
#        sc = 3.0     # 1.5     # .66667    
#        rate = 1.0/sc    #0.5  #40.0     #1.1      #.5 #4  #1.      #2
        self.dpdf = gamma_pdf(self.rr, sh, (sc))           # use shape and scale: sc = 1/rate !!!
        randg = np.random.gamma(sh, sc, size = 2000)          # use shape and scale
        print('max randg', np.max(randg))
        print("mean", sh*sc, "sd", np.sqrt(sh*sc**2), "mode", (sh-1.)*sc)
        print("parameter calc mean", sh * sc)
        print("max LL r", self.rr[self.dpdf==np.max(self.dpdf)], ((sh-1)*sc))
        print("max pdf", self.dpdf[self.dpdf==np.max(self.dpdf)])
        
        P.figure()
        P.subplot(1,2,1)
        P.hist(randg)
        P.subplot(1,2,2)
        P.plot(self.rr, self.dpdf)
#        P.ylim(0, .03)
        P.show()
        P.cla()

class wrpCTest(object):
    def __init__(self):
        days = np.arange(365)
        aa = days/364 * 2. 
        aa = np.arange(0, (np.pi*2), .05)
        mu = 1.4    # np.pi*2
        rho = 3.   #.7
        dwrpc = dwrpcauchy(aa, mu, rho)
        st_dwrpc = (dwrpc/np.max(dwrpc)) * 0.05
        print(dwrpc)
        print('st_dwrpc', st_dwrpc)
        P.figure(0)
#        P.plot(aa, dwrpc)
        P.plot(aa, st_dwrpc, color = 'r')
#        P.ylim(0, 1.0)
        P.show()
        P.cla()

class normalBeta(object):
    def __init__(self):
        M = .4
        V = .01
        dd = logit(np.arange(.001, 1, .001))
        ddpdf = stats.norm.pdf(dd, logit(M), .5)
        l_mu = np.random.normal(logit(M), .05, 2000)
        mu = inv_logit(l_mu)
        print(np.std(mu))
        P.figure(0)
        P.subplot(1,2,1)
        P.hist(mu)
        P.xlim(0,1.0)
        P.subplot(1,2,2)
        P.plot(dd, ddpdf)
#        P.ylim(0, 2.0)
        P.show()
        P.cla()
        

class Poisson(object):
    def __init__(self):
        dat = 91
        datGammaLn = gammaln(dat + 1)
        ll = 178.002847241 

        lpmf = self.ln_poissonPMF(dat, datGammaLn, ll)

        pois = stats.poisson.logpmf(dat, ll)
        print('lpmf', lpmf, 'pois', pois)

    def ln_poissonPMF(self, dat, datGammaLn, ll):
        """
        calc log pmf for poission
        """
        ln_ppmf = ((dat * np.log(ll)) - ll) - datGammaLn
        return(ln_ppmf)

class SurvivorPrp(object):
    def __init__(self):
        n = 100
        age = np.arange(6)
        sur = 0.8
        prpSurv = np.exp(-age * sur)
        prpSuv = np.cumprod(prpSurv)
        print('prpSurv', prpSurv)
        years = (age + 1).astype(int)
        P.figure()
        P.plot(age, prpSurv, color = 'k', linewidth = 4.0)
        P.xlabel('Age')
        P.ylabel('Mean age specific survival')
        FName = 'prpSurviving.png'
        P.savefig(FName, format='png')

        P.show()

#        prpSurv = np.zeros(len(age))
#        p = 1.0
#        for i in age:
#            p = p * (np.exp(-sur * i))
#            prpSurv[i] = p
#        print('prpSurv', prpSurv)
            
def allocateSamples(prop, n):
    nSamp = len(prop)
    outList = []
    nRemain = n
    cc  = 0
    for i in range(nSamp):
        p_i = prop[i] / np.sum(prop[i:])
        s_i = np.round(p_i * nRemain, 0).astype(int)
        cc += s_i
        nRemain = nRemain - s_i
        outList.append(s_i)
        print('pi', p_i, 'si', s_i, 'cc', cc, 'nRem', nRemain)
    print('sum', np.sum(outList))
    return(outList)



class SimTrap(object):
    def __init__(self):
        self.g0 = 0.1
        self.sigma = 50.0
        self.sqDist = 4950
        self.scaleFact = 4.0
        self.maxDist = self.scaleFact * self.sigma
        self.resol = 5
        self.trapSuccessRate = 0.97
        self.nCells = 1000 / 50
        self.trapNights = 50

        self.circXY()
        self.sqXY()
        self.probDetect()
        self.pdNTraps()

    def circXY(self):
        x = np.arange(-self.maxDist, self.maxDist + self.resol, self.resol)
        y = np.arange(self.maxDist, -self.maxDist - self.resol, -self.resol)
        nxy = len(x)
        self.xx = np.tile(x, nxy)
        self.yy = np.repeat(y, nxy)

        self.dist = np.sqrt(self.xx**2 + self.yy**2)
        self.dist = self.dist[self.dist <= self.maxDist]

        circArea = np.pi * self.maxDist**2
        self.prpArea = circArea / 1.0e6
        print('circArea', circArea, 'prpArea', self.prpArea)
#        xxyy = np.empty((len(self.xx), 2))
#        xxyy[:,0] = self.xx
#        xxyy[:,1] = self.yy 
#        print('xxyy', xxyy)

    def sqXY(self):
        x = np.arange(-self.sqDist, self.sqDist + self.resol, self.resol)
        y = np.arange(self.sqDist, -self.sqDist - self.resol, -self.resol)
        nxy = len(x)
        self.sqxx = np.tile(x, nxy)
        self.sqyy = np.repeat(y, nxy)

    def probDetect(self):
        self.pDetCirc = self.g0 * np.exp(-(self.dist**2) / 2.0 / (self.sigma**2))
        self.mnPDetCirc = np.mean(self.pDetCirc) * self.prpArea
        self.totPDetCirc = self.mnPDetCirc * self.prpArea
        self.theoryPDetect = self.g0 / self.scaleFact / 2.0

        self.pdTotAreaTheory = self.g0 * np.pi * 2.0 * self.sigma**2 / 1.0e6

        print('mnPDetCirc', self.mnPDetCirc, 'theoryPD', self.theoryPDetect, 
            'pdTotAreaTheory', self.pdTotAreaTheory)

        self.pDetSq = (self.g0 * 
            np.exp(-(self.sqxx**2 + self.sqyy**2) / 2.0 / (self.sigma**2)))

        self.mnPDetSq = np.mean(self.pDetSq) 

        print('mnPDetSq', self.mnPDetSq)

    def pdNTraps(self):
        self.nTraps = np.arange(0.1, 2.1, .1) * self.trapSuccessRate
        ###########################
        self.trapsPD = (1.0 - (1.0 - self.pdTotAreaTheory)**
            (self.nTraps*self.nCells*self.trapNights))
        ###########################
        print('trapsPD', np.column_stack((self.nTraps, self.trapsPD)))
        P.figure()
        P.plot(self.nTraps, self.trapsPD)
        P.show()


class EmigrationRat(object):
    def __init__(self):
        """
        Explore function and parameters for emigration
        """
        rodent = np.arange(20, 500)        ## 500.     # Beech forest
        gamma = .2            ##.75
        tau = 0.001
        delt = 0.4  #1625.0
#        PrEmig = 1.0 - np.exp(-stoatCont**2 / rodent**gamma)
        PrEmig = (1.0 - np.exp(-stoatCont*gamma)) * np.exp(-rodent*tau)

        sDiff = np.abs(stoatCont - 7.0)
        minSDiff = np.min(sDiff)
        maskS = sDiff == minSDiff
        emK = PrEmig[maskS]
        sK = stoatCont[maskS]
        P.figure(figsize=(7,6))
        P.plot(stoatCont, PrEmig, color = 'k')
        P.vlines(x = sK, ymin=0.0, ymax = emK, linestyles = 'dashed', 
            colors='k')
        P.hlines(y = emK, xmin=0.0, xmax = sK, linestyles = 'dashed', 
            colors='k')
    
        print('pEm stoat', PrEmig[maskS])

        P.xlabel('$S_{k,t}$ ($km^{-2}$)')
        P.ylabel('$pEm^{[S]}_{k,t}$', rotation = 0, labelpad = 20)
        plotFname = 'pEmigrationStoat.png'
        P.savefig(plotFname, format='png', dpi=600)
        P.show()

        #### show prob of emigrating from i to j with distance and R
        R = 5 #np.array([7500, 900, 300, 75])
        dij = np.arange(7000.)/1000
        ax1 = P.gca()
#        ax1.figure(figsize=(8,6))
#        relPr = np.exp(-stoat[0]**2 / rodent**tau) * np.exp(-(dij) / delt)
        Pr = np.zeros((4, len(dij)))
        for i in range(4):
            Pr[i] = (np.exp(-stoat[i]*gamma) * (1- np.exp(-rodent*tau)) * 
                    np.exp(-(dij) * delt))
        sumPr = np.sum(Pr, axis = 0)
        relPr = Pr / np.min(Pr)
        lns0 = ax1.plot(dij, relPr[0], label = str(stoat[0]) + ' Stoats ' + '$km^{-2}$')
#        relPr = np.exp(-stoat[1]**2 / rodent**tau) * np.exp(-(dij) / delt)
##        relPr = np.exp(-stoat[1]*gamma) * (1- np.exp(-rodent*tau)) * np.exp(-(dij) / delt)
        lns1 = ax1.plot(dij, relPr[1], label = str(stoat[1]) + ' Stoats ' + '$km^{-2}$')
#        relPr = np.exp(-stoat[2]**2 / rodent**tau) * np.exp(-(dij) / delt)
##        relPr = np.exp(-stoat[2]*gamma) * (1- np.exp(-rodent*tau)) * np.exp(-(dij) / delt)
        lns2 = ax1.plot(dij, relPr[2], label = str(stoat[2]) + ' Stoats ' + '$km^{-2}$')
#        relPr = np.exp(-stoat[3]**2 / rodent**tau) * np.exp(-(dij) / delt)
##        relPr = np.exp(-stoat[3]*gamma) * (1- np.exp(-rodent*tau)) * np.exp(-(dij) / delt)
        lns3 = ax1.plot(dij, relPr[3], label = str(stoat[3]) + ' Stoats ' + '$km^{-2}$')
        lns = lns0 + lns1 + lns2 + lns3 
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs, loc = 'upper right')
        ax1.set_xlabel('Distance cell $k$ to cell $l$ (km)')
        ax1.set_ylabel('$RelProbEm^{[S]}_{k,l,t}$')
        plotFname = 'stoatRelPrEm.png'
        P.savefig(plotFname, format='png', dpi=600)

        P.show()
    
class Emigration(object):
    def __init__(self):
        """
        Explore function and parameters for emigration
        """
        KRatio = np.arange(0.01, 2.0, 0.01)
        gammaPara = 0.3
#        PrEmig = inv_logit(gammaPara[0] + gammaPara[1] * KRatio)
        PrEmig = 1.0 - np.exp(-gammaPara * KRatio)  
        horizLine = PrEmig[KRatio == 1.0]
        vertLine = 1.0
        print('prob at ratio = 1', horizLine)
        P.figure(figsize=(7,6))
        P.plot(KRatio, PrEmig, color = 'k')
        P.vlines(x = 1.0, ymin=0.0, ymax = horizLine, linestyles = 'dashed', 
            colors='k')
        P.hlines(y = horizLine, xmin=0.0, xmax = 1.0, linestyles = 'dashed', 
            colors='k')
        P.xlabel('Ratio $R_{i,t}$ : $K^{[R]}_{i,t}$')
        P.ylabel('$pEm^{[R]}_{i,t}$', rotation = 0, labelpad = 20)
        plotFname = 'pEmigration.png'
        P.savefig(plotFname, format='png', dpi=600)
        P.show()


class SpatialRemoval(object):
    def __init__(self):
        """
        SPATIAL REMOVAL PROCESS -SOPHIE AND ECOSYSTEM SERVICES
        """
        self.gam = np.array([.4, 1.0, 1.8])
        self.dist = np.arange(20, 400)
        self.nDist = len(self.dist)
        self.nGam = len(self.gam)
        print('ngam', self.nGam, 'gam', self.gam)
        self.rem = np.zeros((self.nDist, self.nGam))
        self.nTraps = 2
        P.figure()
        for j in range(self.nGam):
            gam_j = self.gam[j]
            for i in range(self.nDist):
                rem_i = np.exp(-(self.dist[i]/100)**2 / 2.0 / gam_j**2) 
                self.rem[i, j] = rem_i * self.nTraps
            P.plot(self.dist, self.rem[:, j])
            P.xlabel('Distance')
            P.ylabel('Rem')
        P.show()


class CorreRandVar(object):
    def __init__(self):
        np.random.seed(0) # Set seed
        # Inputs
        samples = 1000
        # Random samples (Uniformly distributed)
        U1 = np.random.rand(samples,1)
        U2 = np.random.rand(samples,1)
        # Random samples (normally distributed uncorrelated) 
        S1 = np.sqrt(-2*np.log(U1))*np.cos(2*np.pi*U2)
        S2 = np.sqrt(-2*np.log(U1))*np.sin(2*np.pi*U2)
        E_S1 = np.mean(S1)
        Var_S1 = np.mean(S1**2) - E_S1**2

        print('E_S1', E_S1, Var_S1)
        P.Figure()
        P.hist(S1)
        P.show()
        


        sigma_S1 = np.sqrt(Var_S1)
        E_S2 = np.mean(S2)
        Var_S2 = np.mean(S2**2) - E_S2**2
        sigma_S2 = np.sqrt(Var_S2)
        Cov_S1_S2 = np.mean(S1*S2) - E_S1*E_S2
        Corr_S1_S2 = Cov_S1_S2/sigma_S1/sigma_S2
        print('corr(S1,S2) = ' + str(Corr_S1_S2))
        # Correlated random samples
        mu_x = 5.0
        mu_y = 50.0
        sigma_x = 1.0
        sigma_y = 10.0
        rho = -0.4
        X = mu_x + sigma_x * S1
        Y = mu_y + sigma_y * (rho*S1 + np.sqrt(1-rho**2)*S2)
        E_X = np.mean(X)
        Var_X = np.mean(X**2) - E_X**2
        sigma_X = np.sqrt(Var_X)
        E_Y = np.mean(Y)
        Var_Y = np.mean(Y**2) - E_Y**2
        sigma_Y = np.sqrt(Var_Y)
        Cov_X_Y = np.mean(X*Y) - E_X*E_Y
        Corr_X_Y = Cov_X_Y/sigma_X/sigma_Y
        print('corr(X,Y) = ' + str(Corr_X_Y))
        P.figure()
        P.subplot(1,2,2)
        P.plot(X,Y, 'ko')

        ## different approach
        x = np.random.normal(0, 1, 1000)
        y = np.random.normal(0, 1, 1000)
        XX = mu_x + sigma_x * x
        YY = mu_y + sigma_y * (rho*x + np.sqrt(1-rho**2)*y)
        E_X = np.mean(XX)
        Var_X = np.mean(XX**2) - E_X**2
        sigma_X = np.sqrt(Var_X)
        E_Y = np.mean(YY)
        Var_Y = np.mean(YY**2) - E_Y**2
        sigma_Y = np.sqrt(Var_Y)
        Cov_X_Y = np.mean(XX*YY) - E_X*E_Y
        Corr_X_Y = Cov_X_Y/sigma_X/sigma_Y
        P.subplot(1,2,2)
        P.plot(XX,YY, 'bo')
        print('corr(XX,YY) = ' + str(Corr_X_Y))

        P.subplot(2,2,3)
        P.hist(XX)
        P.show()

def corrVar():
    x = np.random.uniform(0,20, 1000)
    y = np.random.uniform(5,20, 1000)
    meanX = np.mean(x)
    sdX = np.std(x)
    x_s = (x - meanX) / sdX
    y_s = (y - np.mean(y)) / np.std(y)
    print('mean xs', np.mean(y_s), np.std(y_s))

    mu_x = 5.0
    mu_y = 50.0
    sigma_x = 1.0
    sigma_y = 10.0
    rho = -0.4
    XX = mu_x + sigma_x * x_s
    YY = mu_y + sigma_y * (rho*x_s + np.sqrt(1-rho**2)*y_s)
    P.figure()
    P.plot(XX, YY, 'ko')
    P.show()


def gen_CorrRandom():
    """
    Paramters
    ---------
    n : int
        number of samples generated
    d : int
        dimensionality of samples
    covar : float
        uniform covariance for samples
    
    Returns
    -------
    samples : np.ndarray
        samples in as (n, d)-matrix
    """
    n = 1000
    d = 2
    covar = -0.4
    sig = np.array([1,10]).res
    mn = np.array([5.0, 50.0])

    cov_mat = np.ones((d, d)) * covar #np.fill_diagonal(cov_mat, 1)
    print('cov_mat', cov_mat)

    np.fill_diagonal(cov_mat, 1)
    print('cov_mat', cov_mat)
    
    cov_mat = cov_mat*sig
    print('cov_mat', cov_mat)

    rv = np.random.multivariate_normal(mn, cov_mat, size=n)
#    print('rv', rv)

    P.figure()
    P.plot(rv[:,0], rv[:,1], 'ko')
    P.show()


def genPositiveRandNormal():
    m = 7
    v = 15
    mu = np.log(m**2 / np.sqrt(m**2 + v))
    sig = np.sqrt(np.log(1 + (v / m**2)))
    print('mu and sig', mu, sig)
    r = np.random.normal(mu, sig, 10000)
    expR = np.exp(r)
    print('mean r', np.mean(expR))
    print('var r', np.std(expR)**2)


def numbaTest():
    n = 25
    a = np.random.uniform(0, n, n)
#    print('a', a)
#    minNumba(n, a)
    A = np.random.normal(0, 2, 10000000)
    startTime = time.time()
    s = prange_test(A)
    endTime = time.time()
    execTime = endTime - startTime
    print('s', s, 'mean', np.mean(s), 'exec time', execTime)

    startTime = time.time()
    r = rangeTest(A)
    endTime = time.time()
    execTime = endTime - startTime
    print('r', r, 'exec time', execTime)


@njit
def minNumba(n,a):
    for i in range(n):
        minID = np.argmin(a)
        print('min', minID)



@njit(parallel=True)
def prange_test(A):
    s = 0
    # Without "parallel=True" in the jit-decorator
    # the prange statement is equivalent to range
    for i in prange(A.shape[0]):
        s += A[i]
    return s

@njit
def rangeTest(A):
    r = 0
    for i in range(A.shape[0]):
        r += A[i]
    return r


def distTrial():
    x0 = np.arange(-400,450,50 )/1000
    y0 = np.arange(400,-450,-50 )/1000
    nx = len(x0)
    x = np.tile(x0, nx)
    y = np.repeat(y0, nx)
    print(x, y)
    d = distFX(0, 0, x, y)
    d = d[d<=.4]
    print(d)
    gam = np.arange(0.02, 0.40, 0.01)
    nGam = len(gam)
    habEff = np.zeros(nGam)
    for i in range(nGam):
        habEff[i] = np.sum(np.exp(-(d**2) / 2 / gam[i]**2))
        print('gam', gam[i], 'hab', habEff[i])
    P.Figure()
    P.plot(gam, habEff)
    P.show()

    dist = np.arange(0, 100.25, .25)
    gam = [20, 4]
    P.figure(figsize=(8,8))
    for i in range(2):
        eff = np.exp(-dist**2 / 2.0 / gam[i]**2)
        P.plot(dist, eff, label = 'gamma =' + str(gam[i]))
        effmask = eff < .01
        minDist = np.min(dist[effmask])
        print('gam', gam[i], 'mindist', minDist)
        zeromask = eff < .005
        minDist = np.min(dist[zeromask])
        print('gam', gam[i], 'zerodist', minDist) 
    P.xlabel('Distance (m)', fontsize = 14)
    P.ylabel('HabEff', fontsize = 14)
    P.legend(loc='upper right')
    P.savefig('gammaTest.png', format='png')
    P.show()





def residAlternate():
    n = 4   
    SSe = 0.55
    Y = 12
    pStar = np.arange(n, Y+1, dtype=int) 
    detectRange = np.arange(n, Y+1)     # n detectd
    matLen = len(pStar)
    pMat = np.zeros(matLen)

    for i in range(matLen):
        pMat[i] = stats.binom.pmf(n, pStar[i], SSe)

    pNotDet = 1.0 - pMat
    pY = 1.0 - np.prod(pNotDet)
    print('prob get 4 with 12', pY, 'pMat', pMat, 'pNotDet', pNotDet)    


def estResidualPop():
    n = 4
    SSe = .6
    Y = 18
    pStar = np.arange(n, Y+1, dtype=int)           #n trials
    detectRange = np.arange(n, Y+1)     # n detectd
    matLen = len(pStar)
    pMat = np.zeros((matLen, matLen))
    
    for i in range(matLen):
        pMat[:,i] = stats.binom.pmf(detectRange, pStar[i], SSe)
#    print('pMat', np.round(pMat, 3))
    sumPMF = np.sum(pMat, axis= 0)

    print('Reset prior value if detected all', np.round(sumPMF[0],3))

    CL = [0.95, 0.975, 0.99]
    ## GET 95%, 97.5, AND 99% CL
    for j in range(3):
        maskPositive = sumPMF >= CL[j]
        diff = sumPMF - CL[j]
        minPos = np.min(diff[maskPositive])
        indx = np.squeeze(np.where(diff == minPos))
        print(CL[j],' CL for residual occupied cells = ', pStar[indx])
        if j == 0:
            indx95 = indx
    print('indx95', indx95)

    P.figure(figsize=(13,6))
    psMask = pStar <= indx95
    ps = pStar[psMask]
    
    nZeros = np.zeros(n, dtype = int)
    P.subplot(1,2,1)
    P.plot(np.int32(np.arange(n)), nZeros, 'ro-')
#    P.plot(np.int32(np.arange(n+1)), np.int32(np.append(nZeros, pMat[0,indx95])), 'ro-')
    P.plot(np.int32(pStar), pMat[:,indx95], 'ko-')
    tooHighX = np.arange(n+1+indx95, Y+1)
    pTooHigh = np.zeros(len(tooHighX))
    P.plot(tooHighX, pTooHigh, 'ro-')

    P.vlines(x = (indx95 + n), ymin=0.0, ymax = np.max(pMat[:,indx95]), 
            linestyles = 'dashed', colors='k')
    P.vlines(x = n, ymin=0.0, ymax = np.max(pMat[:,indx95]), linestyles = 'dashed', 
            colors='k')
    P.xticks(np.arange(0, Y+1, 2), fontsize = 14)
    P.yticks(fontsize=14)
    P.xlabel('Number detected', fontsize = 16)
    P.ylabel('Probability mass', fontsize = 16)
    P.title('(A)', loc = 'left')
    P.subplot(1,2,2)
    
    P.plot(pStar, sumPMF, 'ko-')
    xVals = np.arange(Y)
    P.xticks(xVals[::2], [int(i) for i in xVals[::2]], fontsize = 14)
    P.hlines(y=0.95,xmin=0, xmax=Y, linestyles = 'dashed', colors='k')
    P.xlabel('Residual population', fontsize = 16)
    P.ylabel('Probability $P(N \\leq Y)$', fontsize = 16)
    P.title('(B)', loc = 'left')
    P.savefig('ResidPopulation.png', format='png')
    P.show()

    P.figure(figsize=(9,7))
    P.plot(pStar, sumPMF, 'ko-')
    xVals = np.arange(Y)
    P.xticks(xVals[::2], [int(i) for i in xVals[::2]], fontsize = 14)
    P.yticks(fontsize = 14)
    P.hlines(y=0.95,xmin=0, xmax=Y, linestyles = 'dashed', colors='k')
    P.xlabel('Remaining infected herds', fontsize = 14)
    P.ylabel('Probability', fontsize = 14)
    P.savefig('RemainInfHerds.png', format='png')
    P.show()




def binomFX():
    k = 2
    N = 12
    p = .33
    possArr = np.arange(30)
    bPMF = stats.binom.pmf(possArr, N, p)
    cumS = np.cumsum(bPMF)
    print('bPMF', bPMF, 'cumS', cumS)
    P.figure()
    P.plot(possArr, cumS)
    P.show()

def dispersalSteps():

    nSims = 10000
    nSteps = 36
    wShape = 1.0
    wScale = 50.0
    ## VON MISES KAPPA DIRECTION PARAMETER
    kappaDirection = 0.5
    storeSteps = np.zeros(nSims * nSteps)
    storeDispDist = np.zeros(nSims)
    cc = 0
    for i in range(nSims):
        x = 0.0
        y = 0.0
        for j in range(nSteps):
            if j == 0:
                bearing = np.random.uniform(-np.pi, np.pi)
            else:
                bearing = np.random.vonmises(bearing, kappaDirection)

            stepLength = wScale * np.random.weibull(wShape)
            xDist = np.sin(bearing) * stepLength
            yDist = np.cos(bearing) * stepLength
            x = x + xDist
            y = y + yDist
            storeSteps[cc] = stepLength
#            print('stepLength', stepLength)
            cc += 1
        dispDist = np.sqrt(x**2 + y**2)
        storeDispDist[i] = dispDist
    print('mean step length', np.mean(storeSteps))
    print('mean disp distan', np.mean(storeDispDist))

    P.figure(figsize=(13,7))
    P.subplot(1,2,1)
    P.hist(storeDispDist, bins=40)
    P.xlabel('Dispersal distance')
    P.ylabel('Frequency')

    P.subplot(1,2,2)
    P.hist(storeSteps, bins=40)
    P.xlabel('Step length')
    P.savefig('dispersalSteps.png', format='png')
    P.show()


class BetaBinomial(object):
    def __init__(self):
 
        self.pertPara = {0:[.2, .1], 
                        1 : [.25, .15], 
                        2 : [.30, .2], 
                        3 : [.35, .25]}

        self.n = len(self.pertPara)
        self.trial =  10
        self.observed = 6

        self.doTrials()

    def doTrials(self):
        P.figure(figsize=(11,9))
        self.baseLine = np.arange(.001, .999, .001)
        for i in range(self.n):
            self.pert_i = self.pertPara[i]
#            print('i', i, 'beta mn sd',  self.pert_i)
#            self.getPriorAB(i)
            self.get_beta()
            self.getPosterior(i)
            self.makeGraph(i)
        P.suptitle('Trials = {} and Detected = {}'.format(self.trial, self.observed),
            fontsize = 18)
        P.tight_layout()
        P.savefig('detectionTrials.png', format='png')
        P.show()

    def get_beta(self):
        """
        get alpha and beta values frm mean and sd
        """
        a = self.pert_i[0] * ( ((self.pert_i[0] * (1.0 - self.pert_i[0])) / (self.pert_i[1]**2.0)) - 1.0)
        b = (1.0-self.pert_i[0])*((self.pert_i[0]*(1.0-self.pert_i[0]))/self.pert_i[1]**2.0 - 1.0)
        self.ab = np.array([a,b])
#        print('prior a b', self.ab)
#        self.ab = [1,1]


    def getPriorAB_NOT_USED(self, i):
        self.ab = np.zeros(2)
        sh = 4
        mu = (self.pert_i[0] + self.pert_i[2] + sh * self.pert_i[1]) / (sh + 2.0)        
        if mu == self.pert_i[1]:
            self.ab[0] = 1.0 + sh / 2.0
        else:
            self.ab[0] = ((mu - self.pert_i[0]) * (2.0 * self.pert_i[1] - self.pert_i[0] - 
                self.pert_i[2]) / ((self.pert_i[1] - mu) * (self.pert_i[2] - self.pert_i[0])))
        self.ab[1] = self.ab[0] * (self.pert_i[2] - mu) / (mu - self.pert_i[0])
#        print('i', i, 'prior a b', self.ab)

    def getPosterior(self, i):
        self.priorLine = stats.beta.pdf(self.baseLine, self.ab[0], self.ab[1])
        self.post_a = self.observed + self.ab[0]
        self.post_b = self.trial - self.observed + self.ab[1]
        self.postLine = stats.beta.pdf(self.baseLine, self.post_a, self.post_b)
#        print('i', i, 'post a b', self.post_a, self.post_b)

    def makeGraph(self, i):
        P.subplot(2,2, i+1)
        meanPrior = self.pert_i[0]
        maxPost = self.baseLine[self.postLine == np.max(self.postLine)][0]
        P.plot(self.baseLine, self.priorLine, 'k--', linewidth=2)        
        P.plot(self.baseLine, self.postLine, color='r', linewidth=3)
        P.title('Prior = {:.2f} and Posterior = {:.2f}'.format(meanPrior, maxPost))         
        if (i == 0) or (i == 2):
            P.ylabel('Density')
        if i > 1:
            P.xlabel('Probability of detection')
#        print('i', i, 'Max Post', np.max(self.postLine), meanPrior)
        P.savefig('pDetection{}_{}.png'.format(self.trial,self.observed), format='png')

class TestZip(object):
    def __init__(self):

        self.setParams()    
        self.getDevices()
        self.loopIter()

    def setParams(self):
        self.iter = 200
        self.n = 60
        self.g0 = 0.1
        self.sigma = 40.0
        self.trapNights = 20

    def getDevices(self):
        self.hectarePerDevice = 1
        self.sideDist = 10000   #m
        self.gridSpace = np.sqrt(self.hectarePerDevice * 100**2)
        print('grid', self.gridSpace)
        self.maxX = 10000
        self.maxY = 10000
        x = np.arange(self.gridSpace/2.0, 10000.0, self.gridSpace)
        nX = len(x)
        self.x = np.repeat(x, nX)
        self.y = np.tile(x, nX)

        ## SIM APPROACH
        self.intensity = 1 / self.hectarePerDevice / 4
        nDevices = 1/self.hectarePerDevice*100
        prpArea = nDevices * np.pi * (4*self.sigma)**2 / 1000**2

        pdOne = self.g0 * 2.0 * np.pi* self.sigma**2 / 400 / 50**2
#        pdOne = self.g0 * 2.0 * np.pi* self.sigma**2 / 400 / 50**2 * prpArea
        self.seD = (1 - (1.0 - pdOne)**(self.intensity * self.trapNights * 400))
        print('seD', self.seD, 'pdOne', pdOne)

    def loopIter(self):
        self.nDetect = np.zeros(self.iter)
        self.NDetectSim = np.zeros(self.iter)
        minMax = [500, 500]
        ptBuffer = self.sigma/2
        if ptBuffer < 0:
            ptBuffer = 0
        for i in range(self.iter):
            randX = np.random.uniform((np.min(self.x) - ptBuffer), (np.max(self.x) + ptBuffer), self.n)
            randY = np.random.uniform((np.min(self.x) - ptBuffer), (np.max(self.x) + ptBuffer), self.n)
            if np.min(randX) < minMax[0]:
                minMax[0] = np.min(randX)
            if np.max(randX) > minMax[1]:
                minMax[1] = np.max(randX)

#            if i == 0:
#                P.figure()
#                P.scatter(self.x, self.y)
#                P.scatter(randX, randY, color='r')
#                P.show()


            
            dist = distFX(self.x, self.y, randX, randY) # trap on axis 0, individ axis 1
            pDetect = self.g0 * np.exp(-(dist**2) / 2.0/ self.sigma**2)
            pNoDetect = (1.0 - pDetect)**self.trapNights
            pNoDetectTotal = np.prod(pNoDetect, axis=0)
            pDetectInd = 1.0 - pNoDetectTotal
            self.nDetect[i] = np.sum(np.random.binomial(1, pDetectInd))

            ## SIM APPROACH
            self.NDetectSim[i] = np.random.binomial(self.n, self.seD)
#            print('i', i, 'Zip det', self.nDetect[i], 'Sim Detect', self.NDetectSim[i])

        print('prob detection ZIP', np.mean(self.nDetect / self.n))
        print('Sim Prob Detect', np.mean(self.NDetectSim / self.n))
        print('minMax', minMax)



class SimRatDyn(object):
    def __init__(self):
        """
        rat dynamics in 4 ha block
        """
        self.N = np.array([.5, 1, 3, 4]) 
#        self.N = np.array([1,3,6,9,15, 20])
        self.K = 4.0   #1750.0    #np.array([2., 3., 4., 5., 7., 20.0])
#        self.KArray = np.array([7500, 900, 450, 300, 150, 75], dtype = int)
        self.nYears = 7
        self.recruitDecay = 1.75     #1.0         
        self.perCapRec = 3.0        #2.5           
        self.adultSurv = np.exp(-0.5)        # constant annual survival rate (exp model)
        self.adultSurvDecay = 2.6   #2.0   #1.2      

        self.plotAdultSurv()

        self.plotProbRec()
        self.simPop()

#        self.simRatSurv()
#        self.simStochastic()
#        self.plotPrpSurv()


    def simRatSurv(self):
        nYears = 6
        years = np.arange(1,nYears)
        prpSurv = np.exp(-years * self.adultSurv)
        P.figure(figsize=(11,9))
        P.plot(years, prpSurv, color = 'k', linewidth = 4.0)
        P.xlabel('Age (years)')
        P.ylabel('Mean age specific survival')
        FName = 'ratAgeSurv.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()


    def simPop(self):
        self.nCells = len(self.N)
        self.pop = np.zeros((self.nYears, self.nCells))
        self.meanN = np.mean(self.N)
        self.meanPop = np.zeros(self.nYears)
        N = self.N.copy()
        print('N', N)
        for i in range(self.nYears):
            ## sim cells
            surv_i = self.adultSurv * (np.exp(-N**2 / 
                self.K**self.adultSurvDecay))
            NStar = N * surv_i
            pMaxRec = np.exp(-NStar**2 / self.K**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            nRecruit = NStar * recRate

###            N = NStar * np.exp(self.r * (1.0 - 
###                    (NStar**2 / self.KArray[2]**self.recruitDecay)))

            N = nRecruit + NStar
            self.pop[i] = N
#            self.pop[i] = np.random.poisson(self.N)
#            if i > 0:
#                print('i', i,  (self.pop[i,:3] - self.pop[(i-1), :3]) / self.pop[(i-1), :3
#                    'surv_i', surv_i[:2])
            print('i', i, 'N', N)           
            ## sim means
            meanSurv = self.adultSurv * (np.exp(-self.meanN**2 / 
                self.K**self.adultSurvDecay))
            meanNStar = self.meanN * meanSurv
            pMaxRec = np.exp(-meanNStar**2 / self.K**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            nRecruit = meanNStar * recRate
            self.meanN = nRecruit + meanNStar
            self.meanPop[i] = self.meanN
        self.plotSimPop()
        pChange = (self.pop[-1] - self.pop[0]) / self.pop[0]
        print('last population', self.pop[-1])

    def plotSimPop(self):
        P.figure()
        years = np.arange(1, self.nYears + 1)
        for i in range(self.nCells):
            P.plot(years, self.pop[:, i], color = 'k', linewidth = 2)
#        P.plot(years, self.meanPop, color = 'k', linewidth = 3)
        P.xlabel('Years')
        P.ylabel('Rat density ' + '($ha^{-1}$)')
        FName = 'simRatDynamics.png'
        P.savefig(FName, format='png', dpi = 300)
        P.show()  
                  
    def plotAdultSurv(self):
        n = np.arange(1, 6, 0.1)
        nHa = n.copy()
        pMaxSurv = (np.exp(-n**2 / self.K**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.figure(figsize=(11,9))
        P.plot(nHa, sur, color = 'k', linewidth = 4)
        P.xlabel('Rat density ($ha^{-1}$)')
#        P.xlabel('Rat density')
        P.ylabel('Prop. adult surv')
                  
        FName = 'prpRatAdultSurviving.png'
        P.savefig(FName, format='png', dpi = 300)
        P.show()   
                  
    def plotProbRec(self):
        P.figure(figsize = (11,9))
        R = np.arange(1, 6.0, 0.1)
        R_Ha = R.copy()
        self.pMaxRec = np.exp(-R**2 / self.K**self.recruitDecay)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')

        FName = 'probabilityRatRecruit.png'
        P.savefig(FName, format='png', dpi = 300)
        P.show()  
                
               
    def simStochastic(self):
        n = 10    
        seed = 450
        nYears = 200
        years = np.arange(nYears)
        nArray0 = np.zeros(nYears)
        P.figure(figsize=(13,11))
        for i in range(nYears):
            if i % 10 == 0:
                seed_i = 7500
#            elif i % 10 == 1:
#                seed_1 = 600
            else: 
                seed_i = seed 
            surv_i = self.adultSurv * (np.exp(-n**2 / 
                seed_i**self.adultSurvDecay))
            NStar = n * surv_i
            pMaxRec = np.exp(-NStar**2 / seed_i**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            mu = NStar * (1 + recRate)
            n = np.random.poisson(mu)
            nArray0[i] = n
#            print('i', i, 'n', n, 'year', years[i], 'narr', nArray0[i])
        P.subplot(1,2,1)
        P.plot(years, nArray0, color = 'k', linewidth = 4)
        P.ylim(0,80)
        n = 10
        years1 = np.arange(nYears)
        nArray1 = np.zeros(nYears)
        for i in range(nYears):
            if i % 10 == 0:
                seed_i = 5000
#            elif i % 10 == 1:
#                seed_1 = 600
            else: 
                seed_i = seed 
            surv_i = self.adultSurv * (np.exp(-n**2 / 
                seed_i**self.adultSurvDecay))
            NStar = n * surv_i
            pMaxRec = np.exp(-NStar**2 / seed_i**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            mu = NStar * (1 + recRate)
            n = np.random.poisson(mu)
            nArray1[i] = n
        P.subplot(1,2,2)
        P.plot(years1, nArray1, color = 'k', linewidth = 4)
        P.ylim(0,80)
        P.show()  
                  
    def plotPrpSurv(self):
        seed = np.arange(75, 7500)
        surv = self.adultSurv * (np.exp(-20**2 / 
                seed**self.adultSurvDecay))
        P.figure()
        P.plot(seed, surv)
        P.show()
                  

class SimStoatDyn(object):
    def __init__(self):
        """
        rat dynamics in 4 ha block
        """
        self.N = np.array([4, 4, 12, 14, 18, 22, 30]) 
#        self.N = np.array([1,3,6,9,15, 20])
#        self.K = 1750.0    #np.array([2., 3., 4., 5., 7., 20.0])
        self.KArray = np.array([7500, 900, 450, 300, 150, 75], dtype = int)
        self.nYears = 7
        self.recruitDecay = 1.0    #1.0        #.97     
        self.perCapRec = 2.5           #.8
        self.adultSurv = np.exp(-0.5)        # constant annual survival rate (exp model)
        self.adultSurvDecay = 1.2  # 1.05    

        self.plotAdultSurv()

        self.plotProbRec()
        self.simPop()

        self.simRatSurv()
        self.simStochastic()
        self.plotPrpSurv()


    def simRatSurv(self):
        nYears = 6
        years = np.arange(1,nYears)
        prpSurv = np.exp(-years * self.adultSurv)
        P.figure(figsize=(11,9))
        P.plot(years, prpSurv, color = 'k', linewidth = 4.0)
        P.xlabel('Age (years)')
        P.ylabel('Mean age specific survival')
        FName = 'ratAgeSurv.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()


    def simPop(self):
        self.nCells = len(self.N)
        self.pop = np.zeros((self.nYears, self.nCells))
        self.meanN = np.mean(self.N)
        self.meanPop = np.zeros(self.nYears)
        N = self.N.copy()
        print('N', N)
        for i in range(self.nYears):
            ## sim cells
            surv_i = self.adultSurv * (np.exp(-N**2 / 
                self.KArray[2]**self.adultSurvDecay))
            NStar = N * surv_i
            pMaxRec = np.exp(-NStar**2 / self.KArray[2]**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            nRecruit = NStar * recRate

###            N = NStar * np.exp(self.r * (1.0 - 
###                    (NStar**2 / self.KArray[2]**self.recruitDecay)))

            N = nRecruit + NStar
            self.pop[i] = N
#            self.pop[i] = np.random.poisson(self.N)
#            if i > 0:
#                print('i', i,  (self.pop[i,:3] - self.pop[(i-1), :3]) / self.pop[(i-1), :3
#                    'surv_i', surv_i[:2])
            print('i', i, 'N', N)           
            ## sim means
            meanSurv = self.adultSurv * (np.exp(-self.meanN**2 / 
                self.KArray[2]**self.adultSurvDecay))
            meanNStar = self.meanN * meanSurv
            pMaxRec = np.exp(-meanNStar**2 / self.KArray[2]**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            nRecruit = meanNStar * recRate
            self.meanN = nRecruit + meanNStar
            self.meanPop[i] = self.meanN
        self.plotSimPop()
        pChange = (self.pop[-1] - self.pop[0]) / self.pop[0]
        print('last population', self.pop[-1])

    def plotSimPop(self):
        P.figure()
        years = np.arange(1, self.nYears + 1)
        for i in range(self.nCells):
            P.plot(years, self.pop[:, i], color = 'k', linewidth = 2)
#        P.plot(years, self.meanPop, color = 'k', linewidth = 3)
        P.xlabel('rat dynamics in 4 ha blockYears')
        P.ylabel('Rat density ' + '(4 $ha)^{-1}$')
        P.title('simRatPop')
        FName = 'simRatDynamics.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()  
                  
    def plotAdultSurv(self):
        n = np.arange(1, 90., 0.1)
        nHa = n / 4.0
        pMaxSurv = (np.exp(-n**2 / self.KArray[0]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.figure(figsize=(11,9))
        P.subplot(2,2,1)
        P.subplots_adjust(hspace = 0.4, wspace = 0.2)
        P.plot(nHa, sur, color = 'k', linewidth = 4)
        P.title('(A)', loc = 'left')
#        P.title('        self.simRatSurv()Seed Density = ' + str(self.KArray[0]) + ' $m^{-
        P.xlabel('Rat density ($ha^{-1}$)')
#        P.xlabel('Rat density')
        P.ylabel('Prop. adult surv')
                  
        n = np.arange(1, 25, 0.1)
        nHa = n / 4.0
        pMaxSurv = (np.exp(-n**2 / self.KArray[2]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.subplot(2,2,2)
        P.plot(nHa, sur, color = 'k', linewidth = 4)
        P.title('(B)', loc = 'left')
        P.xlabel('Rat density ($ha^{-1})$')
        P.ylabel('Prop. adult surv')
                  
                 
        n = np.arange(1, 12, 0.05)
        nHa = n / 4.0
        pMaxSurv = (np.exp(-n**2 / self.KArray[-1]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.subplot(2,2,3)
        P.plot(nHa, sur, color = 'k', linewidth = 4)
        P.title('(C)', loc = 'left')
        P.xlabel('Rat density ($ha^{-1})$')
        P.ylabel('Prop. adult surv')
                  
        P.subplot(2,2,4)
        years = np.arange(1,6, 0.01)
        prpSurv = self.adultSurv**years
        P.plot(years, prpSurv, color = 'k', linewidth = 4.0)
        P.title('(D)', loc = 'left')
        P.xlabel('Age (years)')
        P.ylabel('Mean age-specific survival')
                  
        FName = 'prpRatAdultSurviving.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()   
                  
    def plotProbRec(self):
        P.figure(figsize = (11,9))
        R = np.arange(1, 90.0, 0.1)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[0]**self.recruitDecay)
        P.subplot(2,2,1)
        P.subplots_adjust(hspace = 0.4, wspace = 0.2)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(A)', loc='left')

        P.subplot(2,2,2)
        R = np.arange(1, 35.0, 0.1)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[1]**self.recruitDecay)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(B)', loc='left')
                  
        P.subplot(2,2,3)
        R = np.arange(1, 22.0, 0.05)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[3]**self.recruitDecay)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(C)', loc='left')
                  
        P.subplot(2,2,4)
        R = np.arange(1, 12.0, 0.05)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[-1]**self.recruitDecay)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(D)', loc='left')
                  
        FName = 'probabilityRatRecruit.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()  
                
               
    def simStochastic(self):
        n = 10    
        seed = 450
        nYears = 200
        years = np.arange(nYears)
        nArray0 = np.zeros(nYears)
        P.figure(figsize=(13,11))
        for i in range(nYears):
            if i % 10 == 0:
                seed_i = 7500
#            elif i % 10 == 1:
#                seed_1 = 600
            else: 
                seed_i = seed 
            surv_i = self.adultSurv * (np.exp(-n**2 / 
                seed_i**self.adultSurvDecay))
            NStar = n * surv_i
            pMaxRec = np.exp(-NStar**2 / seed_i**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            mu = NStar * (1 + recRate)
            n = np.random.poisson(mu)
            nArray0[i] = n
#            print('i', i, 'n', n, 'year', years[i], 'narr', nArray0[i])
        P.subplot(1,2,1)
        P.plot(years, nArray0, color = 'k', linewidth = 4)
        P.ylim(0,80)
        n = 10
        years1 = np.arange(nYears)
        nArray1 = np.zeros(nYears)
        for i in range(nYears):
            if i % 10 == 0:
                seed_i = 5000
#            elif i % 10 == 1:
#                seed_1 = 600
            else: 
                seed_i = seed 
            surv_i = self.adultSurv * (np.exp(-n**2 / 
                seed_i**self.adultSurvDecay))
            NStar = n * surv_i
            pMaxRec = np.exp(-NStar**2 / seed_i**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            mu = NStar * (1 + recRate)
            n = np.random.poisson(mu)
            nArray1[i] = n
        P.subplot(1,2,2)
        P.plot(years1, nArray1, color = 'k', linewidth = 4)
        P.ylim(0,80)
        P.show()  
                  
    def plotPrpSurv(self):
        seed = np.arange(75, 7500)
        surv = self.adultSurv * (np.exp(-20**2 / 
                seed**self.adultSurvDecay))
        P.figure()
        P.plot(seed, surv)
        P.show()
                  
            

class SimDensityDependence(object):
    def __init__(self):
        """
        rat dynamics in 1 km - 100 ha block
        """
#        self.N = np.array([4, 4, 12, 14, 18, 22, 30]) 
        self.N = 200    #np.array([1,3,6,9,15, 20])
#        self.K = 450.0    #np.array([2., 3., 4., 5., 7., 20.0])
#        self.KArray = np.array([7500, 900, 450, 300, 150, 75], dtype = int)
        self.nYears = 7
        self.recruitDecay = 1.0    #1.0        #.97     
        self.perCapRec = 3.0           #.8
        self.adultSurv = np.exp(-0.5)        # constant annual survival rate (exp model)
        self.adultSurvDecay = [.1, .6, 1.0, 1.5]  # 1.05    

        self.plotAdultSurv()

        self.plotProbRec()
        self.simPop()

        self.simRatSurv()
        self.simStochastic()
        self.plotPrpSurv()


    def simRatSurv(self):
        nYears = 6
        years = np.arange(1,nYears)
        prpSurv = np.exp(-years * self.adultSurv)
        P.figure(figsize=(11,9))
        P.plot(years, prpSurv, color = 'k', linewidth = 4.0)
        P.xlabel('Age (years)')
        P.ylabel('Mean age specific survival')
        FName = 'ratAgeSurv.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()


    def simPop(self):
        self.nCells = len(self.N)
        self.pop = np.zeros((self.nYears, self.nCells))
        self.meanN = np.mean(self.N)
        self.meanPop = np.zeros(self.nYears)
        N = self.N.copy()
        print('N', N)
        for i in range(self.nYears):
            ## sim cells
            surv_i = self.adultSurv * (np.exp(-N**2 / 
                self.KArray[2]**self.adultSurvDecay))
            NStar = N * surv_i
            pMaxRec = np.exp(-NStar**2 / self.KArray[2]**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            nRecruit = NStar * recRate

###            N = NStar * np.exp(self.r * (1.0 - 
###                    (NStar**2 / self.KArray[2]**self.recruitDecay)))

            N = nRecruit + NStar
            self.pop[i] = N
#            self.pop[i] = np.random.poisson(self.N)
#            if i > 0:
#                print('i', i,  (self.pop[i,:3] - self.pop[(i-1), :3]) / self.pop[(i-1), :3
#                    'surv_i', surv_i[:2])
            print('i', i, 'N', N)           
            ## sim means
            meanSurv = self.adultSurv * (np.exp(-self.meanN**2 / 
                self.KArray[2]**self.adultSurvDecay))
            meanNStar = self.meanN * meanSurv
            pMaxRec = np.exp(-meanNStar**2 / self.KArray[2]**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            nRecruit = meanNStar * recRate
            self.meanN = nRecruit + meanNStar
            self.meanPop[i] = self.meanN
        self.plotSimPop()
        pChange = (self.pop[-1] - self.pop[0]) / self.pop[0]
        print('last population', self.pop[-1])

    def plotSimPop(self):
        P.figure()
        years = np.arange(1, self.nYears + 1)
        for i in range(self.nCells):
            P.plot(years, self.pop[:, i], color = 'k', linewidth = 2)
#        P.plot(years, self.meanPop, color = 'k', linewidth = 3)
        P.xlabel('rat dynamics in 4 ha blockYears')
        P.ylabel('Rat density ' + '(4 $ha)^{-1}$')
        P.title('simRatPop')
        FName = 'simRatDynamics.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()  
                  
    def plotAdultSurv(self):
        n = np.arange(1, 90., 0.1)
        nHa = n / 4.0
        pMaxSurv = (np.exp(-n**2 / self.KArray[0]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.figure(figsize=(11,9))
        P.subplot(2,2,1)
        P.subplots_adjust(hspace = 0.4, wspace = 0.2)
        P.plot(nHa, sur, color = 'k', linewidth = 4)
        P.title('(A)', loc = 'left')
#        P.title('        self.simRatSurv()Seed Density = ' + str(self.KArray[0]) + ' $m^{-
        P.xlabel('Rat density ($ha^{-1}$)')
#        P.xlabel('Rat density')
        P.ylabel('Prop. adult surv')
                  
        n = np.arange(1, 25, 0.1)
        nHa = n / 4.0
        pMaxSurv = (np.exp(-n**2 / self.KArray[2]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.subplot(2,2,2)
        P.plot(nHa, sur, color = 'k', linewidth = 4)
        P.title('(B)', loc = 'left')
        P.xlabel('Rat density ($ha^{-1})$')
        P.ylabel('Prop. adult surv')
                  
                 
        n = np.arange(1, 12, 0.05)
        nHa = n / 4.0
        pMaxSurv = (np.exp(-n**2 / self.KArray[-1]**self.adultSurvDecay))
        sur = self.adultSurv * pMaxSurv
        P.subplot(2,2,3)
        P.plot(nHa, sur, color = 'k', linewidth = 4)
        P.title('(C)', loc = 'left')
        P.xlabel('Rat density ($ha^{-1})$')
        P.ylabel('Prop. adult surv')
                  
        P.subplot(2,2,4)
        years = np.arange(1,6, 0.01)
        prpSurv = self.adultSurv**years
        P.plot(years, prpSurv, color = 'k', linewidth = 4.0)
        P.title('(D)', loc = 'left')
        P.xlabel('Age (years)')
        P.ylabel('Mean age-specific survival')
                  
        FName = 'prpRatAdultSurviving.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()   
                  
    def plotProbRec(self):
        P.figure(figsize = (11,9))
        R = np.arange(1, 90.0, 0.1)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[0]**self.recruitDecay)
        P.subplot(2,2,1)
        P.subplots_adjust(hspace = 0.4, wspace = 0.2)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(A)', loc='left')

        P.subplot(2,2,2)
        R = np.arange(1, 35.0, 0.1)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[1]**self.recruitDecay)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(B)', loc='left')
                  
        P.subplot(2,2,3)
        R = np.arange(1, 22.0, 0.05)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[3]**self.recruitDecay)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(C)', loc='left')
                  
        P.subplot(2,2,4)
        R = np.arange(1, 12.0, 0.05)
        R_Ha = R / 4.0
        self.pMaxRec = np.exp(-R**2 / self.KArray[-1]**self.recruitDecay)
        P.plot(R_Ha, self.pMaxRec * self.perCapRec, color = 'k', linewidth = 4.0)
        P.xlabel('Rat Density ($ha^{-1}$)')
        P.ylabel('Per Cap Recruit')
        P.title('(D)', loc='left')
                  
        FName = 'probabilityRatRecruit.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()  
                
               
    def simStochastic(self):
        n = 10    
        seed = 450
        nYears = 200
        years = np.arange(nYears)
        nArray0 = np.zeros(nYears)
        P.figure(figsize=(13,11))
        for i in range(nYears):
            if i % 10 == 0:
                seed_i = 7500
#            elif i % 10 == 1:
#                seed_1 = 600
            else: 
                seed_i = seed 
            surv_i = self.adultSurv * (np.exp(-n**2 / 
                seed_i**self.adultSurvDecay))
            NStar = n * surv_i
            pMaxRec = np.exp(-NStar**2 / seed_i**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            mu = NStar * (1 + recRate)
            n = np.random.poisson(mu)
            nArray0[i] = n
#            print('i', i, 'n', n, 'year', years[i], 'narr', nArray0[i])
        P.subplot(1,2,1)
        P.plot(years, nArray0, color = 'k', linewidth = 4)
        P.ylim(0,80)
        n = 10
        years1 = np.arange(nYears)
        nArray1 = np.zeros(nYears)
        for i in range(nYears):
            if i % 10 == 0:
                seed_i = 5000
#            elif i % 10 == 1:
#                seed_1 = 600
            else: 
                seed_i = seed 
            surv_i = self.adultSurv * (np.exp(-n**2 / 
                seed_i**self.adultSurvDecay))
            NStar = n * surv_i
            pMaxRec = np.exp(-NStar**2 / seed_i**self.recruitDecay)
            recRate = self.perCapRec * pMaxRec
            mu = NStar * (1 + recRate)
            n = np.random.poisson(mu)
            nArray1[i] = n
        P.subplot(1,2,2)
        P.plot(years1, nArray1, color = 'k', linewidth = 4)
        P.ylim(0,80)
        P.show()  
                  
    def plotPrpSurv(self):
        seed = np.arange(75, 7500)
        surv = self.adultSurv * (np.exp(-20**2 / 
                seed**self.adultSurvDecay))
        P.figure()
        P.plot(seed, surv)
        P.show()
                  
                  
class NestedBinomial(object):
    def __init__(self):
        self.epsil = 1.0
        self.Tau = 20.0
        self.n = 4
        self.mu = inv_logit(np.random.uniform(-3., 3., self.n))
        x = np.arange(.01, .99, .005)
        P.figure(figsize=(9,6))
        for i in range(self.n):
            phi_i = np.exp(np.random.normal(np.log(self.Tau), self.epsil))

            (a,b) = ((self.mu[i] * phi_i), ((1 - self.mu[i]) * phi_i))
            theta = np.random.beta(self.mu[i] * phi_i, (1 - self.mu[i]) * phi_i, 10000)
            q = mquantiles(theta, prob=[0.05, .5, .95])
            print('mu', self.mu[i], 'phi_i', phi_i, 'a', np.round(a, 2), 
                'b', np.round(b, 2), 
                np.round(q, 3))
            bpdf = stats.beta.pdf(x, a, b)
#            P.hist(theta, bins= 50)
            P.plot(x, bpdf, label='Plot ' + str(i))
        P.xlabel(r'Probability of removal ($\theta$)')
        P.ylabel('Probability density')
        P.legend(loc='upper right')
        P.savefig('nestedBinom.png', format='png', dpi=120) 
        P.show()
        x = 22
            
    

def penguinSurv():
    nMoltFix = [.3, .5, .7, .9]
    nMoltRange = np.arange(.1, .98, .01)
    sic = [.3, .5, .7, .9]
    sicRange = np.arange(.1, .95, .01)
    omeg = 200
    S0 = .95
    P.figure(figsize=(12,6))
    P.subplot(1,2,1)
    for i in range(4):
        s = S0 * np.exp(-nMoltRange / omeg**sic[i])
        P.plot(nMoltRange, s, label = "SIC = " + str(sic[i]))
    P.ylabel('Probability of Survival', fontsize = 14)
    P.xlabel('Prop. of max population', fontsize = 14)
    P.legend(loc='lower left')
    P.subplot(1,2,2)
    for i in range(4):
        s = S0 * np.exp(-nMoltFix[i] / omeg**sicRange)
        P.plot(sicRange, s, label = "Prop max Pop. = " + str(nMoltFix[i]))
    P.xlabel('Sea ice concentration', fontsize = 14)
    P.legend(loc='lower right')
    P.savefig('penguinSurv.eps', format='eps', dpi=300)
    P.show()



def movementDensity():
    mu = 4.0
    sig = 1.0
    a = .5 * np.pi
    b = .2
    dist = np.arange(120.0, 0.0, -0.01)
    nHalf = len(dist)
    print('nHalf', nHalf)
    dist = np.append(dist, np.arange(0.01, 120.01, 0.01))
    dir = np.append(np.repeat(3/2*np.pi, nHalf), np.repeat(a, nHalf))
    distpdf = stats.lognorm.pdf(dist, s = sig, scale = np.exp(mu))
    dirpdf = dwrpcauchy(dir, a, b)

    x = np.append(np.arange(-120.0, 0.0, 0.01), np.arange(0.01, 120.01, 0.01))

    fullpdf = np.log(distpdf) + np.log(dirpdf)

    P.figure()
    P.plot(x, fullpdf)
    P.xlabel("dist")
    P.ylabel('Log density')
    P.savefig('haveToMove.png')
    P.show()  
"""
lnpdf = stats.lognorm.pdf(dist, s = 1, scale=np.exp(0))
>>> P.figure()
<Figure size 640x480 with 0 Axes>
>>> P.plot(dist, lnpdf)
[<matplotlib.lines.Line2D object at 0x7ff4a597d640>]
>>> P.show()
>>> stats.lognorm.pdf(0, s = 1, scale=np.exp(0))
np.float64(0.0)
"""





########            Main function
#######
def main():

#    eradTest()
#    G0Test()
#     gammaTest()
#    wrpCTest()
#    normalBeta()
#    Poisson()
#    SurvivorPrp()

#    outList = allocateSamples(np.random.uniform(1,50, size=5), 200)
#    Emigration()    
#    SpatialRemoval()

#    CorreRandVar()

#    corrVar()
#    gen_CorrRandom()
#    genPositiveRandNormal()

#    simtrap = SimTrap()
#    numbaTest()

#    distTrial()


#    residAlternate()
#    residualpop = estResidualPop()


#    binomFX()

#    BetaBinomial()

#    dispersalSteps()

#    simratdyn = SimRatDyn()

#    SimDensityDependence()

#    testZIP = TestZip()

#    NestedBinomial()

#    penguinSurv()

#    movementDensity()

if __name__ == '__main__':
    main()

