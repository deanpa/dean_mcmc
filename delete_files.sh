#!/bin/bash

# Navigate to the parent directory containing the iter_* subdirectories
cd ScenRakiura9k_Mod1_zip

# Find and delete all directories that match the pattern 'iter_???'
echo "Deleting directories..."
find . -name 'iter_???' -type d -print | sort | while read dir; do
    echo "Deleting directory: $dir"
    rm -rf "$dir"
done

echo "All specified directories have been deleted."
