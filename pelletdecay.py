#!/usr/bin/env python

import numpy as np
from scipy import stats
import pylab as P



class Decay_Test(object):
    def __init__(self):

        self.startPellPop = 188.    #50.0
        self.endPellPop = 197.  #56.0
        self.meanPelletRate = 25.0
        self.minDeerDensity_km = 0.9    # .10
        self.maxDeerDensity_km = 4.0    #4.0
        self.dayDecayRate = 0.00521
        self.daySurvRate =  1.0 - self.dayDecayRate
        self.maxDays = 364.0

        #### run functions  ############################
        self.calcPelletDensity()
#        self.estPelletDensity()
        ################################################

    def calcPelletDensity(self):
        self.deerDensity_km = np.arange(self.minDeerDensity_km, 
            (self.maxDeerDensity_km + 1.0), step = 0.01)
        self.deerDensity = self.deerDensity_km / 100.
        # days in year: add one element for existing pellets - 365 days old
        self.daysInYear = np.arange(1, (self.maxDays + 2.0))
        # pell density for each deer density
        self.pell_day = self.deerDensity * self.meanPelletRate 
        # 2-d array days by expected pell density for each pop density
        self.pellDay2D = np.outer((np.ones(self.maxDays + 1.0)), self.pell_day)
        # make last row the starting pellet density - 365 days old
        self.pellDay2D[-1] = self.startPellPop
#        print('self.pellDay2D', self.pellDay2D[:15, :6])
        self.survRate1D = self.daySurvRate**self.daysInYear
        self.survRate1D = np.expand_dims(self.survRate1D, 1)
#        print('survRate1D', self.survRate1D)
        self.pellDaysDeer2D = self.survRate1D * self.pellDay2D
        print('self.pellDaysDeer2D', self.pellDaysDeer2D[-3:,:4])
        self.totPellPop = np.sum(self.pellDaysDeer2D, axis = 0)
#        print('totPellPop', self.totPellPop)

        tmpArr = np.empty((len(self.totPellPop), 2))
        tmpArr[:,0] = self.deerDensity_km
        tmpArr[:,1] = self.totPellPop
#        print(tmpArr[:30])

        absPellDiff = np.abs(self.endPellPop - self.totPellPop)
        maskID = absPellDiff == np.min(absPellDiff)
        self.endDeerDen = self.deerDensity_km[maskID]
        print('Deer Density End', self.endDeerDen)
        # surviving pell from prev year
        self.survivePellPrevYear = self.pellDaysDeer2D[-1,0]
        # recruit rate across densities
        self.annRecruitRate = (self.endPellPop - self.survivePellPrevYear)
         
        print('Recruit Rate', self.annRecruitRate)

        P.figure(figsize = (11,9))
        ax1 = P.gca()
        lns1 = (ax1.plot(self.deerDensity_km,self.totPellPop, linewidth = 3))
        ax1.set_xlabel('deer density ($\mathregular{km^-2}$)', fontsize = 17)
        ax1.set_ylabel('Expected density of new pellets ($\mathregular{ha^-1}$)', fontsize = 17)
#        ax1.set_ylim([0.0, 26.0])
#        ax1.set_xlim([0.0, np.max(self.popDen + 1.)])
#        ax1.legend(lns, labs, loc = 'upper left')
#        P.xticks(mon, monNames)
        figFname = 'pelletDensity.png'
        P.savefig(figFname, format='png')
 
        P.show()






########            Main function
#######
def main():

    decaytest = Decay_Test()

if __name__ == '__main__':
    main()

