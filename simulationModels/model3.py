#!/usr/bin/env python

#import os
import numpy as np
import killerMales

class Params(object):
    """
    set model parameters
    """
    def __init__(self):
        self.iter = 500
        self.nYears = 12
        ## grid structure
        #####################
        self.modelNumber = 3
        #####################
        self.fineResol = 300
        self.coarseResol = 1500
        self.extentSide = 4500
        self.fineNRows = np.int(self.extentSide / self.fineResol)
        self.fineNCols = self.fineNRows
        self.totFinePix = np.int(self.fineNRows**2)
        self.coarseNRows = np.int(self.extentSide / self.coarseResol)
        self.coarseNCols = self.coarseNRows
        self.totCoarsePix = np.int(self.coarseNRows**2)
        self.nFineRowsPerCoarse = np.int(self.coarseResol / self.fineResol)
        self.nFineColsPerCoarse = np.int(self.coarseResol / self.fineResol)
        self.nFinePixPerCoarse = self.nFineRowsPerCoarse * self.nFineColsPerCoarse
        ## biological parameters
        self.initN = 16
        self.status = 0.80
        self.meanRecruits = 8.0
        self.survival = .80     # np.exp(-age*survival)
#        self.emigrationWindowSize = (self.fineResol * 11)  # less than fineNRows
        self.delta = 4000.       # dispersal decay
        self.meanAge = .45      # mean age of initial pop
        self.toxicAge = 1.0
        self.toxicSurvival = 0.80   #.85
        self.tau_MateDist = 3000
        ## application schedule
        self.pixPerYear = self.coarseNRows**2         # max = coarseNRows**2
        self.nIndividPerPixel = 5   # max = coarseResol / fineResol
        self.maxYearToxic = 2   
        print('fr', self.fineNRows, 'fc', self.fineNCols, 'cr', self.coarseNRows,
            'tot fine cells', self.fineNRows**2, 
            'fine cell per coarse', (self.coarseResol / self.fineResol)**2,
            'number of coarse cells', self.totCoarsePix,
            'ToxicMales per year', self.pixPerYear*self.nIndividPerPixel )






def main():

    params = Params()
    runmodeldata = killerMales.RunModel(params)


if __name__ == '__main__':
    main()