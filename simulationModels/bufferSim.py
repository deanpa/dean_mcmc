#!/usr/bin/env python

#import os
import numpy as np
#from numba import jit
#from scipy.stats.mstats import mquantiles
import pylab as P
import pandas as pd
import statsmodels.api as sm


def standardFX(x):
    stx = (x - np.mean(x)) / np.std(x)
    return(stx)

class Params(object):
    """
    set model parameters
    """
    def __init__(self):
        self.iter = 10000
        self.nYears = 10
        ## area details
        self.protectArea = 200      # km sq
        self.sourceArea = 200       # km sq
        self.sourceDensityRange = [0.05, 2.0]    # stoats per km
        self.protectDensityRange = [0.01, 0.5]  # density per km sq
        self.pDisperseRange = [0.01, .60]       # pdisp distance and direction
        self.pEncounterRange = [0.05, .99]
        self.pInter_EncRange = [0.01, 0.90]
        self.rRange =[0.8, 1.8]
        self.KRange = [1.0, 2.5]        # carrying capacity ind/km-sq
        self.pControlProtectRange = [.05, .95]
         


class SimData(object):
    """
    run model
    """
    def __init__(self, params):
        self.params = params
        ## Run functions #######################################
        self.getParams()
        self.runSim()
        self.doRegression()
        # End Functions  #######################################

                
    def getParams(self):
        self.sourceDensity = np.random.uniform(self.params.sourceDensityRange[0],
                        self.params.sourceDensityRange[1], self.params.iter)
        self.protectDensity = np.random.uniform(self.params.protectDensityRange[0],
                        self.params.protectDensityRange[1], self.params.iter)
        self.pDisperse = np.random.uniform(self.params.pDisperseRange[0],
                        self.params.pDisperseRange[1], self.params.iter)
        self.pEncounter = np.random.uniform(self.params.pEncounterRange[0],
                        self.params.pEncounterRange[1], self.params.iter)
        self.pInter_Enc = np.random.uniform(self.params.pInter_EncRange[0],
                        self.params.pInter_EncRange[1], self.params.iter)
        self.r = np.random.uniform(self.params.rRange[0],
                        self.params.rRange[1], self.params.iter)
        self.K = np.random.uniform(self.params.KRange[0],
                        self.params.KRange[1], self.params.iter) * self.params.protectArea
        self.pControlProtect = np.random.uniform(self.params.pControlProtectRange[0],
                        self.params.pControlProtectRange[1], self.params.iter)
        self.intercept = np.ones(self.params.iter)

        self.covariateArr = pd.DataFrame({'Intercept' : self.intercept, 
            'srcDensity' : self.sourceDensity, 
            'pDisperse' : self.pDisperse, 'pEncounter' : self.pEncounter, 
            'pInter_Enc' : self.pInter_Enc, 'r': self.r, 'K' : self.K, 
            'pControlProtect' : self.pControlProtect})


#        self.covariateArr = pd.DataFrame({'Intercept' : self.intercept, 
#            'srcDensity' : self.sourceDensity, 'ProtectDensity' : self.protectDensity,
#            'pDisperse' : self.pDisperse, 'pEncounter' : self.pEncounter,
#            'pInter_Enc' : self.pInter_Enc, 'r': self.r, 'K' : self.K, 
#            'pControlProtect' : self.pControlProtect})

        self.covNoInter = self.covariateArr.iloc[:, 1:]
        self.covNoInter = self.covNoInter.apply(standardFX, axis = 0)

        self.covStandard = self.covariateArr.copy()
        self.covStandard.iloc[:, 1:] = self.covariateArr.iloc[:,1:].apply(standardFX, axis = 0)

        corCovArr = self.covariateArr.corr(method='pearson')
        corCovStand = self.covStandard.corr(method='pearson')
#        print('corCovArr', np.round(corCovArr.iloc[:,5:], 2))
#        print('corCovStand', np.round(corCovStand.iloc[:,5:], 2))



        self.growthRate = np.zeros(self.params.iter)
#        print('cov', self.covStandard.iloc[:, :7], 'means', self.covStandard.apply(np.mean, axis = 0))
        self.endDensity = np.zeros(self.params.iter)

    def runSim(self):
        # loop iter
        for i in range(self.params.iter):
            N0 = self.protectDensity[i] * self.params.protectArea
            Nt_1 = N0
            for j in range(self.params.nYears):
                potentialImm = self.sourceDensity[i] * self.params.sourceArea * self.pDisperse[i]
                pNoCaptImm = 1.- (self.pEncounter[i] * self.pInter_Enc[i])
                nImm = np.random.binomial(potentialImm, pNoCaptImm)
                Nt_1 = Nt_1 + nImm
                # update Nt
                mu_j  = ((Nt_1 * np.exp(self.r[i] * (1.0 - (Nt_1 / self.K[i])))) * 
                    (1.0 - self.pControlProtect[i]))
                Nt = np.random.poisson(mu_j)
#                if i < 5:
#                    print('i', i, 'j', j, 'Nt_1', Nt_1, 'nImm', nImm, 'r', np.round(self.r[i],2), 
#                        'K', np.round(self.K[i], 1), 
#                        'mu_j', np.round(mu_j, 0), 'Nt', Nt, 
#                        'potImm', potentialImm, 'pNoCaptImm', pNoCaptImm,
#                        'pControlSource', self.pControlProtect[i])


                Nt_1 = Nt
            self.growthRate[i] = (Nt - N0) / N0
            self.endDensity[i] = np.log((Nt / self.params.protectArea) + 1.0)
    
    def doRegression(self):
        """
        Do regression of rate against standardised parameters
        """
        regr = sm.OLS(self.endDensity, self.covStandard).fit()
        print('regr mod results', regr.summary())

        ## plot residuals
        regr_fitVal = regr.fittedvalues
        regr_resids = regr.resid
        P.figure()
        P.subplot(1,2,1)
        P.plot(self.endDensity, regr_fitVal, 'ok')
        P.subplot(1,2,2)
        P.plot(regr_fitVal, regr_resids, 'ob')
        
        P.show()


        beginningtex = """\\documentclass{report}
        \\usepackage{booktabs}
        \\begin{document}"""
        endtex = "\end{document}"

        f = open('bufferTable.tex', 'w')
        f.write(beginningtex)
        f.write(regr.summary().as_latex())
        f.write(endtex)
        f.close()





def main():

    params = Params()
    simdata = SimData(params)


if __name__ == '__main__':
    main()