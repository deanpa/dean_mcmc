import numpy as np
from numba import jit
from scipy.stats.mstats import mquantiles
import pylab as P


def RunModel(params):

    ### Call Functions
#    print('print mn age', np.mean(ageRaster[classRaster>0]), 'class mean',
#        np.mean(classRaster[classRaster>0]), ageRaster, classRaster)
    NPopArray = np.zeros((params.iter, params.nYears))

    ## loop iterations
    for i in range(params.iter):
        ## make empty rasters to populate
        (ageRaster, classRaster) = makeRasters(params)
        ## Distribute the initial stoat population in fine resol cells
        (ageRaster, classRaster) = distribIndivid(params.totFinePix, params.initN, 
            params.fineNCols, params.fineNRows, params.meanAge, ageRaster, classRaster)
        ## loop years
        for j in range(params.nYears):
            ## Distribute maleStar
            if j < params.maxYearToxic:
                (ageRaster, classRaster) = distribMaleStar(params.pixPerYear, 
                    params.nIndividPerPixel, params.nFineRowsPerCoarse, 
                    params.nFineColsPerCoarse, ageRaster, classRaster, params.coarseNCols, 
                    params.coarseNRows, params.nFinePixPerCoarse, params.toxicAge,
                    params.totCoarsePix, params.toxicSurvival)

#            print('i j', i, j, 'n', np.sum(classRaster>0))

            (offSpringRaster, classRaster, ageRaster) = doMating(params.fineNCols, 
                params.fineNRows, classRaster, ageRaster, params.tau_MateDist, 
                params.meanRecruits, params.status, params.fineResol)

            nPixelsEmpty = np.sum(classRaster == 0)
            totalOffSpring = np.sum(offSpringRaster)

#            print('aftermating', 'class', np.sum(classRaster >0), 'n offS', np.sum(offSpringRaster))            

            (ageRaster, classRaster) = doDispersal(offSpringRaster, classRaster, 
                params.delta, ageRaster, params.fineResol, nPixelsEmpty, 
                totalOffSpring, params.fineNRows, params.fineNCols)

            (ageRaster, classRaster) = doSurvivorship(ageRaster, classRaster, params.survival,
                params.fineNRows, params.fineNCols)


            NPopArray = populateNArray(classRaster, NPopArray, i, j)

    summaryTable = processResults(NPopArray, params.iter, params.nYears, params.modelNumber)



def makeRasters(params):
    """
    make raster to populate
    """
    ageRaster = np.zeros((params.fineNRows, params.fineNCols))
    ## 0=absent; 1=female; 2=male; 3=maleStar
    classRaster = np.zeros((params.fineNRows, params.fineNCols), dtype = np.uint16)
    return(ageRaster, classRaster)

@jit
def distribIndivid(totFinePix, initN, fineNCols, fineNRows, meanAge, ageRaster, classRaster):
    """
    ## distribute initial males and females
    """
    probSettle = 1.0/ totFinePix
    sumProbsUsed = 0.0
    nRemain = initN
    cellsRemain = totFinePix
    for cx in range(fineNCols):
        for cy in range(fineNRows):
            if nRemain <= 0:
                break
            # change probEmigrate to be the probability (sum to 1)
#            absprob = probEmigrate[suby, subx] / sumRelProbEm
            prob_fg = probSettle / (1.0 - sumProbsUsed)
            prob_fg = 1.0 - (1.0 - prob_fg)**nRemain
            if prob_fg > 1.0:
#                        print('prob fg > 1 = ', prob_fg, (1.0 - sumProbsUsed), absprob)
                prob_fg = 1.0
            elif prob_fg < 0.0:
                print('prob_fg < 0 =', prob_fg)
                prob_fg = 0.0
            # Eqn 14    Multinomial draw
            if cellsRemain == nRemain:
                Settle = 1
            else:   
                Settle = np.random.binomial(1, prob_fg)
            if Settle == 1:
                classRaster[cy, cx] = np.random.binomial(1, 0.5) + 1
                ageRaster[cy, cx] = np.random.poisson(meanAge) + 1
            sumProbsUsed += probSettle
            nRemain -= Settle
            cellsRemain -= 1
        if nRemain <= 0:
            break
    return(ageRaster, classRaster)

#@jit
def distribMaleStar(pixPerYear, nIndividPerPixel, nFineRowsPerCoarse, nFineColsPerCoarse,
        ageRaster, classRaster, coarseNCols, coarseNRows, nFinePixPerCoarse, toxicAge,
        totCoarsePix, toxicSurvival):
    """
    ## distribute toxic males
    """
    probPixSelect = 1.0 / pixPerYear
    sumPixProbsUsed = 0.0
#    nRemain = initN
    pixToSettle = pixPerYear
    nCandidatePix = totCoarsePix
    for cx in range(coarseNCols):
        for cy in range(coarseNRows):
            if pixToSettle <= 0:
                break
            
            # change probEmigrate to be the probability (sum to 1)
            if sumPixProbsUsed >= 1.0:
                prob_pix = 1.0
            else:
                prob_pix = probPixSelect / (1.0 - sumPixProbsUsed)
            prob_pix = 1.0 - (1.0 - prob_pix)**pixToSettle
            if prob_pix > 1.0:
#                        print('prob fg > 1 = ', prob_fg, (1.0 - sumProbsUsed), absprob)
                prob_pix = 1.0
            elif prob_pix < 0.0:
                print('prob_pix < 0 =', prob_pix)
                prob_pix = 0.0
            # Eqn 14    Multinomial draw
            if nCandidatePix == pixToSettle:
                Settle = 1
            else:   
                Settle = np.random.binomial(1, prob_pix)
            ## update counts and probabilities
            nCandidatePix -= 1
            sumPixProbsUsed += probPixSelect
            pixToSettle -= Settle

            if Settle == 1:
                ## loop thru fine pixels distribute maleStars
                TLX = cx * nFineColsPerCoarse
                BRX = (cx * nFineColsPerCoarse) + nFineColsPerCoarse
                TLY = cy * nFineRowsPerCoarse
                BRY = (cy * nFineRowsPerCoarse) + nFineRowsPerCoarse
                ## loop first to count cells occupied
                pixOccupied = 0
                for fx in range(TLX, BRX):
                    for fy in range(TLY, BRY):
                        if classRaster[fy, fx] > 0:
                            pixOccupied += 1
                finePixToFill = nFinePixPerCoarse - pixOccupied
                
                ## if all pixels are occupied, then toxic male dies
                if finePixToFill == 0:
                    break

#                print('cx', cx, 'cy', cy, 'pixOcc', pixOccupied, 'finePixToFill', finePixToFill)

                probToSettle = 1.0 / finePixToFill
                nToxicRemain = nIndividPerPixel
                sumFinePixProbUsed = 0.0
                ## loop second time to fill toxic males in un-occupied cells
                for fx in range(TLX, BRX):
                    for fy in range(TLY, BRY):
                        if nToxicRemain <= 0:
                            break
                        if classRaster[fy, fx] > 0:
                            continue
                        prob_xy = probToSettle / (1.0 - sumFinePixProbUsed)
                        prob_xy = 1.0 - (1.0 - prob_xy)**nToxicRemain
                        if prob_xy > 1.0:
                            prob_xy = 1.0
                        if nToxicRemain == finePixToFill:
                            toxicSettle = 1
                        else:
                            toxicSettle = np.random.binomial(1, prob_xy)
                        ## update counts and probabilities
                        sumFinePixProbUsed += probToSettle
                        nToxicRemain -= toxicSettle
                        finePixToFill -= 1
                        if toxicSettle == 1:
                            toxicLive = np.random.binomial(1, toxicSurvival)
                            classRaster[fy, fx] = 3 * toxicLive
                            ageRaster[fy, fx] = toxicAge * toxicLive
                    if nToxicRemain <= 0:
                        break
        if pixToSettle <= 0:
            break
    return(ageRaster, classRaster)


@jit
def doMating(fineNCols, fineNRows, classRaster, ageRaster, tau_MateDist, 
            meanRecruits, status, fineResol):
    """
    loop thru cells and do mating/toxin/reproduction, then dispersal of offspring
    """
    countMates =0
    offSpringRaster = np.zeros((fineNRows, fineNCols), dtype = np.uint16 )
    ## loop thru and find females
    for fx in range(fineNCols):
        for fy in range(fineNRows):
            if classRaster[fy, fx] != 1:
                continue
###            print('fx fy', fx, fy, classRaster[fy, fx])
            ## get prob of being mated
            totProbMate = 0.0
            relProbMateRaster = np.zeros((fineNRows, fineNCols))
            sumRelProbMate = 0.0
            ## loop to get rel probs of individual males, tot prob and sum of rel probs
            for FX in range(fineNCols):
                for FY in range(fineNRows):
                    if classRaster[FY, FX] < 2:
                        continue
                    distx = (fx - FX) * fineResol
                    disty = (fy - FY) * fineResol
                    dist = np.sqrt(distx**2 + disty**2)

                    if classRaster[FY, FX] == 3:
                        Prob_XY = np.exp(-dist / tau_MateDist) * status
                    else:
                        Prob_XY = np.exp(-dist / tau_MateDist)
                    totProbMate = 1.0 - ((1.0 - totProbMate) * (1.0 - Prob_XY)) 
                    relProbMateRaster[FY, FX] = Prob_XY
                    sumRelProbMate += Prob_XY
            femaleMates = np.random.binomial(1, totProbMate)
            countMates += femaleMates
###            print('fx fy', fx, fy, 'totprobmate', totProbMate, 'femMate', femaleMates, 'count', countMates,
###                'class', classRaster[fy, fx])  
            if femaleMates == 0:
                continue
            ## loop again to ID males for the female in [fy,fx]
            ## if female mates, ID the male: loop thru cells for multinomial draw of 1
            sumProbsUsed = 0.0
            for FX in range(fineNCols):
                for FY in range(fineNRows):
                    ## if no males in pixel
                    if classRaster[FY, FX] < 2:
                        continue
                    absProb = relProbMateRaster[FY, FX] / sumRelProbMate
                    xy_prob = absProb / (1.0 - sumProbsUsed)
                    mate_xy = np.random.binomial(1, xy_prob)
                    # mate with toxic male - Adult female and offspring die
                    if (mate_xy == 1) & (classRaster[FY, FX] == 3):
                        classRaster[fy, fx] = 0
                        ageRaster[fy, fx] = 0
                    # mate with non-toxic male - get offspring
                    elif (mate_xy == 1) & (classRaster[FY, FX] == 2):
                        offSpringRaster[fy, fx] = np.random.poisson(meanRecruits)
    return(offSpringRaster, classRaster, ageRaster)
                    
@jit
def doDispersal(offSpringRaster, classRaster, delta, ageRaster, fineResol,
        nPixelsEmpty, totalOffSpring, fineNRows, fineNCols):
    """
    ## offspring disperse - presently assume can disperse anywhere... change later
    """
    destPixRemain = nPixelsEmpty
    totOffSpringRemain = totalOffSpring 
    for fx in range(fineNCols):
        for fy in range(fineNRows):
            if offSpringRaster[fy, fx] == 0 :
                continue
            ## with offspring present, loop thru cells to get rel pro and sumrelProb
            ## then loop thru to do multinomial draws
            sumRelProbDisp = 0.0
            relProbDisperseRaster = np.zeros((fineNRows, fineNCols))
            ## loop thru potential destination cells.
            for FX in range(fineNCols):
                for FY in range(fineNRows):
                    if classRaster[FY, FX] > 0:
                        continue
                    ## if pixel is available, carry on...
                    distx = (fx - FX) * fineResol
                    disty = (fy - FY) * fineResol
                    dist = np.sqrt(distx**2 + disty**2)
                    relProb_xy = np.exp(-dist / delta)
                    relProbDisperseRaster[FY, FX] = relProb_xy
                    sumRelProbDisp += relProb_xy
            ## loop thru potential destination for multinomial draw
            sumProbsUsed = 0.0
            individRemain = offSpringRaster[fy, fx]

#            print('fx fy', fx, fy, 'indRemain', individRemain, 'dest rem', destPixRemain)
            sumDisperse = 0
            for FX in range(fineNCols):
                for FY in range(fineNRows):
                    if classRaster[FY, FX] > 0:
                        continue
                    if individRemain == 0:
                        break
                    
                    ## if pixel is available, carry on..
                    if destPixRemain == totOffSpringRemain:
                        disperse_XY = 1
                    else:
                        absProb = relProbDisperseRaster[FY, FX] / sumRelProbDisp
                        prob_XY = absProb / (1.0 - sumProbsUsed)
                        prob_XY = 1.0 - (1.0 - prob_XY)**individRemain
                        if prob_XY > 1.0:
#                            print('prob fg > 1 = ', prob_XY, (1.0 - sumProbsUsed), absprob)
                            prob_XY = 1.0
                        disperse_XY = np.random.binomial(1, prob_XY)
                    if disperse_XY == 1:
                        classRaster[FY, FX] = np.random.binomial(1, 0.5) + 1
                        ageRaster[FY, FX] = 1
                        individRemain -= 1
                        totOffSpringRemain -= 1 
                    sumProbsUsed += absProb
                    destPixRemain -= 1
                if individRemain == 0:
                    break
    return(ageRaster, classRaster)

@jit
def doSurvivorship(ageRaster, classRaster, survival, fineNRows, fineNCols):
    """
    ## process survivorship / mortality
    """
    for fx in range(fineNCols):
        for fy in range(fineNRows):
            if classRaster[fy, fx] == 0:
                continue
            ## process survivorship
            probSurv_xy = np.exp(-survival * ageRaster[fy, fx])
            surv_xy = np.random.binomial(1, probSurv_xy)
            if surv_xy == 1:
                ageRaster[fy, fx] += 1
            else:
                ageRaster[fy, fx] = 0
                classRaster[fy, fx] = 0
    return(ageRaster, classRaster)


def populateNArray(classRaster, NPopArray, i, j):
    """
    ## populate the results array.
    """
    N_ij = np.sum(classRaster > 0)      # len(classRaster[classRaster > 0])
    NPopArray[i, j] = N_ij
    return(NPopArray)    


def processResults(NPopArray, iter, nYears, modelNumber):
    """
    ## summarise results and plot population
    """
    endPop = NPopArray[:, -1]
    nErad = len(endPop[endPop == 0])
    probErad = nErad / iter
    print('Probability of Eradication =', probErad)
    summaryTable = np.zeros((3, nYears))
    summaryTable[0] = np.mean(NPopArray, axis = 0)
    summaryTable[1:] = mquantiles(NPopArray, axis = 0, prob=[0.025, 0.975])
    print('summaryTable', summaryTable)
#    print('npoparr', NPopArray)
    plotPopulation(summaryTable, nYears, modelNumber)
    return(summaryTable)    

def plotPopulation(summaryTable, nYears, modelNumber):
    """
    plot results over time
    """
    P.figure()
    yr = np.arange(1, nYears +1, dtype = int)
    P.plot(yr, summaryTable[0], color = 'k', linewidth = 4.0)
    P.plot(yr , summaryTable[1], 'k--', linewidth = 2.0)
    P.plot(yr, summaryTable[2], 'k--', linewidth = 2.0)
    P.ylim((np.min(summaryTable[1]) - 4), (np.max(summaryTable[2]) +4))
    P.xlabel('Years')
    P.ylabel('Stoat population size')
    modelFName = 'plotResults_Model' + str(modelNumber) + '.png'
    P.savefig(modelFName, format='png')
    P.show()







  

