#!/usr/bin/env python

import numpy as np
from scipy import stats
import pylab as P
from scipy.stats.mstats import mquantiles
from shapely.geometry import Point
from shapely.ops import unary_union


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D



class simTT(object):
    def __init__(self):
        self.params()
        self.simDetection()
        self.printResults()

    def params(self):
        self.maxDen = {'Rats': .5, 'Possums': .4, 'Stoats': 0.02}
        self.g0 = {'Rats' : .1, 'Possums' : 0.05, 'Stoats' : 0.06}
        self.sigma = {'Rats' : 40, 'Possums' : 60, 'Stoats' : 300}



#        self.maxDen = {'Rats': .5, 'Possums': .1, 'Stoats': 0.02}
#        self.g0 = {'Rats' : .05, 'Possums' : 0.1, 'Stoats' : 0.02}
#        self.sigma = {'Rats' : 40, 'Possums' : 80, 'Stoats' : 300}



#        self.nTunnels = 120
        self.trapLayout = {'Rats' : {'transectDist' : 200, 
                                    'trapDist' : 50, 
					                'devicePerLine' : 50}, 
                            'Possums' : {'transectDist' : 200, 
                                    'trapDist' : 50, 
					                'devicePerLine' : 50},
                            'Stoats' : {'transectDist' : 1000, 
                                    'trapDist' : 100, 
					                'devicePerLine' : 50}}
        self.nights_TT = {'Rats': 3, 'Possums': 3, 'Stoats': 3}
        self.iter = 500
        self.nDen = 15
        self.storeDetect = {'Rats': np.zeros((self.iter, self.nDen)), 
            'Possums': np.zeros((self.iter, self.nDen)), 
            'Stoats': np.zeros((self.iter, self.nDen))}

    def simDetection(self):
        self.density = {}
        for spp in self.maxDen.keys():
            self.getLayout(spp)
            self.density[spp] = np.linspace((self.maxDen[spp]/self.nDen), self.maxDen[spp], self.nDen)
#            print('dens', self.density[spp])
            for dd in np.arange(self.nDen):
                self.nPreds = np.int16(np.round(self.total_area * self.density[spp][dd], 0))
                print(spp, 'n', self.nPreds, 'den', self.nPreds/self.total_area)
                for i in np.arange(self.iter):
                    self.trackingTunnelRate(spp, dd, i)
                    

    def getLayout(self, spp):
        yCoord = np.array([0, self.trapLayout[spp]['transectDist']])
        self.yBase = np.repeat(yCoord, self.trapLayout[spp]['devicePerLine'])
        self.xBase = np.tile(np.arange(0, self.trapLayout[spp]['trapDist'] * 
            self.trapLayout[spp]['devicePerLine'], 
            self.trapLayout[spp]['trapDist']), 2)
        self.nTunnels = len(self.xBase)

#        if spp == 'Stoats':
#            print('xy', self.xBase, self.yBase)
 
        self.detectDist = 3.0 * self.sigma[spp]
        print(spp, 'dist det', self.detectDist)

        buffers = [Point(x, y).buffer(self.detectDist) for x, y in zip(self.xBase, self.yBase)]

#        buffers = [Point(x, self.yBase).buffer(self.detectDist) for x in self.xBase]
        union_buffer = unary_union(buffers)
        self.total_area = union_buffer.area / 10000     # convert to ha
        print(spp, 'total area in ha', self.total_area)
 

    def trackingTunnelRate(self, spp, dd, i):
        """
        # Estimate the expected tracking tunnel rate in a management zone
        # Use rodent density at the rodent resolution, ie 100 m (1 ha)
        """
        xpreds = np.zeros(self.nPreds)
        ypreds = np.zeros(self.nPreds)
        # generate some random distances
        for ind in range(self.nPreds):
            tunnel = np.random.randint(0, self.nTunnels, 1)

#            print(spp, 'tunnel', tunnel, 'detect dist', self.detectDist, 'x', self.xBase[tunnel][0])

            xpreds[ind] = np.random.uniform(self.xBase[tunnel][0] - self.detectDist, 
                self.xBase[tunnel][0] + self.detectDist)
            ypreds[ind] = np.random.uniform(self.yBase[tunnel][0] - self.detectDist, 
                self.yBase[tunnel][0] + self.detectDist)
        ## 2D DISTANCE ARRAY
        dist = distFX(xpreds, ypreds, self.xBase, self.yBase)
        # probability of detection of all tracking tunnels over 1 night
        pdect = self.g0[spp] * np.exp(-(dist**2) / 2.0 / (self.sigma[spp]**2))
        # probability of detection over multiple nights
        PD = 1.0 - (1.0 - pdect)**self.nights_TT[spp]
        tracking = np.random.binomial(1, PD)     # / self.nTunnels
        trackingRate = np.sum(np.any(tracking == 1, axis=0)) / self.nTunnels
        self.storeDetect[spp][i, dd] = trackingRate
    
#        print(spp, 'n', self.nPreds, 'd', np.round(dist[:4], 0), 'pdet', np.round(pdect[:4],4), 
#            'PD', np.round(PD, 4), 'tr', np.round(trackingRate,4)) 

    def printResults(self):
        for spp in self.maxDen.keys():
            self.meanRate = np.mean(self.storeDetect[spp], axis = 0)
            self.quants = mquantiles(self.storeDetect[spp], prob=[.05, 0.5, 0.90],  axis = 0)
            for dd in np.arange(self.nDen):
                print(spp, 'density', np.round(self.density[spp][dd], 4),  
                    'mn TR = ', np.round(self.meanRate[dd],4), 
                    'quants TR', np.round(self.quants[:, dd],4))
#            print(spp, 'store Detect', self.storeDetect[spp])

########            Main function
#######
def main():

    tt = simTT()

if __name__ == '__main__':
    main()

