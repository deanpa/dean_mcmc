#!/usr/bin/env python
#

#importing required modules and python scipts
import os
#from amgcommon import basic #this imports basic.py with all our basic functions
import paramsBernReg #this imports params.py with priors & initial paramaters
import numpy as np
from scipy import stats
import pickle

##### global functions
##
def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))
##### end global functions


class RunPars(object):
    """
    Stores updated parameters needed to re-start an MCMC 
    using initial parameter estimates from params.py
    attributes:
    pars = initial parameter estimates from Params()
    """
    def __init__(self): 
        pars = paramsBernReg.Pars() #only brought as a local variable because
            #we don't want to pickle it but only use it to get self.b
        self.b = pars.b.copy()  #initial b estimate

#defining MCMC sampler
class MCMC(object):
    """
    Defines MCMC updating functions and arrays for storing predicted coefficients
    uses paramsBernReg.py and RunPars()
    attributes: 
    priors = using preset priors from Priors
    pars = initial parameter estimates  from Pars() 
    runpars =  values that are updated during an MCMC run and either come from RunPars() if initial run or from an updated pickle file
    mcmcpars = MCMC parameters from MCMCPars
    """
    def __init__(self, runpars, processeddata): 

        #### Data from genBernouill.py in processeddata object
        self.x = processeddata.x    # covariate data
        self.y = processeddata.y    # Bernouilli response variable
        # priors from paramsBernReg.py
        self.priors = paramsBernReg.Priors()
        # parameters to be updated
        self.runpars = runpars

        ### initial theta values
        self.ltheta = np.dot( self.x, self.runpars.b ) #initial log theta
        self.theta = inv_logit( self.ltheta ) #intital theta 
        # mcmc iteration parameters
        self.mcmcpars = paramsBernReg.MCMCPars()
        # storage array for parameter estimates
        self.bgibbs = np.empty((len(self.mcmcpars.keep), self.priors.npara))

    
    def bupdate(self):
        """
        update beta parameters one at a time
        """
        for i in range(self.priors.npara):
            bprop = self.runpars.b.copy()
            bprop[i] = np.random.normal(self.runpars.b[i], self.mcmcpars.betaJump) #proposed values
            ltheta_s = np.dot(self.x, bprop)
            theta_s = inv_logit(ltheta_s)
            #log probabilities for current (pnow) & proposed (pnew) values
            lbinom = stats.binom.logpmf(self.y, 1, self.theta)
            lnorm = stats.norm.logpdf(self.runpars.b[i] , self.priors.meanB, self.priors.varB)
            pnow = np.sum(lbinom) + lnorm  
            lbinom_s = stats.binom.logpmf(self.y, 1, theta_s)
            lnorm_s = stats.norm.logpdf(bprop[i] , self.priors.meanB, self.priors.varB)
            pnew = np.sum(lbinom_s) + lnorm_s
            #print(pnew, pnow)
            r = np.exp( pnew -  pnow ) #acceptance criterion
            #print(r)
            z = np.random.uniform( 1, 0, size = None)
            if z < r: #if accept with probability r update b & theta
                self.runpars.b[i] = bprop[i]
                self.ltheta = ltheta_s.copy()
                self.theta = theta_s.copy()
        

        #                        
    def gibbs(self):
        """
        run metropolis updater"
        """
        print('betas from simulation', -.75, 0.5, -1.0)
        g = 0
        for gg in range(self.mcmcpars.totalIterations):
            self.bupdate()
            #
            if gg in self.mcmcpars.keep:
                self.bgibbs[g] = self.runpars.b
                g += 1
#        print('bgibbs', self.bgibbs)            #


class ProcessedData(object):
    def __init__(self, berndata):
        """
        define pickle structure from genBernouilli.py
        """
        self.y = berndata.y     # bernouilli data
        self.x = berndata.x     # 2-d covariate matrix

class Results(object):
    def __init__( self, mcmcobj ):
        self.bgibbs = mcmcobj.bgibbs
        print('ran results')
            
########            Main function
#######
#def main( pklresults, pklrunpars ):
def main(pklresults=None, pklrunpars=None):
    """
    Run MCMC and pickle updated values from RunPars() and 
    results of MCMC() from Results()
    """

    bernpath = os.getenv('NESIPROJDIR', default = '.') #gets the env var value which is file path to pr
    
    ##to determine if initial values are to be used or updated values from pickle:      
    ##start##
    if pklrunpars is None:
        runpars = RunPars()
        print('updated run values not present so mcmc run started using initial values')
    else:
        # read in pickled updated parameters from previous run stored in RunPars()
        runParsFile = os.path.join(bernpath, pklrunpars)
        fileobj = open(runParsFile, 'rb') #rb because you open it for reading it
        runpars = pickle.load(fileobj)
        fileobj.close()
                                        
    ##to run MCMC and store results in a pickle file:
    ##start##

    BernDataFile = os.path.join(bernpath,'out_berndata.pkl')
    fileobj = open(BernDataFile, 'rb')
    processeddata = pickle.load(fileobj)
    fileobj.close()

    ### run mcmc
    mcmcobj = MCMC( runpars, processeddata ) #assigns an instance of MCMC class
    mcmcobj.gibbs() #runs the MCMC

    #stores predicted results
    results = Results( mcmcobj ) 
    
    # pickle runpars from present run to be used to initiate new runs
    outRunPars = os.path.join(bernpath,'out_runpars.pkl')
    fileobj = open(outRunPars, 'wb')
    pickle.dump(runpars, fileobj)
    fileobj.close()
    
    # pickle mcmc results for post processing in bernPostProcessing.py
    outGibbs = os.path.join(bernpath,'out_gibbs.pkl')
    fileobj = open(outGibbs, 'wb')
    pickle.dump(results, fileobj)
    fileobj.close()
    
if __name__ == '__main__':
        main()

 

 


       
