#!/bin/bash
#SBATCH -J BernReg_slurm
#SBATCH -A landcare00039
#SBATCH --mail-type=end
#SBATCH --mail-user=andersond@landcareresearch.co.nz
#SBATCH --time=19:00:00
#SBATCH --mem-per-cpu=3000
#SBATCH -C wm
#SBATCH -o job-%j.%N.out
#SBATCH -e job-%j.%N.err

#srun send.py 

srun sendBernReg.py --runparsfile=$CHINAPROJDIR/out_runpars.pkl --resultsfile=$RIVERPROJDIR/out_gibbs.pkl

