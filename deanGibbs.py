#!/usr/bin/env python

from scipy import stats
from scipy.special import gamma
import numpy as np
import pylab as P
import prettytable


class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """
        self.ndat = 10
 
        # betas used to simulate data
        self.b = np.array([-2., 2.5])

        # make x data
        self.x = np.empty((self.ndat,2))
        self.x[:,0] = 1.0
        xTmp = np.arange(self.ndat)
        xTmp = np.random.uniform(-10, 10, size= self.ndat) 
        self.x[:,1] = xTmp
        
        # variance used to simulate data
        self.sg = 15.0

        # simulate y data
        yTmp = np.dot(self.x, self.b)
        self.y = np.random.normal(yTmp, np.sqrt(self.sg), size=self.ndat)

        self.npara = len(self.b)

        # Priors
        self.diag = np.diagflat(np.tile(1000., self.npara))
        self.vinvert = np.linalg.inv(self.diag)
        self.s1 = .1
        self.s2 = .1
        self.ppart = np.dot(self.vinvert, np.zeros(self.npara))
        # Initial values
        self.b = np.array([-9.5, 9.5])
        self.sg = 4.0
        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 2000             # number of estimates to save for each parameter
        self.thinrate = 1           # thin rate
        self.burnin = 0             # burn in number of iterations
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)
        ###################################################
        ###################################################
        print('ngibbs', self.ngibbs)

class MCMC(object):
    def __init__(self, params):

        self.params = params

        self.ResultsGibbs = np.zeros([self.params.ngibbs, (self.params.npara +1)]) 


    def bupdate(self):
        """
        This is my function
        """
        xTranspose = np.transpose(self.params.x)
        xCrossProd = np.dot(xTranspose, self.params.x)
        sinv = 1.0/self.params.sg
        sx = np.multiply(xCrossProd, (1./self.params.sg))
        xyCrossProd = np.dot(xTranspose, self.params.y)
        sy = np.multiply(xyCrossProd, sinv)
        bigv = np.linalg.inv(sx + self.params.vinvert)
        smallv = sy + self.params.ppart
        meanVariate = np.dot(bigv, smallv)
        b = np.random.multivariate_normal(meanVariate, bigv)
        self.params.b =  np.transpose(b)
#        self.params.mu = np.dot(self.basicdata.xdat, self.params.b)

    def vupdate(self):
        pred = np.dot(self.params.x, self.params.b)
#        pred = pred.transpose()
        predDiff = self.params.y - pred
#        mufx = self.params.y - pred
#        muTranspose = mufx.transpose()
#        sx = np.dot(mufx, muTranspose)
        sx = np.sum(predDiff**2.0)
        u1 = self.params.s1 + np.multiply(.5, self.params.ndat)
        u2 = self.params.s2 + np.multiply(.5, sx)               # rate parameter    
        isg = np.random.gamma(u1, 1./u2, size = None)            # formulation using shape and scale
        self.params.sg = 1.0/isg

#


########            Main mcmc function
########
    def mcmcFX(self):
        """
        Run gibbs sampler
        """
        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):

            self.vupdate()

            self.bupdate()


            if g in self.params.keepseq:
                self.ResultsGibbs[cc,0:self.params.npara] = self.params.b
                self.ResultsGibbs[cc,-1] = self.params.sg

                cc = cc + 1

#        print('gibbs', self.ResultsGibbs[:300])


    ################
    ################ Functions to make table
    def quantileFX(self, a):
        """
        function to calculate quantiles
        """
        return stats.mstats.mquantiles(a, prob=[0.025, 0.5, 0.975])

    def makeTableFX(self):
        """
        Function to print table of results
        """
        resultTable = np.zeros(shape = (4, (self.params.npara+1)))

        resultTable[0:3, :] = np.apply_along_axis(self.quantileFX, 0, self.ResultsGibbs)

        resultTable[3, :] = np.apply_along_axis(np.mean, 0, self.ResultsGibbs)
        resultTable = np.round(resultTable.transpose(), 3)
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])

        names = ['Intercept', 'Beta1', 'Variance']
        for i in range(np.shape(resultTable)[0]):
            name = names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)


    def plotFX(self, params):
        """
        make diagnostic trace plots
        """
        meanBeta = np.apply_along_axis(np.mean, 0, self.ResultsGibbs)
        Ypredicted = np.dot(self.params.x, meanBeta[0:2])
        nplots = self.params.npara + 1  # + 1
        P.figure(0)
        for i in range(nplots):
            P.subplot(2, 2, i+1)
            P.plot(self.ResultsGibbs[:, i])
        P.subplot(2,2, 4)
        P.plot(self.ResultsGibbs[:,0], self.ResultsGibbs[:,1])
#        P.scatter(self.params.y, Ypredicted)
        P.show()





########            Main function
#######
def main():


    params = Params()

    mcmcobj = MCMC(params)
    mcmcobj.mcmcFX()
    mcmcobj.makeTableFX()
    mcmcobj.plotFX(params)

if __name__ == '__main__':
    main()

