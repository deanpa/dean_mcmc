#!/usr/bin/env python

import os
import pickle
#from scipy import stats
import numpy as np
#import pylab as P
from mcmcBernReg import ProcessedData  # Class structure defined in mcmcBernReg


##### global functions
##
def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))
##### end global functions

class BernData(object):
    def __init__(self):
        """
        Object to generate Bernouilli data
        """
        self.ndat = 200

        # betas used to simulate data
        self.b = np.array([-.75, 0.5, -1.0])
        self.sig = 0.5
        self.npara = len(self.b)

        # make x data
        self.x = np.empty((self.ndat, self.npara))
        self.x[:,0] = 1.0
        xTmp = np.random.uniform(-10, 10, size = self.ndat)
        xTmp = (xTmp - np.mean(xTmp)) / np.std(xTmp)
        self.x[:,1] = xTmp

        xTmp = np.random.uniform(5,75, size = self.ndat)
        xTmp = (xTmp - np.mean(xTmp)) / np.std(xTmp)
        self.x[:, 2] = xTmp

        # simulate y data
        mu = np.dot(self.x, self.b)
        
        Ltheta = np.random.normal(mu, self.sig)
        theta = inv_logit(Ltheta)
        self.y = np.random.binomial(1, theta)

        print('sum y', np.sum(self.y), theta)



########            Main function
#######
def main():

    bernpath = os.getenv('NESIPROJDIR', default = '.')
    # run berndata and pickle results
    berndata = BernData()

    # create instance of ProcessedData from mcmcBernReg.py
    processeddata = ProcessedData(berndata)
    # pickle data to be used in mcmcBern.py
    outberndata = os.path.join(bernpath,'out_berndata.pkl')
    fileobj = open(outberndata, 'wb')
    pickle.dump(processeddata, fileobj)
    fileobj.close()


if __name__ == '__main__':
    main()
