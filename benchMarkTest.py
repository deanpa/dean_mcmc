import numpy as np
import timeit

# Creating large boolean arrays
size = 1000000  # Setting array size to 1,000,000 for substantial benchmarking
mask1 = np.random.choice([True, False], size=size)
mask2 = np.random.choice([True, False], size=size)

# Defining the test setups as strings for timeit
and_test = "mask1 & mask2"
logical_and_test = "np.logical_and(mask1, mask2)"
multiplication_test = "mask1 * mask2"

# Number of repetitions
number_of_repetitions = 100

# Timing the logical AND operator
and_time = timeit.timeit(and_test, globals=globals(), number=number_of_repetitions)

# Timing the np.logical_and function
logical_and_time = timeit.timeit(logical_and_test, globals=globals(), number=number_of_repetitions)

# Timing the multiplication
multiplication_time = timeit.timeit(multiplication_test, globals=globals(), number=number_of_repetitions)

# Printing results
print(f"Logical AND operator time: {and_time:.5f} seconds")
print(f"np.logical_and function time: {logical_and_time:.5f} seconds")
print(f"Multiplication time: {multiplication_time:.5f} seconds")
