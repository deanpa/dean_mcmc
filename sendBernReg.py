#!/usr/bin/env python
#
#importing required python scripts
import mcmcBernReg
import optparse
import sys

class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--runparsfile", dest="runparsfile", help="Input pickled runpars file")
        p.add_option("--resultsfile", dest="resultsfile", help="Input pickled results file")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)
                                    

cmdargs = CmdArgs()
print(cmdargs.runparsfile)
print(cmdargs.resultsfile)

if len(sys.argv) == 1:
    # no runpars and don't write results
    mcmcBernReg.main()

else:
    mcmcBernReg.main( pklresults = cmdargs.resultsfile, pklrunpars = cmdargs.runparsfile ) 
           
#mcmcBernReg.main( pklrunpars = cmdargs.runparsfile ) 
           
                
