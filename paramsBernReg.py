#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
"""
This file contains classes for all the parameter values needed to run a Gibbs MCMC for Bernouilli regression

Classes include: 

Priors: priors 
Params: initial coefficient estimates of the model and mcmc parameters eg. no. of runs, 
      thinning and burnin rates

Can be used with mcmcBernRed.py
@author: cruzbernalj and Dean Anderson
"""

#importing required modules and python scripts
import numpy as np


#defining initial parameter estimates
class Pars(object):
    """
    Initial coefficient estimates of the model.
    attributes:
    b = initial b 
    """
    def __init__(self):
        self.b = np.array([1, -1.0, .05]) #initial b estimate

#defining priors
class Priors(object):
    """
    Vague priors.
    attributes: 
    bprior = beta priors, vmat = covariance matrix.
    """
    def __init__(self):

        # VaguePriors
        self.meanB = 0.0
        self.varB = np.sqrt(1000.0)
        self.npara = 3

#defining mcmc run parameters
class MCMCPars(object):
    '''
    Values to determine the mcmc run.
    attributes: 
    ngibbs = number of iterations to run, thin = thin rate, 
    burnin = burnin rate
    '''
    def __init__(self):
        self.betaJump = .1
        self.ngibbs = 3000  #number of runs
        self.thin = 1       # thin rate
        self.burnin = 0  # burn in number of iterations
        self.totalIterations = (self.ngibbs * self.thin) + self.burnin
        self.keep = np.arange( start = self.burnin, stop = self.totalIterations, step = self.thin)
        

########            Main function
#######
def main():
    """
    Main function
    """

if __name__ == '__main__':
    main()

 

 


       
