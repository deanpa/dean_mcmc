#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 23 12:10:43 2022

@author: dean
"""


from scipy import stats
from scipy.special import gamma
from scipy.special import gammaln
import numpy as np
import pylab as P

def nblpmf(y, lamb, psi):
    """
    ## FROM FU 2015; MAA PAPER
    """
    lpmf = (gammaln(y + psi + 1) - gammaln(y + 1) - gammaln(psi + 1) + 
            (y * np.log(lamb / (lamb + psi))) +
            (psi * np.log(psi / (lamb + psi))))
    return(lpmf)

def plotpmf(y, lpmf, lamb):
    P.figure()
    P.plot(y, lpmf, color='k')
    P.axvline(x = lamb)
#    P.show()



psi = 50
y = np.arange(100)
lamb= 25.0
var0 = lamb * ( 1 + lamb / psi)
print('var0', var0)

lpmf = nblpmf(y, lamb, psi)
plotpmf(y, lpmf, lamb)

lpoiss = stats.poisson.logpmf(y, lamb)

#P.Figure()
P.plot(y, lpoiss, color='b')
#P.axvline(x = lamb)
#P.show()


## SECOND PMF MODEL     MUTISO 2022 PAPER
def nb2(y, a, psi):
    lpm = (gammaln(y + 1.0/a) - gammaln(1.0/a) - gammaln(y+1) + ((1/a) * np.log(1-psi)) +
           (y * np.log(psi)))
    P.Figure()
    P.plot(y, lpm, color='g')
    mu = psi / (a * (1-psi))
    var = mu * (1 + a * mu)
    print('mu', mu, 'var', var)
#    P.axvline(x = mu)
#    P.title('second pmf')
#    P.show()

a = .001
mu = 25.0
psi = .28
a = psi / (mu * (1 - psi))
print('a', a, 'y', y)
nb2(y, a, psi)



## CLARK VARIATION

def nbClark(y, a, m):
    lpmf = (gammaln(a+y) - gammaln(a) - gammaln(y+1) + 
        (y * np.log(m/a)) - ((a+y) * np.log(1 + m/a)))
    return(lpmf)

def clarkTest():
    y = np.arange(1.0, 100.0)
    a = 10000.0
    mu = 25.
    lpmf = nbClark(y, a, mu)
#    P.figure()
    P.plot(y, lpmf, color='r')
    P.title('Clark')
#    P.show()

clarkTest()


## JAGS NEG BINOMIAL

def nbJags(y, mu, p):
    r = mu * p / (1-p)
    lpmf = gammaln(y+r) - gammaln(y+1) - gammaln(r) + (r*np.log(p)) + (y*np.log(1-p))
    print('JAGS r', r)
    return(lpmf)

def testNBjags():
    mu = 25.0
    p = 0.95
    y = np.arange(90)
    lpmfJags = nbJags(y, mu, p)
    P.plot(y, lpmfJags, color = 'y', linewidth = 2.5)
    P.show()

testNBjags()

