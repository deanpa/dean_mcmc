#!/usr/bin/env python

import os
import numpy as np
import datetime
from scipy import stats
import pylab as P


def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)

def formatDate(inArray):
    n = len(inArray)
    outArray = np.empty(n, dtype = datetime.date)
    for i in range(n):
        date_i = inArray[i]
        if date_i != '':
            outArray[i] = datetime.datetime.strptime(date_i, '%Y-%m-%d').date()
        else:
            outArray[i] = datetime.date(1900, 1, 1).date()
    return(outArray)



def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

 

def pidgeon_hole(n, n_bins): 
    if n >= n_bins:
        quotient = n // n_bins
        remainder = n % n_bins
        bins = np.repeat(quotient, n_bins)
        randBin = np.random.choice(n_bins, remainder, replace = False)
    else:
        bins = np.zeros(n_bins, dtype=int)
        randBin = np.random.choice(n_bins, n, replace = False)
    bins[randBin] += 1
    return bins




class Params(object):
    def __init__(self):
        """
        Set parameters for poa model
        """
        ## set Data names
        self.inputDataPath = os.path.join('Data')

        self.poaInFname = os.path.join(self.inputDataPath, 
            'Testing_Eth_DHB_Age_Contact_Tracing_20210829.csv')
        self.dbhRROutFname = os.path.join(self.inputDataPath, 'dhbRelRisk.csv')
        self.rrFname = os.path.join(self.inputDataPath, 'rrData2.csv')
        self.ethAgeContRelRiskFname = os.path.join(self.inputDataPath, 
            'ethAgecontRelRisk.csv')

        ###################################################
        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 500    # 1000  #3000  # number of estimates to save for each paramet
        self.thinrate = 10   #10   #30   # 200      # thin rate
        self.burnin = 2000     # burn in number of iterations

        self.totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
        ## array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + 
            self.burnin), self.thinrate)


        self.startDate = datetime.date(2021,8,17)
        self.endDate = datetime.date(2021,8,29)
        self.prior = [0.0, 1.0]
        self.beta = np.repeat(0.01, 12)
        self.searchBeta = 0.14

class RegressData(object):
    def __init__(self, params):
        """
        Object to read in  data
        """
        self.params = params

        #############################################
        ############ Run basicdata functions
        self.readData()
        self.readRR()
        self.makeDataArrays()
        self.makeXArray()
        self.mcmc()
        self.tracePlotFX()

        

    def readData(self):
        mohDat = np.genfromtxt(self.params.poaInFname,  delimiter=',', 
            names=True, dtype=['S10', 'S64', 'S10', 'S64', 'S32', 'S10', 
            'i8', 'i8', 'i8', 'f8', 'f8'])
        self.mohDate = mohDat['FirstMessageEventDtm']
        self.mohDHB = mohDat['DHB']
        self.mohEthnicity = mohDat['Ethnicity']
        self.mohAge = mohDat['AgeGroup5Year']
        self.mohNTests = mohDat['Total_Tests']
        self.mohPositive = mohDat['Positive']
        self.nMohDat = len(self.mohNTests)
        ## ADD RANDOM VACC STATUS
        self.vaccineStatus = np.random.binomial(1, 0.05, self.nMohDat)
        self.mohDate = decodeBytes(self.mohDate, self.nMohDat)
        self.mohDate = formatDate(self.mohDate)
        self.mohDHB = decodeBytes(self.mohDHB, self.nMohDat)
        self.mohEthnicity = decodeBytes(self.mohEthnicity, self.nMohDat)
        self.mohAge = decodeBytes(self.mohAge, self.nMohDat)
        self.mohContact = decodeBytes(mohDat['POTENTIAL_CONTACT_FLG'], self.nMohDat)
        
#        self.uMohAge = np.unique(self.mohAge)
#        self.nUMohAge = len(self.uMohAge)

        self.tmpUAge = np.array(['0 to 4', '10 to 14', '15 to 19', '20 to 24', '25 to 29', 
            '30 to 34', '35 to 39', '40 to 44', '45 to 49', '5 to 9', '50 to 54', 
            '55 to 59', '60 to 64', '65 to 69', '70 to 74', '75 to 79',
            '80 to 84', '85 and over'])

        self.mohAgeIndx = np.array([0, 1,1, 2,2,2,2, 3,3, 1, 3,3,3, 4,4,4,4,4])
#        for i in range(self.nUMohAge):
#            print('i', i, 'indx', self.mohAgeIndx[i], self.uMohAge[i])



    def readRR(self):
        rrDat = np.genfromtxt(self.params.rrFname,  delimiter=',', 
            names=True, dtype=['S32', 'S64', 'f8'])
        self.rrNode = rrDat['Node']
        self.nRR = len(self.rrNode)
        self.rrNode = decodeBytes(self.rrNode, self.nRR)
        self.rrGroup = decodeBytes(rrDat['Group'], self.nRR)
        self.rr = rrDat['RR']
        self.uDHB = self.rrGroup[self.rrNode == 'DHB']
        self.nUDHB = len(self.uDHB)
        self.uEthnic = self.rrGroup[self.rrNode == 'Ethnicity']
        self.nUEthnic = len(self.uEthnic)
        self.uAge = self.rrGroup[self.rrNode == 'Age']
        self.nUAge = len(self.uAge)
        self.uVaccine = self.rrGroup[self.rrNode == 'Vaccine']
        self.nUVaccine = len(self.uVaccine)
        self.uContact = self.rrGroup[self.rrNode == 'Contact']
        self.nUContact = len(self.uContact)

    def makeDataArrays(self):
        self.dates = []
        self.dhb = []
        self.ethnic = []
        self.age = []
        self.contact = []
        self.posTests = []
        self.totalTests = []
        self.dhbIn = np.array(['Auckland', 'Counties Manukau', 'Waitemata', 
            'Capital and Coast'])
        self.ndhb = len(self.dhbIn)
        self.nDays  = (self.params.endDate - self.params.startDate).days + 1
        day_i = self.params.startDate
#        cc = 0
        for i in range(self.nDays):
            dayMask = self.mohDate == day_i
            for j in range(self.ndhb):
                dhb_j = self.dhbIn[j]
                dhbMask = self.mohDHB == dhb_j
                daydhbmask = dayMask & dhbMask
                nPos = np.sum(self.mohPositive[daydhbmask])
                if nPos == 0:
                    continue
                for k in range(self.nUEthnic):
                    eth_k = self.uEthnic[k]
                    ethMask = self.mohEthnicity == eth_k
                    daydhbethmask = daydhbmask & ethMask
                    for l in range(self.nUAge):
                        age_l = self.uAge[l]
                        age_indx_l = np.where(self.uAge == age_l)[0]
                        mohAgeValuesMask = self.mohAgeIndx == age_indx_l
                        mohAgeValues = self.tmpUAge[mohAgeValuesMask]
                        mohagemask = np.in1d(self.mohAge, mohAgeValues)
                        dayDHBEthAgeMask = daydhbethmask & mohagemask
                        for m in range(self.nUContact):
                            cont_m = self.uContact[m]
                            contMask = self.mohContact == cont_m
                            bigMask = dayDHBEthAgeMask & contMask
                            nPos = np.sum(self.mohPositive[bigMask])
                            nTot = np.sum(self.mohNTests[bigMask])
                            self.dates.append(day_i)
                            self.dhb.append(dhb_j)
                            self.ethnic.append(eth_k)
                            self.age.append(age_l)
                            self.contact.append(cont_m)
                            self.posTests.append(nPos)
                            self.totalTests.append(nTot)
#                            if cc < 20:
#                                print('day', day_i, dhb_j, eth_k, age_l, cont_m, nPos, nTot)
#                            cc += 1
            day_i += datetime.timedelta(days=1)
        self.dates = np.array(self.dates)
        self.dhb = np.array(self.dhb)
        self.ethnic = np.array(self.ethnic)
        self.age = np.array(self.age)
        self.contact = np.array(self.contact)
        self.posTests = np.array(self.posTests)
        self.totalTests = np.array(self.totalTests)


    def makeXArray(self):
        self.beta = self.params.beta
        self.nDat = len(self.age)
        self.nBeta = 12 
        self.X = np.zeros((self.nDat, self.nBeta))
        self.X[:,0] = 1.0
        self.X[:,-1] = np.where(self.contact == 'Y', 1.0, 0.0)
        self.covNames = np.array(['Intercept', 'Capital and Coast', 'Counties Manukau',
            'Waitemata', 'Maori', 'Other', 'Pacific', '5-19', '20-39', '40-64', 
            '65+', 'Y'])

        self.covDHB = np.array(['Capital and Coast', 'Counties Manukau', 'Waitemata'])
        self.covEth = np.array(['Maori', 'Other', 'Pacific'])
        self.covAge = np.array(['5-19', '20-39', '40-64', '65+'])

        for i in range(0, len(self.covDHB)):
            cov_i = self.covDHB[i]
            self.X[:, (i+1)] = np.where(self.dhb == cov_i, 1.0, 0.0)
            for j in range(len(self.covEth)):
                eth_j = self.covEth[j]
                colIndx = ((j+1) + len(self.covDHB))
                self.X[:, colIndx] = np.where(self.ethnic == eth_j, 1.0, 0.0)
                for k in range(len(self.covAge)):
                    age_k = self.covAge[k]
                    ageIndx = ((k+1) + len(self.covDHB) + len(self.covEth))
                    self.X[:, ageIndx] = np.where(self.age == age_k, 1.0, 0.0)

        self.betaGibbs = np.zeros((self.params.ngibbs, self.nBeta))
        self.lth = np.dot(self.X, self.beta)
        self.th = inv_logit(self.lth)
        self.lpmf = stats.binom.logpmf(self.posTests, self.totalTests, self.th)

    def betaUpdate(self):
        for i in range(self.nBeta):
            beta_s = self.beta.copy()
            beta_s[i] = np.random.normal(self.beta[i], self.params.searchBeta)
#            mask = self.X[:, i] == 1.0
            x_s = self.X.copy()
            lth_s = np.dot(x_s, beta_s)
            th_s = inv_logit(lth_s)
            lpmf_s = np.sum(stats.binom.logpmf(self.posTests, 
                self.totalTests, th_s))
            prior_s = stats.norm.logpdf(beta_s[i], self.params.prior[0], 
                self.params.prior[1])
            pnew = lpmf_s + prior_s
            lpmf_i = np.sum(self.lpmf)
            prior = stats.norm.logpdf(self.beta[i], self.params.prior[0], 
                self.params.prior[1])
            pnow = lpmf_i + prior
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.beta = beta_s.copy()
                self.lth = lth_s.copy()
                self.th = th_s.copy()
                self.lpmf = lpmf_s.copy()






    ########            Main mcmc function
    ########
    def mcmc(self):
        cc = 0
        for self.g in range(self.params.totalIterations):

            self.betaUpdate()

            if self.g in self.params.keepseq:
                self.betaGibbs[cc] = self.beta
                cc += 1

#        print('beta', np.round(self.betaGibbs,2))


           

    def tracePlotFX(self):
        """
        plot diagnostic trace plots
        """
        ncol = np.shape(self.betaGibbs)[1]
        nTotalFigs = ncol
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        cc = 0
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < nTotalFigs:
                    P.plot(self.betaGibbs[:, cc])
                    P.title(self.covNames[cc])
                cc += 1
            P.show(block=lastFigure)





######################
# Main function
def main():
    # set paths to scripts and data


    params = Params()

    regressdata = RegressData(params)



if __name__ == '__main__':
    main()

