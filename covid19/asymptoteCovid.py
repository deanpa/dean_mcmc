#!/usr/bin/env python

from scipy import stats
from scipy.special import gamma
import numpy as np
import pylab as P
import prettytable
import datetime

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)



class CovidData(object):
    def __init__(self):
        """
        Object to generate Bernouilli data
        """
        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 3000           # number of estimates to save for each parameter
        self.thinrate = 60       # thin rate
        self.burnin = 300          # burn in number of iterations
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + 
            self.burnin), self.thinrate)

        self.iter = (self.ngibbs * self.thinrate) + self.burnin

        ####################
        self.r = 0.3
        self.rPriors = np.array([0.29, 0.5])
        self.rSearch = 0.03

#        self.lastDate = datetime.datetime(2020,4,2)
        self.plotFName = 'CovidAsymptotic_6April2020.png'

        self.K = 1100
        self.kSearch = 40
        self.kPriors = np.array([1400, 1000]) # or uniform
        ####################


        self.data = np.genfromtxt('NZ_COVID19-GH.csv',  delimiter=',', names=True,
            dtype=['S10', 'i8', 'i8'])
        
        self.day = self.data['DAY']
        self.obs = self.data['OBS']
        self.ndat = len(self.day)
        self.DATE = decodeBytes(self.data['DATE'], self.ndat)
        self.minK = self.obs[-1]
        ###################
        ## RUN FUNCTIONS
        self.getJulDay()
        self.getInitialMu()
        self.mcmcFX()
        self.makeTableFX()
        self.plotFX()
        self.calcPred()
        self.plotPredCI()
        ##
        ##################

    def getJulDay(self):
        self.julDay = np.zeros(self.ndat)
        self.dayDiff = np.zeros(self.ndat)
        self.date = np.empty(self.ndat, dtype = datetime.date)
        startdate = datetime.datetime.strptime(self.DATE[0], '%d/%m/%y')
        self.lastDate = datetime.datetime.strptime(self.DATE[-1], '%d/%m/%y')
        print('startdate', startdate, 'end date', self.lastDate)
        for i in range(self.ndat):
            date_i = datetime.datetime.strptime(self.DATE[i], '%d/%m/%y')
            self.date[i] = date_i
            self.julDay[i] = (date_i - startdate).days
            if i > 0:
                daydiff = self.date[i] - self.date[i - 1]
                self.dayDiff[i] = daydiff.days
#        print('self.date', self.date, 'jul', self.julDay, 'ddiff', self.dayDiff)
        self.dateMask = self.date <= self.lastDate
        self.julDay = self.julDay[self.dateMask]
        self.dayDiff = self.dayDiff[self.dateMask]
        self.date = self.date[self.dateMask]
        self.obs = self.obs[self.dateMask]
        self.ndat = len(self.obs)

    def getInitialMu(self):
        self.mu = self.obs[:-1] * np.exp(self.r * (1.- (self.obs[:-1] / self.K)))
        self.logPoisPMF = np.sum(stats.poisson.logpmf(self.obs[1:], self.mu))
        self.pR = stats.norm.logpdf(np.log(self.r), np.log(self.rPriors[0]), 
                self.rPriors[1])
#        self.pK = stats.norm.logpdf(self.K, self.kPriors[0], self.kPriors[1])
        (ks, self.p_ks_K_Log, self.p_K_ks_Log) = self.getKStar()
        self.pnow = self.logPoisPMF + self.pR + self.p_ks_K_Log
#        print('obs', self.obs, 'mu', self.mu, self.logPoisPMF)

########            MAIN MCMC FUNCTION
########
    def mcmcFX(self):
        """
        ## RUN FUNCTIONS TO UPDATE PARAMETERS WITH MCMC
        """
        cc = 0          ## COUNTER
        ## EMPTY ARRAYS TO POPULATE WITH MCMC
        self.rgibbs = np.zeros(self.ngibbs)
        self.kgibbs = np.zeros(self.ngibbs)
        
        ## LOOP THROUGH ITERATIONS OF MCMC
        for g in range(self.iter):

            self.updateR()
            self.updateK()

            ## POPULATE STORAGE ARRAYS WITH PARAMETER ESTIMATES
            if (g in self.keepseq):
                self.rgibbs[cc] = self.r
                self.kgibbs[cc] = self.K
                cc += 1

    def updateR(self):
        rs = np.random.normal(self.r, self.rSearch)
        mu_s = self.obs[:-1] * np.exp(rs * (1.0 - (self.obs[:-1] / self.K)))
        logPoisPMF_s = np.sum(stats.poisson.logpmf(self.obs[1:], mu_s))
        rPriors_s = stats.norm.logpdf(np.log(rs), np.log(self.rPriors[0]), 
                self.rPriors[1])
        pnew = logPoisPMF_s + rPriors_s + self.p_ks_K_Log

#        print('pmf', self.logPoisPMF, 'pmf_s', logPoisPMF_s)

        pdiff = pnew - self.pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0)
        if (rValue > zValue):
            self.r = rs
            self.logPoisPMF = logPoisPMF_s
            self.pR = rPriors_s
            self.pnow = pnew

    def getKStar(self):
        """
        ## FIND SEARCH DOMAIN FOR ks, ks, AND MH PROBABILITIES 
        """
        lowSearch = self.K - self.kSearch
        if lowSearch < self.minK:
            lowSearch = self.minK
        elif lowSearch == self.minK:
            lowSearch == self.minK + 1
        hiSearch = self.K + self.kSearch
        kDomain = np.arange(lowSearch, (hiSearch + 1))
        kDomain = kDomain[kDomain != self.K]
        lenK = len(kDomain)
        ## SELECT ks
        ks = np.random.choice(kDomain)
        ## get prob of ks given self.K
        self.p_ks_K_Log = np.log(1 / lenK)

        ### GET KS DOMAIN
        lowSearch = ks - self.kSearch
        if lowSearch < self.minK:
            lowSearch = self.minK
        elif lowSearch == self.minK:
            lowSearch == self.minK + 1
        hiSearch = ks + self.kSearch
        kDomain_s = np.arange(lowSearch, (hiSearch + 1))
        kDomain_s = kDomain_s[kDomain_s != ks]
        lenK_s = len(kDomain_s)
        ## GET PROB OF self.K GIVEN ks
        self.p_K_ks_Log = np.log(1 / lenK_s)
        return(ks, self.p_ks_K_Log, self.p_K_ks_Log)

    def updateK(self):
        """
        ## UPDATE WITH MH ALGORITHM
        """
        (ks, self.p_ks_K_Log, self.p_K_ks_Log) = self.getKStar()

#        ks = np.random.normal(self.K, self.kSearch)
        mu_s = self.obs[:-1] * np.exp(self.r * (1.0 - (self.obs[:-1] / ks))) 
        logPoisPMF_s = np.sum(stats.poisson.logpmf(self.obs[1:], mu_s))
#        kPriors_s = stats.norm.logpdf(ks, self.kPriors[0], 
#                self.kPriors[1])
        pnew = logPoisPMF_s + self.pR + self.p_K_ks_Log

#        print('pmf', self.logPoisPMF, 'pmf_s', logPoisPMF_s)

        pdiff = pnew - self.pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0)
        if (rValue > zValue):
            self.K = ks
            self.logPoisPMF = logPoisPMF_s
#           self.pK = kPriors_s
            self.p_ks_k_Log = self.p_K_ks_Log
            self.pnow = pnew


    def plotFX(self):
        """
        make diagnostic trace plots
        """
        P.figure(figsize=(10, 8))
        P.subplot(1,2,1)
        P.plot(self.rgibbs)
        P.subplot(1,2,2)
        P.plot(self.kgibbs)
        P.show()


    ################ Functions to make table
    def quantileFX(self, a):
        """
        function to calculate quantiles
        """
        return stats.mstats.mquantiles(a, prob=[0.025, 0.975])

    def makeTableFX(self):
        """
        Function to print table of results
        """
        self.resultTable = np.zeros((3, 3))
        ## BGIBBS
        self.resultTable[0, 0] = np.round(np.mean(self.rgibbs), 4)
        self.resultTable[0, 1:] = np.round(self.quantileFX(self.rgibbs), 4)
        self.resultTable[1, 0] = np.round(np.mean(self.kgibbs), 0)
        self.resultTable[1, 1:] = np.round(self.quantileFX(self.kgibbs), 0)
        doubleTime = np.log(2) / self.resultTable[0]
        self.resultTable[2,0] = np.round(doubleTime[0], 2)
        self.resultTable[2,1] = np.round(doubleTime[-1], 2)
        self.resultTable[2,2] = np.round(doubleTime[1], 2)
        aa = prettytable.PrettyTable(['Names', 'Mean', 'Low CI', 'High CI'])
        self.names = ['r', 'K', 'double time']
        for i in range(np.shape(self.resultTable)[0]):
            name = self.names[i]
#            print('i', i, 'name', name)
            row = [name] + self.resultTable[i].tolist()
            aa.add_row(row)
        print(aa)


    def calcPred(self):
        self.predictArray = np.zeros(((self.ndat) - 1, 3))
        for i in range(self.ndat - 1):
            predN = np.zeros(self.ngibbs)
            n0 = self.obs[i]
            for j in range(self.ngibbs):
                r = self.rgibbs[j]
                k = self.kgibbs[j]
                predN[j] = n0 * np.exp(r * (1.0 - (n0 / k)))
            self.predictArray[i, 0] = np.mean(predN)
            self.predictArray[i, 1:] = self.quantileFX(predN)
        print('predict', self.predictArray, 'obs', self.obs)

    def plotPred(self):
        self.getPrediction()
        P.figure()
        ax1 = P.gca()
        lns0 = ax1.plot(self.date[1:], self.predictArray[:, 0], color='k', 
                linewidth = '2', label = 'Mean prediction')
        lns1 = ax1.plot(self.date[1:], self.predictArray[:, 1], color='k', 
                ls = '--', label = '95% CI')
        lns2 = ax1.plot(self.date[1:], self.predictArray[:, 2], color='k', ls = '--')
        lns3 = ax1.plot(self.date, self.obs, 'ko', ms = 6.0, label = 'Observed cases')
        lns4 = ax1.plot(self.predDates, self.predCases, color = 'r', linewidth = '2',
                label = 'Forward projection') 
        lns5 = ax1.plot(self.predDates[1:], self.predCases[1:], 'ro', ms = 6.0)

        lns = lns0 + lns1 + lns3 + lns4
        labs = [l.get_label() for l in lns]

        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(10)
            tick.label.set_rotation(30)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(12)
        ax1.set_xlabel('Date', fontsize = 17)
        ax1.set_ylabel('Covid-19 cases', fontsize = 17)
        ax1.legend(lns, labs, loc = 'upper left')
        xMax = self.predDates[-1] + datetime.timedelta(days=0.5)
        xMin = self.date[0] - datetime.timedelta(days=0.5)
        ax1.set_xlim(xMin, xMax)
        P.tight_layout()
        FName = 'CovidModelled_R.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()

    def getPrediction(self):
        self.predDates = self.lastDate
        date_i = self.lastDate
        ndays = 14
        self.predCases = np.zeros(ndays)
        self.predCases[0] = self.obs[-1]
        for i in range(ndays - 1):
            date_i = date_i + datetime.timedelta(days=1)
            self.predDates = np.append(self.predDates, date_i)
            pred_i = self.predCases[i] * np.exp(self.resultTable[0, 0] *
                (1.0 -(self.predCases[i] / self.resultTable[1, 0])))
            self.predCases[i + 1] = pred_i
        print('preddates', len(self.predDates), 'cases', len(self.predCases))


    def plotPredCI(self):
        self.getPredictionCI()
        projectMeanN = np.append(self.obs[-1], self.forwardProj[0]) 
        P.figure()
        ax1 = P.gca()
        lns0 = ax1.plot(self.date[1:], self.predictArray[:, 0], color='k', 
                linewidth = '2', label = 'Mean prediction')
        lns1 = ax1.plot(self.date[1:], self.predictArray[:, 1], color='k', 
                ls = '--', label = '95% CI')
        lns2 = ax1.plot(self.date[1:], self.predictArray[:, 2], color='k', ls = '--')
        lns3 = ax1.plot(self.date, self.obs, 'ko', ms = 6.0, label = 'Observed cases')
        lns4 = ax1.plot(self.datesProjected, projectMeanN, color = 'r', linewidth = '2',
                label = 'Forward projection') 
        lns5 = ax1.plot(self.datesProjected[1:], self.forwardProj[0], 
                'ro', ms = 6.0)
        lns6 = ax1.plot(self.datesProjected[1:], self.forwardProj[1],
                color = 'r', ls = '--')
        lns7 = ax1.plot(self.datesProjected[1:], self.forwardProj[2],
                color = 'r', ls = '--')

        lns = lns0 + lns1 + lns3 + lns4
        labs = [l.get_label() for l in lns]

        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(10)
            tick.label.set_rotation(30)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(12)
        ax1.set_xlabel('Date', fontsize = 17)
        ax1.set_ylabel('Covid-19 cases', fontsize = 17)
        ax1.legend(lns, labs, loc = 'upper left')
        xMax = self.datesProjected[-1] + datetime.timedelta(days=0.5)
        xMin = self.date[0] - datetime.timedelta(days=0.5)
        ax1.set_xlim(xMin, xMax)
        P.tight_layout()
        P.savefig(self.plotFName, format='png', dpi = 500)
        P.show()

    def getPredictionCI(self):
        date_i = self.lastDate
        ndays = 14
        self.datesProjected = self.lastDate
        for i in range(ndays):
            date_i = date_i + datetime.timedelta(days=1)
            self.datesProjected = np.append(self.datesProjected, date_i)
        self.forwardProj = np.zeros((3, ndays))
        storePred = np.zeros((self.ngibbs, ndays))
        n0 = self.obs[-1]
        for i in range(self.ngibbs):
            r = self.rgibbs[i]
            k = self.kgibbs[i]
            n = self.obs[-1]
            for j in range(ndays):
                n = n * np.exp(r * (1.0 -(n / k)))
                storePred[i, j] = n
        self.forwardProj[0] = np.mean(storePred, axis = 0)
        self.forwardProj[1:] = stats.mstats.mquantiles(storePred,
            prob = [0.025, 0.975], axis = 0)
        print('forward projection', self.forwardProj)



########            Main function
#######
def main():
    coviddata = CovidData()

if __name__ == '__main__':
    main()
