#!/usr/bin/env python

import numpy as np
import pylab as P


def getBetaFX(mg0, g0Sd):
    a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    return (a, b)

class CovidPoA(object):
    def __init__(self):
        """
        Object to generate Bernouilli data
        """
        ##  SET PARAMETERS ################
        self.iter = 10
        self.days = np.arange(1, 28, dtype = int)
        self.n = len(self.days)
        self.present = 0.90     ## DAILY PROB OF 0.45
        self.swab = 0.97
        self.sero = 0.75
        self.intro =0.03
        self.gamma = 0.001
#        self.meanSe = .70
#        self.sdSe = .25
        self.meanPrior = .001
        self.sdPrior = .1
        self.RR = np.array([1, 5, 25, 45, 75])
        self.testNumbers = np.array([4500, 8000])
        self.target = 95
        ###################################


        ## RUN FUNCTIONS ##################
        self.getClinicalMeanSe()
        self.runPoA()
        self.plotPoA()
        ###################################

    def getClinicalMeanSe(self):
        ## NON-STRATEGIC       
        self.sumRR = np.sum(self.RR)
        self.prpRR = self.RR/self.sumRR
        ## G IS FOR GAMMA
        nG = 1000
        gArray = np.linspace(.0008, 0.0015, nG)
        seStore = np.zeros((nG, 2))
        P.figure()
        for j in range(2):
            nTests = self.testNumbers[j]
            regionT = nTests * self.prpRR

            for i in range(nG):
                wtse = (1 - np.exp(-gArray[i] * regionT)) * self.prpRR
                seStore[i, j] = np.sum(wtse)
            P.plot(gArray, seStore[:, j])
        P.show()
 

    def runPoA(self):
        """
        ## RUN THREE SCENARIOS: UNIFORM, STRATEGIC, INCREASED-STRATEGIC
        """
        self.poa = np.zeros((3, self.n))
        self.sumRR = np.sum(self.RR)
        self.prpRR = self.RR/self.sumRR
        self.nRegion = len(self.RR)
        ## 3 SCENARIOS
        ## UNIFORM SAMPLING
        self.getPoAScenarios()


    def getPoAScenarios(self):
        """
        ## UNIFORM SAMPLING PROPORTIONAL TO POPULATION
        """
        print('days', self.days)
        for i in range(3):
            if i == 0:
                regionT = self.testNumbers[0] / self.nRegion
            if i == 1:
                regionT = self.testNumbers[0] * self.prpRR
            if i == 2:
                regionT = self.testNumbers[1] * self.prpRR
            mnClinic = 1 - np.exp(-self.gamma * regionT)
            prior_j = self.meanPrior
            printDay = True
            
            for j in range(self.n):
                wtSe = self.present * mnClinic * self.swab * self.sero * self.prpRR
                SSe = np.sum(wtSe)
                post_j = prior_j / (1.0 - SSe*(1.0 - prior_j))
                self.poa[i, j] = post_j
                prior_j = (1.0 - self.intro) * post_j
                if (np.round(post_j * 100, 0) >= self.target) & printDay:
                    print('Scenario =', i, 'Day =', self.days[j], 
                            'PoA', np.round(post_j,2))
                if np.round(post_j*100,0) >= self.target:
                    printDay = False
                if i == 2:
                    print('day', j+1, 'poa', self.poa[:, j])


    def plotPoA(self):
        P.figure(figsize=(12,7))
        ax1 = P.gca()
        confidence = self.poa
        for i in range(3):    
            if i == 0:
                lns1 = ax1.plot(self.days, confidence[i], linewidth = 3, color='k', 
                    label='Non-strategic testing')
            elif i == 1:
                lns2 = ax1.plot(self.days, confidence[i], linewidth = 3, color='b', 
                    label='Strategic testing')
            else:
                lns3 = ax1.plot(self.days, confidence[i], linewidth = 3, color='r', 
                    label='Increased strategic testing')

        xmax = 24
        P.hlines(y = self.target / 100.0, xmin = 0, xmax = xmax, linestyles = 'dashed', 
            color='k', label = 'Target confidence level')

        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]

#        P.hlines(y = self.target, xmin = 0, xmax = xmax, linestyles = 'dashed', 
#            color='k', label = 'Target confidence level')
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)


        ax1.set_xlabel('Days', fontsize = 14)
        ax1.set_ylabel('Probability \nof absence', multialignment='center', 
                rotation = 0, fontsize = 14, ha='right')
        ax1.legend(lns, labs, loc='lower right')
        ax1.set_xlim(1, xmax)
        P.tight_layout()
        FName = 'PoAScenario.png'
        P.savefig(FName, format='png', dpi = 500)
        P.show()






########            Main function
#######
def main():
    covidpoa = CovidPoA()

if __name__ == '__main__':
    main()

