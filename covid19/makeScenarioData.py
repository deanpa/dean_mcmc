#!/usr/bin/env python

import os
import numpy as np
import pandas as pd
import datetime

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


def formatDate(inArray, sepType):
#    dateFormat = '%Y' + sepType + '%m'+ sepType + '%d'
    n = len(inArray)
    outArray = np.empty(n, dtype = datetime.date)
    for i in range(n):
        date_i = inArray[i]
        if date_i != '':
            outArray[i] = datetime.datetime.strptime(date_i, sepType).date()
#            outArray[i] = datetime.datetime.strptime(date_i, '%Y-%m-%d').date()
        else:
            outArray[i] = datetime.date(1900, 1, 1)
            print('No Date, line', i)
    return(outArray)


class Params(object):
    def __init__(self):
        """
        Set parameters for poa model
        """
        ## set Data names
        inputDataPath = os.path.join('Data')
        ## set Data names

        self.dataInFname = os.path.join(inputDataPath, 
            'covid_NZ_20210816.csv')
        self.outFname = os.path.join(inputDataPath, 
            'covid_Scen_NZ_48Days.csv') 



#        self.dataInFname = os.path.join(inputDataPath, 
#            'covid_ExclAuck_Increase_20210904.csv')
#        self.outFname = os.path.join(inputDataPath, 
#            'scen_ExclAuck_Increase_20210920.csv') 

#        self.dataInFname = os.path.join(inputDataPath, 'covid_ExclAuck_20210924.csv')
#        self.outFname = os.path.join(inputDataPath, 'scen_ExclAuck_20211001.csv') 


        self.keepDates = None   #np.array([datetime.date(2021,9,2), datetime.date(2021,9,4)])
        self.scenarioDates = np.array([datetime.date(2021,10,15), 
            datetime.date(2021,11,16)])
        self.poolDates = np.array([datetime.date(2021,8,1), datetime.date(2021,8,16)])

#        self.additionalDays = np.array([datetime.date(2021,8,7), datetime.date(2021,8,16)])

class ScenarioData(object):
    """
    read and get data
    """
    def __init__(self, params):
        self.params = params

        #########################################
        ## run functions  #######################
        self.readdata()
        self.makeDataBlock0()
        self.makeDataBlock1()
        self.makeDataBlock2()
#        self.addMoreDays()
#        self.changeDates()
        self.writePandasFile()

        ## end running functions  ###############
        #########################################

    def readdata(self):
        """
        ## read in data for known nests
        """
        self.testDat = pd.read_csv(self.params.dataInFname, delimiter=',')
        self.date = np.array(self.testDat[['Date']]).flatten()
        self.nDat = len(self.date)
        self.date = formatDate(self.date, '%Y-%m-%d')
        print('date', self.date[:30])
        self.uDates = np.unique(self.date)

    def makeDataBlock0(self):
        self.block0 = self.testDat.copy(deep=True)
        daysAdd = (self.params.scenarioDates[0] - self.params.poolDates[0]).days
        self.dateB0 = np.empty(self.nDat, dtype = datetime.date)
        for i in range(self.nDat):
            self.dateB0[i] = self.date[i] + datetime.timedelta(days = daysAdd)
        self.block0.loc[:, 'Date'] = self.dateB0
        print('block0', self.block0.loc[0:4, 'Date'], 
            self.block0.loc[self.nDat - 4:, ['Date']])


    def makeDataBlock1(self):
        self.block1 = self.block0.copy(deep=True)
        nPoolDays = (self.params.poolDates[1] - self.params.poolDates[0]).days + 1
        self.dateB1 = np.empty(self.nDat, dtype = datetime.date)
        for j in range(self.nDat):
            self.dateB1[j] = self.dateB0[j] + datetime.timedelta(days = nPoolDays)
        self.block1.loc[:, 'Date'] = self.dateB1
        print('block1', self.block1.loc[0:4, ['Date']], 
            self.block1.loc[self.nDat - 4:, ['Date']])

        self.scenDat = self.block0.append(self.block1, ignore_index = True)


    def makeDataBlock2(self):
        self.block2 = self.block1.copy(deep=True)
        nPoolDays = (self.params.poolDates[1] - self.params.poolDates[0]).days + 1
        self.dateB2 = np.empty(self.nDat, dtype = datetime.date)
        for j in range(self.nDat):
            self.dateB2[j] = self.dateB1[j] + datetime.timedelta(days = nPoolDays)
        self.block2.loc[:, 'Date'] = self.dateB2
        print('block2', self.block2.loc[0:4, ['Date']], 
            self.block2.loc[self.nDat - 4:, ['Date']])

        self.scenDat = self.scenDat.append(self.block2, ignore_index = True)




    def addMoreDays(self):
        udays = []
        nAddDays = (self.params.additionalDays[1] - self.params.additionalDays[0]).days + 1
        d_k = self.params.additionalDays[0]
        udays.append(d_k)
        for k in range(nAddDays):
            d_k = d_k + + datetime.timedelta(days = 1)
            udays.append(d_k)
        udays = np.array(udays)

        print('nAddDays', nAddDays, 'udays', udays)




        dayMask = np.in1d(self.date, udays)
        self.block2 = self.block1[dayMask].copy()
        dateB1Reduced = self.dateB1[dayMask] 



    

        nadd = self.block2.shape[0]

        print('block2 Rows', self.block2.iloc[:4, :4])

        print('nadd', nadd, 'dateB1Red', dateB1Reduced[:5], dateB1Reduced[-4:])

        print('shp bl2', self.block2.shape, 'len(dateB1Reduced)', len(dateB1Reduced))

        self.dateB2 = np.empty(nadd, dtype = datetime.date)
        for l in range(nadd):
            self.dateB2[l] = dateB1Reduced[l] + datetime.timedelta(days = nAddDays)

        print('finish loop', 'dateb2', self.dateB2[:5])

        self.block2.loc[:, 'Date'] = self.dateB2


        print('############## BLOCK2 Rows', self.block2.loc[:4, ['Date']])


        print('block2', self.block2.loc[0:4, ['Date']]) 
#            self.block2.loc[nadd - 4:, ['Date']])
        self.scenDat = self.scenDat.append(self.block2, ignore_index = True)




    def changeDates(self):
        self.keepDatesArr = []
        day_i = self.params.keepDates[0]
        self.keepDatesArr.append(day_i)
        self.nKeepDays  = (self.params.keepDates[1] - self.params.keepDates[0]).days + 1
        for i in range(1, self.nKeepDays):
            day_i += datetime.timedelta(days=1)
            self.keepDatesArr.append(day_i)
        changeMask = ~np.in1d(self.date, self.keepDatesArr)
        nAdd = (self.params.keepDates[1] - self.params.poolDates[0]).days + 1
        for i in range(self.nDat):
            if changeMask[i]:
                self.date[i] += datetime.timedelta(days=nAdd)




    def writePandasFile(self):
        """
        write pandas df to directory
        """
#        self.testDat.loc[:, 'Date'] = self.date
        self.scenDat.to_csv(self.params.outFname, sep=',', header=True, index_label = 'did')
#        self.testDat.to_csv(self.params.outFname, sep=',', header=True, index_label = 'did')

def main():

    params = Params()
    scenariodata = ScenarioData(params)
    

if __name__ == '__main__':
    main()

