#!/usr/bin/env python

import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles

def getBetaFX(mg0, g0Sd):
    a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    return (a, b)

def allocateSamples(prop, n):
    nSamp = len(prop)
    outList = []
    nRemain = n
    cc  = 0
    for i in range(nSamp):
        p_i = prop[i] / np.sum(prop[i:])
        s_i = np.round(p_i * nRemain, 0).astype(int)
        cc += s_i
        nRemain = nRemain - s_i
        outList.append(s_i)
#        print('pi', p_i, 'si', s_i, 'cc', cc, 'nRem', nRemain)
#    print('sum', np.sum(outList))
    return(outList)





class CovidPoA(object):
    def __init__(self):
        """
        Object to generate Bernouilli data
        """
        ##  SET PARAMETERS ################
        self.iter = 100
        self.nNodes = 3
        self.nElements = [5,5,5]
        self.totalGroups = np.prod(self.nElements)
        self.pSymptomatic = 0.7
        self.pSeek = 0.75
        self.meanSe = .96
        self.sdSe = .0001
        self.rrDomain = np.array([1,2,5,10,15,20,30, 50, 75, 100, 125]) 
        self.nTotalTests = np.int32(self.totalGroups * 20)
        self.popByGroup = 2000
        self.totalPop = self.popByGroup * self.totalGroups
        self.testablePop = np.int64(self.popByGroup * self.pSymptomatic * self.pSeek)
        print('testable pop', self.testablePop)        
        ###################################
        ## RUN FUNCTIONS ##################
        self.doIterations()
        ###################################

    def makeRisks(self):
        self.rrList = []       #np.zeros((self.nNodes, self.nElements))
        for i in range(self.nNodes):
            randPerm = np.random.choice(self.rrDomain, size = self.nElements[i],
                replace = True)
            minRand = np.min(randPerm)
            self.rrList.append(randPerm / minRand)
#        print('rrList', self.rrList)

    

    def doIterations(self):
        ## ELEMENT INDICATOR FOR EACH NODE WHERE INFECTION OCCURS
        self.infectNodeElement = np.zeros(self.nNodes, dtype = int)
        self.detectExactRandUni = np.zeros(3)
        self.propExactRandUni = np.zeros(3)
        self.SSe = np.zeros((self.iter, 3))
        self.herdSSe = np.zeros((self.iter, 3))
        for iter in range(self.iter):
            self.makeRisks()
            self.assignInfection()
            self.assignTests()
            self.getRandSe()
            self.getDetection()
            self.getSSe(iter)
            self.getHerdSSe(iter)
        self.propDetect = self.detectExactRandUni / self.iter
        print('nDetected', self.detectExactRandUni, 'self.propDetect', self.propDetect)
        print('mean SSe', np.mean(self.SSe, axis = 0))
        print('quantiles SSe', mquantiles(self.SSe, prob = [0.05, 0.5, 0.95],
            axis = 0))
        print('mean herdSSe', np.mean(self.herdSSe), 'quants herdSSe',
            mquantiles(self.herdSSe, prob = [0.05, 0.5, 0.95]))

    def getRandSe(self):
        (a,b) = getBetaFX(self.meanSe, self.sdSe)
        self.seTest = np.random.beta(a,b, size = 1)
#        print('seTest', self.seTest)


    def assignInfection(self):
        self.mnpList = []
        ## loop nodes
        for node in range(self.nNodes):
            mnProb = self.rrList[node] / np.sum(self.rrList[node])
            self.mnpList.append(mnProb)
            randInfect = np.random.multinomial(1, mnProb, size = 1)
            indx = np.where(randInfect[0] == 1)[0]
            self.infectNodeElement[node] = indx
#        print('mnp', mnProb, 'inf node', self.infectNodeElement,
#            'mnpList', self.mnpList)
        
    def assignTests(self):
        self.testUniformList = []
        self.testRandList = []
        self.testExactList = []
        nUniformTests = np.int64(self.nTotalTests / self.totalGroups)
        ## BY DHB
        dhbExactTests = allocateSamples(self.mnpList[0], self.nTotalTests)
        dhbRandTests = np.random.multinomial(self.nTotalTests, self.mnpList[0],
            size = 1)[0]
        ## DHB LIST
        dhbExactList = []
        dhbRandList = []
        dhbUniformList = []
        for dhb in range(self.nElements[0]):
            ethnicExactList = []      ##ETHNIC LIST
            ethnicRandList = []      ##ETHNIC LIST
            ethnicUniformList = []      ##ETHNIC LIST
            ethnicExactTest = allocateSamples(self.mnpList[1], dhbExactTests[dhb]) 
            ethnicRandTest = np.random.multinomial(dhbRandTests[dhb], self.mnpList[1])
            ageUniformTest = np.repeat(nUniformTests, self.nElements[2])
            ## LOOP ETHNIC GROUPS IN DHB
            for eth_i in range(self.nElements[1]):
                ageExactTest = np.array(allocateSamples(self.mnpList[2],
                    ethnicExactTest[eth_i])) 
                ageRandTest = np.random.multinomial(ethnicRandTest[eth_i], 
                    self.mnpList[2])
#                print('dhb', dhb, 'eth', eth_i, 'exacttest', ageExactTest)
                ethnicExactList.append(ageExactTest)
                ethnicRandList.append(ageRandTest)
                ethnicUniformList.append(ageUniformTest)
            self.testUniformList.append(ethnicUniformList)
            self.testRandList.append(ethnicRandList)
            self.testExactList.append(ethnicExactList)


    def getDetection(self):
        dhb = self.infectNodeElement[0]
        eth = self.infectNodeElement[1]
        age = self.infectNodeElement[2]
        ## PROPORTION SAMPLED IN INFECTED GROUP
        self.propExactRandUni[2] = self.testUniformList[dhb][eth][age] / self.testablePop 
        self.propExactRandUni[1] = self.testRandList[dhb][eth][age] / self.testablePop 
        self.propExactRandUni[0] = self.testExactList[dhb][eth][age] / self.testablePop  
#        print('self.propExactRandUni', self.propExactRandUni, 'se', self.seTest, 
#            'testExactList', self.testExactList[dhb][eth][age])
        self.detectExactRandUni[2] += np.random.binomial(1, self.propExactRandUni[2] *
            self.seTest)
        self.detectExactRandUni[1] += np.random.binomial(1, self.propExactRandUni[1] *
            self.seTest) 
        self.detectExactRandUni[0] += np.random.binomial(1, self.propExactRandUni[0] *
            self.seTest)
#        print('prop test E R U', self.propExactRandUni, 'detect', self.detectExactRandUni)
    

    def getSSe(self, iter):
        prpTested = np.zeros(3)
        wtAveSeDHB = np.zeros(3)
        ## LOOP DHB
        for dhb in range(self.nElements[0]):
            wtAveSeEth_dhb = np.zeros(3)
            ## LOOP ETHNIC GROUPS WITHIN DHB
            for eth in range(self.nElements[1]):
                wtAveSeAge_eth = np.zeros(3)
                ## LOOP AGE GROUPS IN ETHNIC GROUP IN DHB
                for age in range(self.nElements[2]):
                    prpTested[0] = self.testExactList[dhb][eth][age] / self.testablePop
                    prpTested[1] = self.testRandList[dhb][eth][age] / self.testablePop
                    prpTested[2] = self.testUniformList[dhb][eth][age] / self.testablePop
                    wtAveSeAge_eth += (self.seTest * prpTested * self.mnpList[2][age])
                wtAveSeEth_dhb += (wtAveSeAge_eth * self.mnpList[1][eth])
            wtAveSeDHB += (wtAveSeEth_dhb * self.mnpList[0][dhb])
        self.SSe[iter] = wtAveSeDHB
#        print('iter', iter, 'SSe', wtAveSeDHB)

    def getHerdSSe(self, iter):
        self.herdSSe[iter] = self.seTest * self.nTotalTests / self.totalPop


########            Main function
#######
def main():
    covidpoa = CovidPoA()

if __name__ == '__main__':
    main()

