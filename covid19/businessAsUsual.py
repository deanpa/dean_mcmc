#!/usr/bin/env python

import os
import numpy as np
import pandas as pd
import datetime

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


def formatDate(inArray, sepType):
#    dateFormat = '%Y' + sepType + '%m'+ sepType + '%d'
    n = len(inArray)
    outArray = np.empty(n, dtype = datetime.date)
    for i in range(n):
        date_i = inArray[i]
        if date_i != '':
            outArray[i] = datetime.datetime.strptime(date_i, sepType).date()
#            outArray[i] = datetime.datetime.strptime(date_i, '%Y-%m-%d').date()
        else:
            outArray[i] = datetime.date(1900, 1, 1)
            print('No Date, line', i)
    return(outArray)


class Params(object):
    def __init__(self):
        """
        Set parameters for poa model
        """
        ## set Data names
        inputDataPath = os.path.join('Data')
        ## set Data names

        self.dataInFname = os.path.join(inputDataPath, 
            'Testing_Eth_DHB_Age_Contact_Tracing_20210911.csv')
        self.outFname = os.path.join(inputDataPath, 
            'businessAsUsual.csv') 

        self.keepDates = np.array([datetime.date(2021,7,20), datetime.date(2021,8,15)])
        self.scenarioDates = np.array([datetime.date(2021,11,1), 
            datetime.date(2021,12,31)])

class ScenarioData(object):
    """
    read and get data
    """
    def __init__(self, params):
        self.params = params

        #########################################
        ## run functions  #######################
        self.readdata()
#        self.changeDates()
#        self.writePandasFile()

        ## end running functions  ###############
        #########################################

    def readdata(self):
        """
        ## read in data for known nests
        """
        self.testDat = pd.read_csv(self.params.dataInFname, delimiter=',')
        self.date = np.array(self.testDat[['FirstMessageEventDtm']]).flatten()
        self.nDat = len(self.date)
        self.date = formatDate(self.date, '%Y-%m-%d')
        print('date', self.date[:30])
        print('testDat', self.testDat.iloc[:5])

    def changeDates(self):
        self.keepDatesArr = []
        day_i = self.params.keepDates[0]
        self.keepDatesArr.append(day_i)
        self.nKeepDays  = (self.params.keepDates[1] - self.params.keepDates[0]).days + 1
        for i in range(1, self.nKeepDays):
            day_i += datetime.timedelta(days=1)
            self.keepDatesArr.append(day_i)
        changeMask = ~np.in1d(self.date, self.keepDatesArr)
        nAdd = (self.params.keepDates[1] - self.params.poolDates[0]).days + 1
        for i in range(self.nDat):
            if changeMask[i]:
                self.date[i] += datetime.timedelta(days=nAdd)




    def writePandasFile(self):
        """
        write pandas df to directory
        """
        self.testDat.loc[:, 'Date'] = self.date
        self.testDat.to_csv(self.params.outFname, sep=',', header=True, index_label = 'did')

def main():

    params = Params()
    scenariodata = ScenarioData(params)
    

if __name__ == '__main__':
    main()

