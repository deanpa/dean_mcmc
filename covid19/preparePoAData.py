#!/usr/bin/env python

import os
import numpy as np
import datetime
import pandas as pd

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)

def formatDate(inArray, sepType):
#    dateFormat = '%Y' + sepType + '%m'+ sepType + '%d'
    n = len(inArray)
    outArray = np.empty(n, dtype = datetime.date)
    for i in range(n):
        date_i = inArray[i]
        if date_i != '':
            outArray[i] = datetime.datetime.strptime(date_i, sepType).date()
#            outArray[i] = datetime.datetime.strptime(date_i, '%Y-%m-%d').date()
        else:
            outArray[i] = datetime.date(1900, 1, 1)
            print('No Date, line', i)
    return(outArray)


def pidgeon_hole(n, n_bins): 
    if n >= n_bins:
        quotient = n // n_bins
        remainder = n % n_bins
        bins = np.repeat(quotient, n_bins)
        randBin = np.random.choice(n_bins, remainder, replace = False)
    else:
        bins = np.zeros(n_bins, dtype=int)
        randBin = np.random.choice(n_bins, n, replace = False)
    bins[randBin] += 1
    return bins




class Params(object):
    def __init__(self):
        """
        Set parameters for poa model
        """
        ## set Data names
        self.inputDataPath = os.path.join('Data')
        ## TESTING DATA
        self.poaInFname = os.path.join(self.inputDataPath, 
            'Testing_Eth_DHB_Age_Contact_Tracing_Border_EWork_Crossing_20211208.csv')
        ## DATA FILE TO OUTPUT
        self.poaOutFname = os.path.join(self.inputDataPath, 
            'covidSouth20211208.csv')
        ## INPUT RR DATA
        self.rrFname = os.path.join(self.inputDataPath, 'rrSouthIsland.csv')
        ## NATIONAL ETHNICITY PROPORTION DATA INPUT
        self.ethnicPropFname = os.path.join(self.inputDataPath, 'ethnicityProp.csv')
        ## FLU INCIDENCE DATA ANALYSED FROM FLU TRACKER
        self.incidenceFname = os.path.join(self.inputDataPath, 
            'incidenceAllDates.csv')
        ## POPULATION DATA
        self.popDatFname = os.path.join(self.inputDataPath, 'pop_proj.csv')
        ## COMMUNITY CASE DATA FOR UPDATING RR        
        self.communityFname = os.path.join(self.inputDataPath, 
            'community_case_20210924.csv')
        ## UPDATED RR DATA FOR OUTPUT
        self.rrUpdateFname = os.path.join(self.inputDataPath, 'rrUpdateSouth.csv')
        ## TEST RATE BY GROUPS IF INCREASE TESTING IN SCENARIO
        self.testRateFname = os.path.join(self.inputDataPath, 
            'testRateSouth20211208.csv')


        self.startDate = datetime.date(2021,11,1)
        self.endDate = datetime.date(2021,12,8)

        self.updateRR = True
        if self.updateRR:
            self.rrDateRange = [datetime.date(2021,10,15), datetime.date(2021,11,30)]

        
        ## VACC RATE PREDICTED FOR MID OCTOBER
        self.vaccineAgePrp = np.array([0.0, 0.2, 0.85, 0.85, 0.90])
        self.vaccinePrpTest = 0.25

        ## IN 10 DAYS WILL GET 95% OF CONTACTS TESTED
        self.contactTestPool = [1000, 30]
        self.contactNTests = 348
        ## PROB SYMPTOMATIC FOR NON-VACC AND VACC 2-D ARRAY
        self.PSymptom = np.array([[0.7, 0.1],
                                  [0.5, 0.1]])
        self.PGive = [0.99, 0.01]
        self.PTest = [0.82, 0.04]
        ## PERCENT INCREASE TESTING FOR COMMUNITY AND CONTACT TESTING, RESPECTIVELY
        self.increaseTest = False
        self.rrBins = np.array([1, 100, 200, 300, 400])
        self.rrIncreaseTest = np.array([0.3, 0.4, 0.5, 0.6, 0.7])
#        ## 10% INCREASE FOR CONTACT TRACING = 418 / 1000
#        self.increaseContactTests = 430 #np.round(self.contactNTests * 1.25, 0)

class ManipData(object):
    def __init__(self, params):
        """
        Object to read in  data
        """
        self.params = params

        #############################################
        ############ Run basicdata functions
        self.getDates()
        self.readData()
        self.readRR()
        self.readCommunityCases()

        if self.params.updateRR:
            self.getRRDates()
            self.updateRelRisk()
            self.writeRelRisks()

        self.correctAgeData()

        self.readEthnicity()
        self.readIncidence()
        self.readPopData()

        print('READ ALL DATA, UPDATED RR AND CORRECTED AGE DATA') 

        self.makeBasicArrays()

        print('MADE BASIC ARRAYS')
        self.dataByDates()
        print('MADE DATES')
        self.populateRR()
        print('POPULATED RR')
        self.redistributeUnknownEthnic()
        print('REDISTRIBUTED UNKNOWN ETHNIC')
        self.getPSeek()
        print('OBTAINED TEST POOL AND NTESTS')
        if self.params.increaseTest:
            self.writeTestsToFile()
            print('INCREASED TEST NUMBERS, IF IN INCREASE TESTING SCENARIO')
        self.writeToFile()


    def getDates(self):
        self.Dates = []
        day_i = self.params.startDate
        self.Dates.append(day_i)
        self.nDays  = (self.params.endDate - self.params.startDate).days + 1
        for i in range(1, self.nDays):
            day_i += datetime.timedelta(days=1)
            self.Dates.append(day_i)
        self.uDates = np.unique(self.Dates)
        self.nUDates = len(self.uDates)
        print('udates', self.uDates)

    def getRRDates(self):
        self.rrDates = []
        d_i = self.params.rrDateRange[0]
        self.rrDates.append(d_i)
        n = (self.params.rrDateRange[1] - self.params.rrDateRange[0]).days + 1
        for i in range(1, n):
            d_i += datetime.timedelta(days=1)
            self.rrDates.append(d_i)
        self.uRRDates = np.unique(self.rrDates)
        self.nURRDates = len(self.uRRDates)
        print('u rel risk dates', self.uRRDates)        


    def readData(self):
        mohDat = np.genfromtxt(self.params.poaInFname,  delimiter=',', 
            names=True, dtype=['S10', 'S64', 'S10', 'S64', 'S32', 'S10', 'S10',
            'i8', 'f8', 'f8'])
        self.mohDate = mohDat['FirstMessageEventDtm']
        self.nMohDat = len(self.mohDate)
        self.mohDate = decodeBytes(self.mohDate, self.nMohDat)
        self.mohDate = formatDate(self.mohDate, '%Y-%m-%d')
        ## SUBSET BY DATES
        dateMask = np.in1d(self.mohDate, self.uDates)
        self.mohDate = self.mohDate[dateMask]
        self.nMohDat = len(self.mohDate)
        self.mohDHB = mohDat['DHB'][dateMask]

        self.mohEthnicity = mohDat['Ethnicity'][dateMask]
        self.mohAge = mohDat['AgeGroup5Year'][dateMask]
        self.mohNTests = mohDat['Total_Tests'][dateMask]
        
        self.mohDHB = decodeBytes(self.mohDHB, self.nMohDat)
        self.mohEthnicity = decodeBytes(self.mohEthnicity, self.nMohDat)
        self.mohAge = decodeBytes(self.mohAge, self.nMohDat)
        self.mohContact = mohDat['POTENTIAL_CONTACT_FLG'][dateMask]
        self.mohContact = decodeBytes(self.mohContact, self.nMohDat)
        self.essentialWork = decodeBytes(mohDat['Border_Cross_Ess_Worker_Flag'], 
            self.nMohDat)
        self.nonEssentialMask = self.essentialWork == 'N'
        
        self.tmpUAge = np.array(['0 to 4', '10 to 14', '15 to 19', '20 to 24', '25 to 29', 
            '30 to 34', '35 to 39', '40 to 44', '45 to 49', '5 to 9', '50 to 54', 
            '55 to 59', '60 to 64', '65 to 69', '70 to 74', '75 to 79',
            '80 to 84', '85 and over'])
        self.mohAgeIndx = np.array([0, 1,1, 2,2,2,2, 3,3, 1, 3,3,3, 4,4,4,4,4])

    def readRR(self):
        rrDat = np.genfromtxt(self.params.rrFname,  delimiter=',', 
            names=True, dtype=['S32', 'S64', 'f8', 'S10'])
        self.rrNode = rrDat['Node']
        self.nRR = len(self.rrNode)
        self.rrNode = decodeBytes(self.rrNode, self.nRR)
        self.rrGroup = decodeBytes(rrDat['Group'], self.nRR)
        self.rr = rrDat['RelRisk']
        self.rrInAnalysis = decodeBytes(rrDat['InAnalysis'], self.nRR)
        self.uDHB = self.rrGroup[self.rrNode == 'DHB']
        self.nUDHB = len(self.uDHB)
        self.uEthnic = self.rrGroup[self.rrNode == 'Ethnicity']
        self.nUEthnic = len(self.uEthnic)
        self.uAge = self.rrGroup[self.rrNode == 'Age']
        self.nUAge = len(self.uAge)
        self.uVaccine = self.rrGroup[self.rrNode == 'Vaccine']
        self.nUVaccine = len(self.uVaccine)
        self.uContact = self.rrGroup[self.rrNode == 'Contact']
        self.nUContact = len(self.uContact)
        if ~self.params.updateRR:
            self.rrUpdated = self.rr


    def readCommunityCases(self):
        commDat = np.genfromtxt(self.params.communityFname,  delimiter=',', 
            names=True, dtype=['S32', 'S32', 'S10', 'S32', 'S64', 'S64', 'S32', 
                'S32', 'S32'])
        self.commDate = commDat['ReportDate']
        self.nCommDat = len(self.commDate)
        self.commDate = decodeBytes(self.commDate, self.nCommDat)
        self.commDate = formatDate(self.commDate, '%Y-%m-%d')
        self.commAge = decodeBytes(commDat['AgeGrp5'], self.nCommDat)
        self.commEthnic = decodeBytes(commDat['EthnicityPrioritisedMPAMEU'], self.nCommDat)
        self.commContact = decodeBytes(commDat['HowDiscov'], self.nCommDat)
        ## REMOVE UNKNOWN AGES
        remvMask = self.commAge != 'Unknown'
        self.commAge = self.commAge[remvMask]
        self.commEthnic = self.commEthnic[remvMask]
        self.commContact = self.commContact[remvMask]
        self.commDate = self.commDate[remvMask]
        self.nCommDat = len(self.commDate)
        
        self.commUAge = np.unique(self.commAge)
        self.commAgeIndx = np.array([0, 1, 1, 2, 2, 2, 2, 3, 3, 1, 3, 3, 3, 4, 4, 4, 4, 4])
        print('self.commUAge', self.commUAge, 'commAgeIndx', self.commAgeIndx)

        self.uCommEthnic = np.unique(self.commEthnic)
        ## INDEXING TO MERGE ETHNIC DATA IN DIFFERENT FORMATS; EXCLUDING UNKNOWN
        self.commEthnicIndx = np.array([0, 2, 1, 2, 3, 99])
        print('comEthn', self.uCommEthnic, 'rrethnic', self.uEthnic)

    def updateRelRisk(self):
        self.rrUpdated = self.rr.copy()
#        self.rrInAnalysis = np.where(self.rr > 0, 'Y', 'N')
#        print('rrInAnalysis', self.rrInAnalysis, 'ndhb', self.nUDHB)
        contactYesMask = self.mohContact == 'Y'
        ## UPDATE DHB
        contactArr = np.zeros(self.nUDHB)
        mohDateMask = np.in1d(self.mohDate, self.uRRDates)
        for i in range(self.nUDHB):
            dhb_i = self.uDHB[i]
            mask_i = ((self.mohDHB == dhb_i) & contactYesMask & mohDateMask & 
                self.nonEssentialMask)
            contactArr[i] = np.sum(mask_i) + 1
        rrDHB = contactArr / np.min(contactArr)
        self.rrUpdated[self.rrNode == 'DHB'] = rrDHB

        ## UPDATE ETHNICITY FROM COMMUNITY CASE DATA

        ## UPDATE ETHNICITY
        commDateMask = np.in1d(self.commDate, self.uDates)
        discoverTypes = np.array(['Contact', 'Sought healthcare'])
        discoverMask = np.in1d(self.commContact, discoverTypes) 
        contactArr = np.zeros(self.nUEthnic)
        for i in range(self.nUEthnic):
            e_i = self.uEthnic[i]

            commEthArr = self.uCommEthnic[self.commEthnicIndx == i]
            mask_i = np.in1d(self.commEthnic, commEthArr) & commDateMask & discoverMask
            contactArr[i] = np.sum(mask_i) + 1
        rrEth = contactArr / np.min(contactArr)
        self.rrUpdated[self.rrNode == 'Ethnicity'] = rrEth
 
        ## UPDATE AGE
        contactArr = np.zeros(self.nUAge)
        for i in range(self.nUAge):
            age_i = self.uAge[i]
            commAgeArr = self.commUAge[self.commAgeIndx == i]
            mask_i = np.in1d(self.commAge, commAgeArr) & commDateMask & discoverMask

#            age_indx_i = np.where(self.uAge == age_i)[0]
#            mohAgeValuesMask = self.mohAgeIndx == age_indx_i
#            mohAgeValues = self.tmpUAge[mohAgeValuesMask]
#            mohagemask = np.in1d(self.mohAge, mohAgeValues)
            contactArr[i] = np.sum(mask_i) + 1                   
#            contactArr[i] = np.sum(self.mohPositive[mohagemask]) + 1
        rrAge = contactArr / np.min(contactArr)
        self.rrUpdated[self.rrNode == 'Age'] = rrAge

#        ## UPDATE CONTACT
#        commContactMask = self.commContact == 'Contact'
#        nonContactMask = self.commContact == 'Sought healthcare'

#        nYes = np.sum(commContactMask)
#        nNo = np.sum(~nonContactMask)
#        contactArr = np.zeros(self.nUContact)
#        contactArr[0] = nNo
#        contactArr[1] = nYes
#        rrContact = contactArr / np.min(contactArr)
#        self.rrUpdated[self.rrNode == 'Contact'] = rrContact

        for i in range(self.nRR):
            print('node', self.rrNode[i], 'gp', self.rrGroup[i], 'rr', self.rrUpdated[i])





    def writeRelRisks(self):
        """
        # Write relative risks to file
        """
        # create new structured array with columns of different types
        structured = np.empty((self.nRR,), dtype=[('Node', 'U32'),
                    ('Group', 'U64'), ('RelRisk', 'f2'),
                    ('InAnalysis', 'U32')])
        structured['Node'] = self.rrNode
        structured['Group'] = self.rrGroup
        structured['RelRisk'] = self.rrUpdated
        structured['InAnalysis'] = self.rrInAnalysis       
        fmts = ['%s','%s', '%.4f', '%s']
        np.savetxt(self.params.rrUpdateFname, structured, comments = '', 
            delimiter = ',', fmt = fmts,
            header='Node, Group, RelRisk, InAnalysis')


    def correctAgeData(self):
        badAgeMask = ~np.in1d(self.mohAge, self.tmpUAge)
        nBad = np.sum(badAgeMask)
        randAge = np.random.choice(self.tmpUAge, nBad)
        self.mohAge[badAgeMask] = randAge
        self.uMohAge = np.unique(self.mohAge)
        self.nUMohAge = len(self.uMohAge)
        print('umohage', self.uMohAge, len(self.uMohAge))
 
    def readEthnicity(self):
        ethnicDat = np.genfromtxt(self.params.ethnicPropFname,  delimiter=',', 
            names=True, dtype=['S64', 'f8', 'f8', 'f8', 'f8'])
        self.prpAsian = ethnicDat['Asian']
        self.nDatEthnic = len(self.prpAsian)
        self.prpPopEthnic = np.zeros((self.nDatEthnic, 4))
        self.prpPopEthnic[:,0] = self.prpAsian
        self.prpPopEthnic[:,1] = ethnicDat['Maori']
        self.prpPopEthnic[:,2] = ethnicDat['Other']
        self.prpPopEthnic[:,3] = ethnicDat['Pacific']
#        self.prpPopEthnic[:,4] = ethnicDat['Unknown']
        self.ethnicDHB = decodeBytes(ethnicDat['DHB'], self.nDatEthnic)
        print('ethDHB', self.ethnicDHB, self.prpAsian)

    def readIncidence(self):
        incidenceDat = np.genfromtxt(self.params.incidenceFname,  delimiter=',', 
            names=True, dtype=['S10', 'S32', 'S10', 'f8', 'i8', 'S10'])
        self.incidenceDate = incidenceDat['SurveyWeek']
        self.nIncidenceDat = len(self.incidenceDate)
        self.incidenceDate = decodeBytes(self.incidenceDate, self.nIncidenceDat)
        self.incidenceDate = formatDate(self.incidenceDate, '%Y-%m-%d')
        self.incidenceLoc = decodeBytes(incidenceDat['Location'], self.nIncidenceDat)
        self.incidenceAge = decodeBytes(incidenceDat['age_band'], self.nIncidenceDat)
        self.propIncidence = incidenceDat['prop_unique_incidents']
        self.incidenceSymGp = decodeBytes(incidenceDat['SymptomGrouping'], 
            self.nIncidenceDat)
        self.uIncidenceLoc = np.unique(self.incidenceLoc)
    
        print('Incidence Loc', self.uIncidenceLoc)

    def readPopData(self):
        popDat = np.genfromtxt(self.params.popDatFname,  delimiter=',', 
            names=True, dtype=['S32', 'S10', 'i8'])
        self.popN_DHBAge = popDat['pop2021']
        self.nPopDat = len(self.popN_DHBAge)
        self.popDHB = decodeBytes(popDat['DHB_name'], self.nPopDat)
        self.popAge = decodeBytes(popDat['age_Orion'], self.nPopDat)
        self.uPopAge = np.unique(self.popAge)
        self.popAgeIndx = np.array([0, 1, 1, 1, 2,2,2,2, 3,3,3,3,3, 4,4,4,4])


    def makeBasicArrays(self):
        self.dhbIncidenceIndx = np.array([4,0,1,2,3,0,1,3,1,1,4,2,1,2,2,1,1,1,3,0,2,1])
        ## SUBSET DHB WHERE RR > 0
        rrDHBMask = self.rrNode == 'DHB'
        rr1mask = (self.rrInAnalysis == 'Y') & rrDHBMask
        self.analyseDHB = self.rrGroup[rr1mask]
 
        ## SUBSET ETHNIC WHERE RR > 0
        rrEthnicMask = self.rrNode == 'Ethnicity'
        rr1mask = (self.rrInAnalysis == 'Y') & rrEthnicMask
        self.analyseEthnic = self.rrGroup[rr1mask]
        ## COMBINE NODES AND GROUPS
        self.nFullDat = (self.nUDHB * self.nUEthnic * self.nUAge * 
            self.nUVaccine * self.nUContact)
        self.DHB = np.repeat(self.uDHB, (self.nUEthnic * self.nUAge * 
            self.nUVaccine * self.nUContact))
        ethnicInDHB = np.repeat(self.uEthnic, (self.nUAge * 
            self.nUVaccine * self.nUContact))
        self.Ethnic = np.tile(ethnicInDHB, self.nUDHB)
        ageInEthnicDHB = np.repeat(self.uAge, (self.nUVaccine * self.nUContact))
        self.Age = np.tile(ageInEthnicDHB, (self.nUDHB * self.nUEthnic))
        vaccInAge = np.repeat(self.uVaccine, self.nUContact)
        self.Vaccine = np.tile(vaccInAge, (self.nUDHB * self.nUEthnic * self.nUAge))
        
        self.Contact = np.tile(self.uContact, (self.nUDHB * self.nUEthnic * 
            self.nUAge * self.nUVaccine))



    def dataByDates(self):
        self.DHB = np.tile(self.DHB, self.nDays)
        self.Ethnic = np.tile(self.Ethnic, self.nDays)
        self.Age = np.tile(self.Age, self.nDays)
        self.Vaccine = np.tile(self.Vaccine, self.nDays)
        self.Contact = np.tile(self.Contact, self.nDays)
        self.Dates = np.repeat(self.Dates, self.nFullDat)
        self.nFullDat = len(self.Age)

    def populateRR(self):
        self.RR_DHB = np.zeros(self.nFullDat)
        self.RR_Ethnicity = np.zeros(self.nFullDat)
        self.RR_Age = np.zeros(self.nFullDat)
        self.RR_Vaccine = np.zeros(self.nFullDat)
        self.RR_Contact = np.zeros(self.nFullDat)
        for i in range(self.nRR):
            if self.rrInAnalysis[i] == 'N':
                continue
            node_i = self.rrNode[i]
            if node_i == 'DHB':
                dhb_i = self.rrGroup[i]
                mask = self.DHB == dhb_i
                self.RR_DHB[mask] = self.rrUpdated[i]
            elif node_i == 'Ethnicity':
                eth_i = self.rrGroup[i]
                mask = self.Ethnic == eth_i
                self.RR_Ethnicity[mask] = self.rrUpdated[i]
            elif node_i == 'Age':
                age_i = self.rrGroup[i]
                mask = self.Age == age_i
                self.RR_Age[mask] = self.rrUpdated[i]
            elif node_i == 'Vaccine':
                vac_i = self.rrGroup[i]
                mask = self.Vaccine == vac_i
                self.RR_Vaccine[mask] = self.rr[i]
            elif node_i == 'Contact':
                contact_i = self.rrGroup[i]
                mask = self.Contact == contact_i
                self.RR_Contact[mask] = self.rrUpdated[i]


    def redistributeUnknownEthnic(self):
        prob = np.repeat(1.0 / self.nUEthnic, self.nUEthnic)

        dhb = np.repeat(self.uDHB, (self.nUEthnic * self.nUAge * self.nUContact))
        ethnicInDHB = np.repeat(self.uEthnic, (self.nUAge * self.nUContact))
        ethnic = np.tile(ethnicInDHB, self.nUDHB)
        ageInEthnicDHB = np.repeat(self.uAge, self.nUContact)
        age = np.tile(ageInEthnicDHB, (self.nUDHB * self.nUEthnic))
        contact = np.tile(self.uContact, (self.nUDHB * self.nUEthnic * self.nUAge))

        self.dhbUnknown = np.tile(dhb, self.nDays)
        self.ethnicUnknown = np.tile(ethnic, self.nDays)
        self.ageUnknown = np.tile(age, self.nDays)
        self.contactUnknown = np.tile(contact, self.nDays)
        ndat = (self.nUDHB * self.nUEthnic * self.nUAge * self.nUContact)
        self.datesUnknown = np.repeat(self.uDates, ndat)
        ndat = len(self.ageUnknown)

        self.unknownTests = np.zeros((ndat, 4))

        for i in range(ndat):
            dhb_i = self.dhbUnknown[i]
            ethnic_i = self.ethnicUnknown[i]
            age_i = self.ageUnknown[i]
            date_i = self.datesUnknown[i]
            contact_i = self.contactUnknown[i]

            mohdatemask = self.mohDate == date_i
            mohdhbmask = self.mohDHB == dhb_i
            mohunknownmask = self.mohEthnicity == 'Unknown'

            age_indx_i = np.where(self.uAge == age_i)[0]
            mohAgeValuesMask = self.mohAgeIndx == age_indx_i
            mohAgeValues = self.uMohAge[mohAgeValuesMask]
            mohagemask = np.in1d(self.mohAge, mohAgeValues)
            mohcontactmask = self.mohContact == contact_i
            unknownMask = (mohdatemask & mohdhbmask & mohunknownmask & mohagemask & 
                mohcontactmask)
            
            testUnknown = np.sum(self.mohNTests[unknownMask])

            self.unknownTests[i] = pidgeon_hole(testUnknown, self.nUEthnic)

    def getPSeek(self):
        self.Population = np.zeros(self.nFullDat)
        self.nTests = np.zeros(self.nFullDat)
        self.nHiSymp = np.zeros(self.nFullDat)
        self.nLowSymp = np.zeros(self.nFullDat)
        self.meanTestPool = np.zeros(self.nFullDat)
        self.sdTestPool = np.zeros(self.nFullDat)

        self.meanPSymp = np.zeros(self.nFullDat)
        self.sdPSymp = np.zeros(self.nFullDat)

        self.dhbTestPool = np.zeros((self.nUDHB, 2))
        self.dhbTests = np.zeros((self.nUDHB, 2))


        self.uIncidenceDates = np.unique(self.incidenceDate)
        self.nIncidDates = len(self.uIncidenceDates)
        self.dayDiff = np.zeros(self.nIncidDates, dtype = int)
        ## LOOP THRU DATA AND GET PSEEK
        for i in range(self.nFullDat):
            ## GET DATA IF RR > 0
            RR_prod = (self.RR_DHB[i] * self.RR_Ethnicity[i] * self.RR_Age[i] * 
                self.RR_Vaccine[i] * self.RR_Contact[i])
            if RR_prod == 0:
                continue
            ## GET CLOSEST DATE IN INCIDENT DATA
            self.getIncidenceDate(i)
            ## GET CORRESPONDING PROP INCIDENCE FOR DHB AND AGE 
            self.getPropIncidence(i)
            ## GET TEST DATA FROM MOH DATA
            self.getTestData(i)
            ## GET POPULATION FOR AGE AND DHB
            self.getPopulation(i)            
        print('udate Incidence', self.uIncidenceDates)

    def getIncidenceDate(self, i):
        date_i = self.Dates[i]
        for j in range(self.nIncidDates):
            self.dayDiff[j] = np.abs((date_i - self.uIncidenceDates[j]).days)
        self.maskDiff = self.dayDiff == np.min(self.dayDiff)
        self.incidDate_i = self.uIncidenceDates[self.maskDiff]

    def getPropIncidence(self, i):
        self.DHB_i = self.DHB[i]
        
        self.ethnic_i = self.Ethnic[i]
        self.age_i = self.Age[i]
        self.date_i = self.Dates[i]
        self.vacc_i = self.Vaccine[i]
        self.contact_i = self.Contact[i]
        
        incidDateMask = self.incidenceDate == self.incidDate_i
        uDHBMask = self.uDHB == self.DHB_i
        incidIndx_i = self.dhbIncidenceIndx[uDHBMask]
        incidLoc = self.uIncidenceLoc[incidIndx_i]  
        incidLocMask = self.incidenceLoc == incidLoc
        lowMask = (self.incidenceSymGp == 'CLI2+') & incidDateMask & incidLocMask
        hiMask = (self.incidenceSymGp == 'CLI1+')  & incidDateMask & incidLocMask
        ageMask = self.incidenceAge == self.age_i
        agelowMask = ageMask & lowMask
        agehiMask = ageMask & hiMask
        self.lowIncidence_i = self.propIncidence[agelowMask]
        self.hiIncidence_i = self.propIncidence[agehiMask]
        
    def getTestData(self, i):
        mohdatemask = self.mohDate == self.date_i
        mohdhbmask = self.mohDHB == self.DHB_i
        mohethnicmask = self.mohEthnicity == self.ethnic_i

        self.age_indx_i = np.where(self.uAge == self.age_i)[0]
        mohAgeValuesMask = self.mohAgeIndx == self.age_indx_i
        mohAgeValues = self.uMohAge[mohAgeValuesMask]
        mohagemask = np.in1d(self.mohAge, mohAgeValues)

        mohcontactmask = self.mohContact == self.contact_i
        fullmask = (mohdatemask & mohdhbmask & mohethnicmask & mohagemask & 
            mohcontactmask & self.nonEssentialMask)

        self.totalTests_i = np.sum(self.mohNTests[fullmask])

        ## REDISTRIBUTE THE UNKNOWN TESTS
        udhbmask = self.dhbUnknown == self.DHB_i
        uethnicmask = self.ethnicUnknown == self.ethnic_i
        uagemask = self.ageUnknown == self.age_i
        ucontactmask = self.contactUnknown == self.contact_i  
        udatemask = self.datesUnknown == self.date_i

        unknownMask = (udatemask & udhbmask & uethnicmask & uagemask & 
                ucontactmask)
        ethnicIndx = np.where(self.uEthnic == self.ethnic_i)[0]
        self.totalTests_i += self.unknownTests[unknownMask, ethnicIndx]


    def getPopulation(self, i):
        popDHBMask = self.popDHB == self.DHB_i
        pop_i = self.popN_DHBAge[popDHBMask]
        popAgeMask = self.popAgeIndx == self.age_indx_i
        self.pop_i = np.sum(pop_i[popAgeMask])
        ethnicIndx = np.where(self.uEthnic == self.ethnic_i)[0]
        ethnicDHBMask = self.ethnicDHB == self.DHB_i
        self.pop_i = self.pop_i * self.prpPopEthnic[ethnicDHBMask, ethnicIndx]
        ## GET TEST DATA FROM MOH
        self.getTestData(i)
        ## IF <5; GET TESTS = TOTAL TEST BECAUSE NO VACC
        if self.age_indx_i == 0:
            if self.vacc_i == 'Vaccinated':
                self.pop_i = 0
                self.test_i = 0
            else:
                self.test_i = self.totalTests_i
                self.pop_i = self.pop_i
        ## IF >5; SPLIT POP AND TESTS BY VACC GROUPS
        else:
            if self.vacc_i == 'Vaccinated':
                ## SPLIT UP POP BY PROP VACCINATED BY AGE GROUP (EQUAL ACROSS ETHNIC)
                self.pop_i = self.pop_i * self.params.vaccineAgePrp[self.age_indx_i]
                ## P(SYM | VACC AND INF)
                self.test_i = np.round((self.totalTests_i * self.params.vaccinePrpTest), 0)
                ## P(SYMPTOMATIC | VACCINATED)
                self.meanPSymp[i] = self.params.PSymptom[1,0]
                self.sdPSymp[i] = self.params.PSymptom[1,1]
            else:
                self.pop_i = self.pop_i * (1.0-self.params.vaccineAgePrp[self.age_indx_i])
                ## P(SYM | VACC AND INF)
                self.test_i = np.round((self.totalTests_i * 
                    (1.0 - self.params.vaccinePrpTest)), 0)
                ## P(SYMPTOMATIC | UN-VACCINATED)
                self.meanPSymp[i] = self.params.PSymptom[0,0]
                self.sdPSymp[i] = self.params.PSymptom[0,1]

        if self.contact_i == 'N':
            self.Population[i] = self.pop_i
            ## DIVIDE BY 7 DAYS BECAUSE INCIDENCE IS FOR 1 WEEK
            self.nLowSymp[i] = self.lowIncidence_i * self.pop_i / 7
            self.nHiSymp[i] = self.hiIncidence_i * self.pop_i / 7
            self.meanTestPool[i] = self.nLowSymp[i]
            self.sdTestPool[i] = 1.0
            self.meanTestPool[i] = (self.nHiSymp[i] + self.nLowSymp[i]) / 2.0
            self.sdTestPool[i] = np.abs(self.nHiSymp[i] - self.nLowSymp[i]) / 6.0
            self.nTests[i] = self.test_i
            ## CORRECT FOR LOW TEST POOL
            if self.nTests[i] >= self.meanTestPool[i]:
                self.meanTestPool[i] = self.nTests[i]
                self.sdTestPool[i] = 0.0
            if (self.meanTestPool[i] - self.nTests[i]) < 3.0:
                self.sdTestPool[i] = 0.0
            if ((self.meanTestPool[i] - (self.sdTestPool[i] * 5)) < 0.0):
                self.sdTestPool[i] = self.sdTestPool[i] * 0.5 
            
        else:
            self.meanTestPool[i] = self.params.contactTestPool[0]
            self.sdTestPool[i] = self.params.contactTestPool[1]
            self.nTests[i] = self.params.contactNTests
        ## CORRECT FOR CHILDREN AND 
        childVaccMask = (self.age_indx_i== 0) & (self.vacc_i == 'Vaccinated')
        if childVaccMask:
            self.meanTestPool[i] = 0
            self.sdTestPool[i] = 0
            self.nTests[i] = 0

        dhbMask = self.uDHB == self.DHB_i
        ## INCREASE TESTING FOR NON-CONTACTS
        if ~childVaccMask & (self.contact_i == 'N') & self.params.increaseTest:
            ## BEFORE ADDING TESTS, ADD TO DHB TESTING ARRAY FOR RESULTS
            self.dhbTestPool[dhbMask, 0] += self.meanTestPool[i]
            self.dhbTests[dhbMask, 0] += self.nTests[i]
            ## FIND RR FOR DHB AND INCREASE RATE
            increaseIndx = np.digitize(self.RR_DHB[i], self.params.rrBins)
            increasePrp = self.params.rrIncreaseTest[increaseIndx - 1]
            if self.nTests[i] == 0:
                self.nTests[i] = np.random.binomial(1, increasePrp)
            else:            
                self.nTests[i] += np.random.binomial(self.nTests[i], increasePrp)
            ## CORRECT FOR LOW TEST POOL
            if self.nTests[i] >= self.meanTestPool[i]:
                self.meanTestPool[i] = self.nTests[i]
                self.sdTestPool[i] = 0.0
            if (self.meanTestPool[i] - self.nTests[i]) < 3.0:
                self.sdTestPool[i] = 0.0
            if ((self.meanTestPool[i] - (self.sdTestPool[i] * 5)) < 0.0):
                self.sdTestPool[i] = self.sdTestPool[i] * 0.5 
            ## AFTER ADDING TESTS, ADD TO DHB TESTING ARRAY FOR RESULTS
            self.dhbTestPool[dhbMask, 1] += self.meanTestPool[i]
            self.dhbTests[dhbMask, 1] += self.nTests[i]
#        ## INCREASE TEST EFFORT FOR CONTACTS
#        if ~childVaccMask & (self.contact_i == 'Y') & self.params.increaseTest:
#            self.nTests[i] = self.params.increaseContactTests

        if self.nTests[i] > self.meanTestPool[i]:
            print('more tests than people:','date', self.date_i, 'dhb', self.DHB_i, 'eth', self.ethnic_i,
                'age', self.age_i, 'age_indx', self.age_indx_i,'vacc', self.vacc_i, 
                'test', self.nTests[i], 
                self.meanTestPool[i])


    def writeTestsToFile(self):
        print('self.dhbTestPool', self.dhbTestPool, 'self.dhbTests', self.dhbTests)

        dhbMask = self.rrNode == 'DHB'
        dhbInAnalysis = self.rrInAnalysis[dhbMask]
        inMask = dhbInAnalysis == 'Y'
        testRateArr = np.zeros((self.nUDHB, 2))
        testRateArr[inMask, 0] = (self.dhbTests[inMask, 0] / 
            self.dhbTestPool[inMask, 0])
        testRateArr[inMask, 1] = (self.dhbTests[inMask, 1] / 
            self.dhbTestPool[inMask, 1])


        # create new structured array with columns of different types
        structured = np.empty((self.nUDHB,), dtype=[('DHB', 'U32'), ('RR', 'f2'),
                    ('TestRateObs', 'f4'), ('TestRateIncreased', 'f4'),
                    ('nTestsObs', 'f4'), ('nTestsIncreased', 'f4')]) 
        # copy data over
        structured['DHB'] = self.uDHB
        structured['RR'] = self.rrUpdated[dhbMask]


        structured['TestRateObs'] = np.round(testRateArr[:, 0], 3)
        structured['TestRateIncreased'] = np.round(testRateArr[:, 1], 3)
        structured['nTestsObs'] = np.round(self.dhbTests[:, 0] / self.nDays, 2)
        structured['nTestsIncreased'] = np.round(self.dhbTests[:, 1] / self.nDays, 2) 
        fmts = ['%s', '%.2f', '%.4f', '%.4f', '%.4f', '%.4f'] 
        np.savetxt(self.params.testRateFname, structured, comments = '', 
            delimiter = ',', fmt = fmts,
            header='DHB, RR, ObservedTestRate, IncreasedTestRate, nTestsObs, nTestsIncreased')


    def writeToFile(self):
        """
        # Write result table to file
        """
        # create new structured array with columns of different types
        structured = np.empty((self.nFullDat,), dtype=[('Date', 'U32'),
                    ('DHB', 'U32'), ('RR_DHB', 'f2'),
                    ('ETHNICITY', 'U12'), ('RR_ETHNICITY', 'f2'), ('AGE', 'U12'), 
                    ('RR_AGE', 'f2'), ('VACCINATED', 'U12'), 
                    ('RR_VACCINATED', 'f2'),
                    ('CONTACT', 'U12'), ('RR_CONTACT', 'f2'), 
                    ('MEAN_TESTPOOL', 'f8'), ('SD_TESTPOOL', 'f8'), 
                    ('TOTAL_TESTS', 'i8'),
                    ('P_Symptomatic_mean', 'f4'),
                    ('P_Given_mean', 'f4'), ('P_Test_mean', 'f4'),
                    ('P_Symptomatic_sd', 'f4'), 
                    ('P_Given_sd', 'f4'), ('P_Test_sd', 'f4')])
        # copy data over
        structured['Date'] = self.Dates
        structured['DHB'] = self.DHB
        structured['RR_DHB'] = self.RR_DHB
        structured['ETHNICITY'] = self.Ethnic
        structured['RR_ETHNICITY'] = self.RR_Ethnicity
        structured['AGE'] = self.Age
        structured['RR_AGE'] = self.RR_Age
        structured['VACCINATED'] = self.Vaccine
        structured['RR_VACCINATED'] = self.RR_Vaccine
        structured['CONTACT'] = self.Contact
        structured['RR_CONTACT'] = self.RR_Contact
        structured['MEAN_TESTPOOL'] = self.meanTestPool
        structured['SD_TESTPOOL'] = self.sdTestPool
        structured['TOTAL_TESTS'] = self.nTests
        structured['P_Symptomatic_mean'] = self.meanPSymp
        structured['P_Given_mean'] = np.repeat(self.params.PGive[0], 
            self.nFullDat)
        structured['P_Test_mean'] = np.repeat(self.params.PTest[0], 
            self.nFullDat)
        structured['P_Symptomatic_sd'] = self.sdPSymp
        structured['P_Given_sd'] = np.repeat(self.params.PGive[1], 
            self.nFullDat)
        structured['P_Test_sd'] = np.repeat(self.params.PTest[1], 
            self.nFullDat)
        fmts = ['%s','%s', '%.4f', '%s', '%.4f', '%s', '%.4f', '%s', '%.4f', '%s', '%.4f',
            '%.4f', '%.4f', '%i', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f']
        np.savetxt(self.params.poaOutFname, structured, comments = '', 
            delimiter = ',', fmt = fmts,
            header='Date, DHB, RR_DHB, ETHNICITY, RR_ETHNICITY, AGE, RR_AGE, VACCINATED, RR_VACCINATED, CONTACT, RR_CONTACT, MEAN_TESTPOOL, SD_TESTPOOL, TOTAL_TESTS, MEAN_PSYMPTOMATIC, MEAN_PGIVEN, MEAN_PTEST, SD_PSYMPTOMATIC, SD_PGIVEN, SD_PTEST')

#            header='Date, DHB, RR_DHB, ETHNICITY, RR_ETHNICITY, AGE, RR_AGE, VACCINATED, RR_VACCINATED, CONTACT, RR_CONTACT, MEAN_PSYMPTOMATIC, MEAN_PSEEK, MEAN_PGIVEN, MEAN_PTEST, SD_PSYMPTOMATIC, SD_PSEEK, SD_PGIVEN, SD_PTEST')

######################
# Main function
def main():
    # set paths to scripts and data


    params = Params()

    manipdata = ManipData(params)



if __name__ == '__main__':
    main()


