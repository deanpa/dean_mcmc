#!/usr/bin/env python

import numpy as np
import pylab as P
from scipy import stats
from scipy.stats.mstats import mquantiles



def getBetaFX(mg0, g0Sd):
    a = mg0 * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    b = (1.0 - mg0) * ((mg0 * (1.0 - mg0)) / g0Sd**2.0 - 1.0)
    return (a, b)

class CovidPrev(object):
    def __init__(self):
        """
        Object to generate Bernouilli data
        """

        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 3000           # number of estimates to save for each parameter
        self.thinrate = 15       # thin rate
        self.burnin = 4000          # burn in number of iterations
                                    # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + 
            self.burnin), self.thinrate)

        self.iter = (self.ngibbs * self.thinrate) + self.burnin
        ####################

        ##  SET PARAMETERS ################
        self.n = 26000
        self.N = 5.01e6
        self.meanSe = .9
        self.sdSe = 0.1
        self.betaPrior = ([0.001, 0.09])  # prev 1/100000, 1/10000, 1/1000
#        self.prev = np.random.uniform(self.betaPrior[0], self.betaPrior[1])
        self.pSearch = 0.25 
        self.cases = np.array([1,2,3,5,10,15,20,25,30]) #np.arange(6)
        self.nCases = len(self.cases)
        self.zeroPrior = np.array([0.9, 0.1]) #mean and sd
        ###################################


        ## RUN FUNCTIONS ##################
        self.mcmcFX()
        self.plotFX()
        self.getMeanCI()
        self.plotPrev()
        self.getZeroDetect()
        ###################################

    #######            MAIN MCMC FUNCTION
    ########
    def mcmcFX(self):
        """
        ## RUN FUNCTIONS TO UPDATE PARAMETERS WITH MCMC
        """
        ## EMPTY ARRAYS TO POPULATE WITH MCMC
        self.pgibbs = np.zeros((self.ngibbs, self.nCases))
        
        ## LOOP THROUGH ITERATIONS OF MCMC
        for h in range(self.nCases):
            self.ncase_h = self.cases[h]
            self.getSe()
            if self.cases[h] == 0:
                self.prev = np.random.uniform(0.001, 0.0001)
            else:
                lowPrev = self.cases[h] / (self.n - 10000)
                hiPrev = self.cases[h] / (self.n + 10000)
                self.prev = np.random.uniform(lowPrev, hiPrev)
            theta = self.prev * self.se_g
            prevlogpmf = stats.binom.logpmf(self.ncase_h, self.n, theta)
            self.pnow = prevlogpmf

            cc = 0
            for g in range(self.iter):
                self.getSe()
                self.updatePrev(h)

                ## POPULATE STORAGE ARRAYS WITH PARAMETER ESTIMATES
                if (g in self.keepseq):
                    self.pgibbs[cc, h] = self.prev
                    cc += 1

    def getSe(self):
        a = self.meanSe * ((self.meanSe * (1.0 - self.meanSe)) / self.sdSe**2.0 - 1.0)
        b = (1.0 - self.meanSe) * ((self.meanSe * 
            (1.0 - self.meanSe)) / self.sdSe**2.0 - 1.0)
        self.se_g = np.random.beta(a, b)

    def updatePrev(self, h):
        if self.cases[h] == 0:
            ps = np.exp(np.random.normal(np.log(self.prev), self.pSearch/4.0))
        else:
            ps = np.exp(np.random.normal(np.log(self.prev), self.pSearch))
        if self.cases[h] == 0:
            while ps > 0.0005:
                ps = np.exp(np.random.normal(np.log(self.prev), self.pSearch/2.0))
        theta_s = ps * self.se_g
        prevlogpmf_s = stats.binom.logpmf(self.ncase_h, self.n, theta_s)

#        pPriors_s = stats.beta.logpdf(ps, self.betaPrior[0], self.betaPrior[1]), 
#                self.rPriors[1])
        pnew = prevlogpmf_s         #+ rPriors_s

        pdiff = pnew - self.pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -5.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0)
        if (rValue > zValue):
            self.prev = ps
            self.prevlogpmf = prevlogpmf_s
            self.pnow = pnew

    def plotFX(self):
#        popIndx = np.array([25, 146, 202, 290, 316, 340])
        nplots = self.nCases
        cc = 0
        nfigures = np.int(np.ceil(nplots/6.0))
        ng = np.arange(self.ngibbs, dtype=int)
        #print nfigures, self.ncov
        for i in range(nfigures):
            P.figure(i, figsize=(11,7))
            lastFigure = i == (nfigures - 1)
            #print lastFigure
            for j in range(6):
                P.subplot(2,3,j+1)
                #print cc
                if cc < nplots:
                    P.plot(ng, self.pgibbs[:, cc])
                    P.title(str(self.cases[cc]) + 'cases')
                cc = cc + 1
            P.show(block=lastFigure)


    def getMeanCI(self):
        self.prevResults = np.zeros((self.nCases, 3))
        self.prevResults[:, 0] = np.mean(self.pgibbs, axis = 0) * 100

        quants = mquantiles(self.pgibbs, prob=[0.025, 0.975], axis = 0) * 100
        print('quants', quants)
        self.prevResults[:, 1:] = np.transpose(quants)
        print('prevResults', self.prevResults)

    def plotPrev(self):
        P.figure(figsize=(10, 8))
        # save plots to project directory
        ax = P.gca()
        lowCI = self.prevResults[:, 0] - self.prevResults[:, 1]
        hiCI = self.prevResults[:, 2] - self.prevResults[:, 0]
        lns1 = ax.errorbar(self.cases, self.prevResults[:, 0], 
            yerr = [lowCI, hiCI], ecolor = 'black',
            marker = 'o', markerfacecolor = 'black', ms = 3, ls = 'none', mew = 3,
            markeredgecolor = 'black', capsize = 8)
#        lns4 = ax.plot(self.cases, self.prevResults[:, 0], 'ko', 
#            markerfacecolor = 'none', ms = 10, mew = 1.5)
        lns5 = ax.plot(self.cases, self.prevResults[:, 0], linewidth = 3, color='k') 
#        lns4 = tuple(lns4)
#        P.legend([lns1, lns4, lns5], 
#            ["Cost mean and 95% CI", 
#            'Total origin-risk shipments',
#            'Surveyed origin-risk shipments'], 
#            loc='upper right')        
        maxy = np.max(self.prevResults[:,2]) + 0.005
        miny = np.min(self.prevResults[:,1]) - 0.002 
        minx = np.min(self.cases) - 1 
        maxx = np.max(self.cases) + 1
        ax.set_ylim([miny, maxy])
        ax.set_xlim([minx, maxx])
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
#        self.getNTicks()
#        ax.set_xticks(self.xTicksYr, self.xlabelsYr)
        ax.set_xlabel('Positive cases', fontsize=17)
        ax.set_ylabel('$\%$ Prevalence', fontsize=17)
        P.savefig("prevalenceCases.png", format='png', dpi = 500)
        P.show()

    def getZeroDetect(self):
        self.oneCaseMean = self.prevResults[0,0]
        print('mean one case ($\%$)', self.oneCaseMean, 
            'nInfected', self.oneCaseMean/100.0 * self.N)
        (a, b) = getBetaFX(self.meanSe, self.sdSe)
        self.seArray = np.random.beta(a, b, 5000)
        SSe = (1.0 - (1.0 - self.seArray * self.n / self.N)**
                (self.oneCaseMean / 100.0 * self.N))


        (c,d) = getBetaFX(self.zeroPrior[0], self.zeroPrior[1])
        prior = np.random.beta(c, d, 5000)
        self.posterior = prior / (1.0 - (SSe * (1.0 - prior)))
        meanzerocase = np.mean(self.posterior)
        quantzerocase = mquantiles(self.posterior, prob=[0.025, 0.975])
        print('mean zero =', meanzerocase, 'CI zero =', quantzerocase)



        


########            Main function
#######
def main():
    covidprev = CovidPrev()

if __name__ == '__main__':
    main()

