#!/usr/bin/env python

from scipy import stats
from scipy.special import gamma
import numpy as np
import pylab as P
import prettytable
from scipy.stats.mstats import mquantiles

rng = np.random.default_rng()

##### global functions
##
def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))
##### end global functions



#########################################
#
#   GENERATE DATA
#
#########################################
class BernData(object):
    def __init__(self):
        """
        Object to generate Bernouilli data
        """
        self.nNights = 5
        self.nTraps = 100

        # betas used to simulate data
        self.B = np.array([-2.75, 1.0])         #-1.2, 1.3])         #-2.75, 1.0])
 #       self.Sig = 0.5
        self.npara = len(self.B)
        self.gam = 0.09     #0.05     #0.075

        self.genData()
        self.plotJitter()

    def genData(self):
        self.dist = {}
        self.habEffGen = {}
        self.lthetaGen = {}
        self.thetaGen = {}
        self.Y = {}        
        for i in range(self.nTraps):
            ## NUMBER OF HABITAT PATCHES
            nHab = rng.integers(0,15)
            self.dist[i] = rng.uniform(0, .360, nHab)
            hab_i = np.sum(np.exp(-(self.dist[i]**2) / 2.0 / self.gam**2))
            self.habEffGen[i] = np.repeat(hab_i, self.nNights)
            self.lthetaGen[i] = self.B[0] + self.B[1] * self.habEffGen[i]
            self.thetaGen[i] = inv_logit(self.lthetaGen[i])
            self.Y[i] = rng.binomial(1,self.thetaGen[i])
#            print('i', i, 'nHab', nHab, 'thetaGen', self.thetaGen[i],
#                'Y', self.Y[i])

    def plotJitter(self):
        yValues = list(self.Y.values())
        y = np.concatenate(yValues)
        jitterY = rng.uniform(-.25, 0.251, len(y)) + y
        habEff = list(self.habEffGen.values())
        hab = np.concatenate(habEff)
        th = list(self.thetaGen.values())
        th = np.concatenate(th)
        P.figure(figsize=(11, 9))
        P.subplot(1,2,1)
        P.plot(hab, jitterY, 'ko')
        P.subplot(1,2,2)
        P.plot(th, jitterY, 'ko')
        P.show()
#        print('yValues', yValues, 'y', y)




############################################
#
#   END GENERATING DATA
#
############################################



############################################
#
#   ANALYSE DATA
#
############################################


class Params(object):
    def __init__(self):
        """
        #define priors and intial parameter
        """
        self.b = np.array([-.05, .05]) #initial b estimate
        #defining priors
        self.meanB = np.array([0.0, 0.0])
        self.varB = np.sqrt(100)
        self.npara = len(self.b)
        self.betaJump = .07
        ## GAMMA PARAMETERS
        self.gam = 0.05
        self.meanGam = 0.05
        self.varGam = np.sqrt(4)
        self.gamJump = 0.05        

        #defining mcmc run parameters
        self.ngibbs = 1000  #number of runs
        self.thin = 20    # thin rate
        self.burnin = 500  # burn in number of iterations
        self.totalIterations = (self.ngibbs * self.thin) + self.burnin
        self.keep = np.arange( start = self.burnin, stop = self.totalIterations, step = self.thin)
        

#defining MCMC sampler
class MCMC(object):
    """
    """
    def __init__(self, berndata, params): 

        self.b = params.b.copy()
        self.gam = params.gam
        #### Data from generated data
        self.dist = berndata.dist
        self.nNights = berndata.nNights
        self.y = berndata.Y
        self.nTraps = berndata.nTraps

        self.getInitialTheta()


        # storage array for parameter estimates
        self.bgibbs = np.empty((len(params.keep), params.npara))
        self.ggibbs = np.zeros(len(params.keep))
        print('betas from simulation', berndata.B)
        print('gamma from simulation', berndata.gam)

        #######################################
        #
        #   Run updater

        self.gibbs(params)
        
        #
        #######################################

    def getInitialTheta(self):
        self.habEff = {}
        self.ltheta = {}
        self.theta = {}
        self.lbinom = 0.0
        for i in range(self.nTraps):
            hab_i = np.sum(np.exp(-(self.dist[i]**2) / 2.0 / self.gam**2))
            self.habEff[i] = np.repeat(hab_i, self.nNights)
            self.ltheta[i] = self.b[0] + self.b[1] * self.habEff[i]
            self.theta[i] = inv_logit(self.ltheta[i])
            self.lbinom += np.sum(stats.binom.logpmf(self.y[i], 1, self.theta[i]))

    def updateTheta(self, b, gam):
        self.habEff_s = {}
        self.ltheta_s = {}
        self.theta_s = {}
        self.lbinom_s = 0.0
        for i in range(self.nTraps):
            hab_i = np.sum(np.exp(-(self.dist[i]**2) / 2.0 / gam**2))
            self.habEff_s[i] = np.repeat(hab_i, self.nNights)
            self.ltheta_s[i] = b[0] + b[1] * self.habEff_s[i]
            self.theta_s[i] = inv_logit(self.ltheta_s[i])
            self.lbinom_s += np.sum(stats.binom.logpmf(self.y[i], 1, self.theta_s[i]))


    
    def bupdate(self, params):
        """
        update beta parameters one at a time
        """
        for i in range(params.npara):
            bprop = self.b.copy()
            bprop[i] = np.random.normal(self.b[i], params.betaJump) #proposed values
            self.updateTheta(bprop, self.gam)
            lprior = stats.norm.logpdf(self.b[i] , params.meanB[i], params.varB)
            pnow = self.lbinom + lprior
            lprior_s = stats.norm.logpdf(bprop[i] , params.meanB[i], params.varB)
            pnew = self.lbinom_s + lprior_s
#            print('i', i, 'pnew', pnew, 'pnow', pnow, lprior)
            r = np.exp( pnew -  pnow ) #acceptance criterion
            #print(r)
            z = np.random.uniform( 1, 0, size = None)
            if z < r: #if accept with probability r update b & theta
                self.b[i] = bprop[i]
                self.habEff = self.habEff_s
                self.ltheta = self.ltheta_s
                self.theta = self.theta_s
                self.lbinom = self.lbinom_s

    
    def gupdate(self, params):
        """
        update beta parameters one at a time
        """
        gam_s = np.exp(np.random.normal(np.log(self.gam), params.gamJump)) #proposed values)
        self.updateTheta(self.b, gam_s)
        lprior = stats.norm.logpdf(np.log(self.gam) , params.meanGam, params.varGam)
        pnow = self.lbinom + lprior
        lprior_s = stats.norm.logpdf(np.log(self.gam) , params.meanGam, params.varGam)
        pnew = self.lbinom_s + lprior_s
#        print('i', i, 'pnew', pnew, 'pnow', pnow, lprior)
        r = np.exp( pnew -  pnow ) #acceptance criterion
        #print(r)
        z = np.random.uniform( 1, 0, size = None)
        if z < r: #if accept with probability r update b & theta
            self.gam = gam_s
            self.habEff = self.habEff_s
            self.ltheta = self.ltheta_s
            self.theta = self.theta_s
            self.lbinom = self.lbinom_s



    def gibbs(self, params):
        """
        run metropolis updater"
        """
        g = 0
        for gg in range(params.totalIterations):
            self.bupdate(params)
            self.gupdate(params)
            #
            if gg in params.keep:
                self.bgibbs[g] = self.b
                self.ggibbs[g] = self.gam
                g += 1
#        print('bgibbs', self.bgibbs)            #


class Results(object):
    """
    Process results
    """
    def __init__(self, berndata, params, mcmc): 
    ################
    ################ Functions to make table

        self.makeTableFX(berndata, params, mcmc)
        self.plotFX(params, mcmc)
#        self.plotUpdate(params, mcmc)
###        self.plotTrace(params, mcmc)

    def quantileFX(self, a):
        """
        function to calculate quantiles
        """
        return stats.mstats.mquantiles(a, prob=[0.025, 0.5, 0.975])

    def makeTableFX(self, berndata, params, mcmc):
        """
        Function to print table of results
        """
        self.setPara = [berndata.B[0], berndata.B[1], berndata.gam]
        self.names = np.array(['Beta0', 'Beta1', 'Gamma'])
        self.results = np.hstack([mcmc.bgibbs,
                                np.expand_dims(mcmc.ggibbs, 1)])

        ## PRINT CORRELATION
        corrPara = np.corrcoef(self.results, rowvar=False)
        print('Parameter correlation', corrPara)
        self.ncol = self.results.shape[1]
        resultTable = np.zeros(shape = (4, self.ncol))

        resultTable[0] = np.round(np.mean(self.results[:, :self.ncol], axis = 0), 3)
        resultTable[1:3] = np.round(mquantiles(self.results[:, :self.ncol], 
                prob=[0.025, 0.975], axis = 0), 3)
        resultTable[-1] = self.setPara
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Mean', 'Low CI', 'High CI', 'Parameter'])
        for i in range(self.ncol):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryTable = resultTable.copy()



#        quants =  np.apply_along_axis(self.quantileFX, 0, mcmc.bgibbs)
#        print('quants', quants)
#        resultTable[0:3, :] = quants 
#        resultTable[3, :] = np.apply_along_axis(np.mean, 0, mcmc.bgibbs)
#        resultTable = np.round(resultTable.transpose(), 3)
#        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])
#        names = ['Intercept', 'Beta1', 'Beta2']
#        for i in range(np.shape(resultTable)[0]):
#            name = names[i]
#            row = [name] + resultTable[i].tolist()
#            aa.add_row(row)
#        print(aa)


    def plotFX(self, params, mcmc):
        """
        make diagnostic trace plots
        """
        nTotalFigs = self.ncol  # + 1
        nfigures = np.int16(np.ceil(nTotalFigs / 6.0))
        cc = 0
        for i in range(nfigures):
            P.figure(i, figsize=(13, 9))
            lastFigure = i == (nfigures - 1)
            for j in range(4):
                P.subplot(2, 2, j+1)
                if cc < nTotalFigs:
                    P.plot(self.results[:, cc])
                    P.title(self.names[cc])
                cc += 1
                if lastFigure and (j==3):
                    P.subplot(2,2, 4)
                    P.plot(mcmc.bgibbs[:,0], mcmc.bgibbs[:,1])
                    P.title('parameter space: covariate 0 and 1')
            P.show(block=lastFigure)




    def plotFX_XXX(self, params, mcmc):
        """
        make diagnostic trace plots
        """
        meanBeta = np.apply_along_axis(np.mean, 0, mcmc.bgibbs)
        Ypredicted = np.dot(mcmc.x, meanBeta[0:3])
        nplots = params.npara  # + 1
        P.figure(figsize=(9,11))
        for i in range(nplots):
            P.subplot(2, 2, i+1)
            P.plot(mcmc.bgibbs[:, i])
            P.title('Parameter' + str(i))
        P.subplot(2,2, 4)
        P.plot(mcmc.bgibbs[:,0], mcmc.bgibbs[:,1])
        P.title('parameter space: covariate 0 and 1')
#        P.scatter(self.params.y, Ypredicted)
        P.show()


    def plotTrace(self, params, mcmc):
        """
        make diagnostic trace plots
        """
        nplots = params.npara  # + 1
        P.figure(figsize=(9,11))
        for plot in range(nplots):
            P.subplot(2, 2, plot+1)
            P.plot(mcmc.bgibbs[:, plot])
            P.title('Parameter' + str(plot))
        P.show()

    def plotUpdate(self, params, mcmc):
        """
        plot priors in posteriors 
        """
        print('params.meanB', params.meanB)
        nplots = params.npara  # + 1
        P.figure(figsize=(9,11))
        betaArr = np.arange(-4., 4., 0.01)
        for plot in range(nplots):
            prior_i = stats.norm.pdf(betaArr, params.meanB[plot], params.varB)
            P.subplot(2, 2, plot+1)
            P.hist(mcmc.bgibbs[:, plot], density=True, bins=20, color='b', label = 'Posterior')
            P.plot(betaArr, prior_i, color='k', label='Prior')
            P.legend(loc = 'upper left')
        P.show()



########            Main function
#######
def main():

    # run berndata to generate data
    berndata = BernData()
    params = Params()
    mcmc = MCMC(berndata, params)
    results = Results(berndata, params, mcmc)

if __name__ == '__main__':
    main()
